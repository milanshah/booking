export class UserProfile {

  userId: number;
  contactNumber: number;
  email: string;
  address: string;
  userName: string;

}