import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/map';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs-compat/operator/map';
import { UserProfile } from 'src/app/shared/services/user-profile';
@Injectable({
  providedIn: 'root'
})
export class DashServiceService {
  public myData: any;
  public subData: any;
  public catData: any;
  public setData: any;
  public isUserLoggedin: any;
  // UserDetails = new UserProfile()
  public userData: any = {
    email: '',
    password: '',
    userName: ''
  };

  constructor(private http: HttpClient) { }
  category = '';

  logout() {
    this.userData = {
      email: '',
      password: '',
      userName: ''
    };
  }
  getData(): any {   
   return this.http.get(environment.API_URL_REG + '/api/Dashboard/CategoryMasterData');
  
  }
  getSubCategory(id: any): any {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/CategoriesData?id=' + id);
  }
  getCategory(id: any): any {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/CategoriesData?id=' + id);
  }
  getstep(id: any): any {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/SubCategoryData?id=' + id);
  }
  setSelectedData(id) {
    this.setData = id;
  }
  postaddAdUnit(obj) {
    return this.http.post(environment.API_URL_REG + '/api/User/RegisterUser', obj);
  }
  postloginUnit(login) {
debugger
    return this.http.post(environment.API_URL_REG + '/api/User/userLogin', login);
  }
  getTimeSchedule(dayNumber) {
    return this.http.get(environment.API_URL_REG + '/api/User/DaysTime?dayNumber=' + dayNumber);
  }
  postAddUser(UserDetails) {
    return this.http.post(environment.API_URL_REG + '/api/User/UserDetails', UserDetails);
  }
  Paymentadd(pay) {
    return this.http.post(environment.API_URL_REG + '/api/User/Payment', pay);
  }
  PaymentReceipt(receipt) {
    return this.http.post(environment.API_URL_REG + '/api/User/PaymentReceipt', receipt);
  }
  Myorder(userId: any) {
    return this.http.get(environment.API_URL_REG + '/api/User/Userorder?userId=' + userId);

  }

  MyCancelorder(userId: any) {
    return this.http.get(environment.API_URL_REG + '/api/User/Usercancelorder?userId=' + userId);
  }
  userorderDelete(obj) {
    return this.http.get(environment.API_URL_REG + '/api/User/UserOrderDelete?orderId=' + obj);
  }
  downloadorderinpdf(orderId: any) {
    return this.http.get(environment.API_URL_REG + '/api/User/downloadorderinpdf?orderId=' + orderId);
  }
  // download(orderId:any) {
  //   return this.http.get(environment.API_URL_REG + '/api/User/download?orderId='+ orderId );
  // }
  download(orderId: any): any {
    try {
      return this.http.get<Blob>(environment.API_URL_REG + '/api/User/download?orderId=' + orderId, { responseType: 'blob' as 'json' })
        .map(res => {
          return new Blob([res], { type: 'application/pdf' });
        });
    }
    catch (Error) {
      alert(Error.message);
    }
  }
  cardDetails(obj) {
    return this.http.get(environment.API_URL_REG + '/api/User/CategoryName?CategoryValues=' + obj);
  }
  isUserExist(obj) {
    return this.http.get(environment.API_URL_REG + '/api/User/UserExist?email=' + obj);
  }
  EditProfile(obj) {
    return this.http.post(environment.API_URL_REG + '/api/User/EditProfile', obj);
  }
  ChngPass(obj) {
    return this.http.post(environment.API_URL_REG + '/api/User/ChangePassword', obj);
  }


  CreatePassword(token) {
    return this.http.post(environment.API_URL_REG + '/api/User/Create-Password', token);
  }

  ForgotPass(email) {
    return this.http.post(environment.API_URL_REG + '/api/User/ForgotPassword', email);
  }

  getProfile(userId: any) {
    const params = new HttpParams().set('userId', userId);
    return this.http.get(environment.API_URL_REG + '/api/User/Profile', { params: params });
  }
  getuserlist() {

    return this.http.get(environment.API_URL_REG + '/api/Dashboard/getuserlist');

  }
  getsingleuserlist(userId: any) {

    const params = new HttpParams().set('userId', userId);
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/getsingleuserlist', { params: params });

  }

  gettemplate(obj: any) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/gettemplate?Action=' + obj);
  }
  getedittemplate(settingId: any) {
    const params = new HttpParams().set('settingId', settingId);
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/getedittemplate', { params: params });
  }
  updatetemplate(obj) {

    return this.http.post(environment.API_URL_REG + '/api/Dashboard/updatetemplate?Action=', obj);
  }
  getsetting(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/getsetting?Action=' + obj);
  }
  updatesetting(obj) {

    return this.http.post(environment.API_URL_REG + '/api/Dashboard/Updatesetting?Action=', obj);
  }
  // for Master Category
  MasterCategoryList(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/MasterCategoryList?Action=' + obj);
  }
  Pagination(pageIndex, pageSize) {
    var obj = '?pageIndex=' + pageIndex + '&pageSize=' + pageSize
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/Pagination' + obj);
  }
  GetCategory(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/GetCategory?masterCategoryId=' + obj);
  }
  MasterCategoryEdit(obj) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/MasterCategoryEdit', obj);
  }
  MasterCategoryDelete(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/MasterCategoryDelete?masterCategoryId=' + obj);
  }

  AddMasterCategory(obj) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/AddMasterCategory', obj);
  }
  CheckBox(obj: any) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/MasterCategoryCheckbox', obj);
  }
  UserCheckBox(obj: any) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/ActiveUserCheckbox', obj);
  }
  MasterCategoryExist(obj, id) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/MasterCategoryExist?categoryName=' + obj + '&id=' + id);
  }


  //For Category
  CategoryList(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/CategoryList', obj);
  }

  CategoryEdit(obj) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/CategoryEdit', obj);
  }
  DeleteCategory(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/CategoryDelete?categoryId=' + obj);
  }
  AddCategory(obj) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/CategoryAdd', obj);
  }
  CategoryExist(obj, id) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/ CategoryExist?subcategoryName=' + obj + '&id=' + id);
  }
  CategoryCheckbox(obj: any) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/CategoryCheckbox', obj);
  }
  CategoryPagination(pageIndex, pageSize) {
    var obj = '?pageIndex=' + pageIndex + '&pageSize=' + pageSize
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/CategoryPagination' + obj);
  }
  CheckboxList(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/CheckboxList', obj);
  }

  // For Subcategory
  SubCategoyList(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/SubCategoryList', obj);
  }
  SubCategoryEdit(obj) {
    console.log('obj', obj)
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/SubCategoryEdit', obj);
  }
  SubCheckboxList(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/SubCheckboxList', obj);
  }
  SubCategoryAdd(obj) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/SubCategoryAdd', obj);
  }
  SubCategoryDelete(obj) {
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/SubCategoryDelete?SubCategoriesId=' + obj);
  }
  SubCategoryCheckbox(obj: any) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/SubCategoryCheckbox', obj);
  }
  SubCategoryPagination(pageIndex, pageSize) {
    var obj = '?pageIndex=' + pageIndex + '&pageSize=' + pageSize
    return this.http.get(environment.API_URL_REG + '/api/Dashboard/SubCategoryPagination' + obj);
  }
  ImageUpload(obj: any) {
    return this.http.post(environment.API_URL_REG + '/api/Dashboard/ImageUpload', obj);
  }

  Postdata(body: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Expose-Headers': 'Content-Length,X-CSRF-Token',
      }),
    };
    return this.http.post(environment.API_URL_REG + '/api/User/UserDetails', body, httpOptions);
  }
}



