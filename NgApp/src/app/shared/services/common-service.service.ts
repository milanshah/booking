import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/map';


@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public price: any;
  public time: any;
  public date: any;
  public category: any;
  public cardid: any;
  public services: any;
  public userid: any;
  public address: any;
  public Typeserivce: any;
  public Apartment: any;
  public keyInfo: any;
  public ZipCode: any;
  public special_Notes: any;
  public showHeader = true;
  public Image :any;
  


  constructor() {
    //for display selecetd data on header2//
    if (sessionStorage.getItem('selectedcategory')) {
      this.category = sessionStorage.getItem('selectedcategory')
    }
    if (sessionStorage.getItem('selectedDate')) {
      this.date = sessionStorage.getItem('selectedDate')
    }
    if (sessionStorage.getItem('SetTime')) {
      this.time = sessionStorage.getItem('SetTime')
    }
    if (sessionStorage.getItem('Number_of_selected_item')) {
      this.services = sessionStorage.getItem('Number_of_selected_item')
    }
    if (sessionStorage.getItem('id_of_cards')) {
      this.cardid = sessionStorage.getItem('id_of_cards')
    }
    if (sessionStorage.getItem('price')) {
      this.price = sessionStorage.getItem('price')
    }
    if (sessionStorage.getItem('Address')) {
      this.address = sessionStorage.getItem('Address')
    }
    if (sessionStorage.getItem('Apartment')) {
      this.Apartment = sessionStorage.getItem('Apartment')
    }

  }

}



