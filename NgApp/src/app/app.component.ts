import { Component, OnInit, HostListener, HostBinding, Inject, Input } from '@angular/core';
import { DashServiceService } from './shared/services/dash-service.service';
import { Location } from '@angular/common';
import { ActivatedRoute, Routes } from '@angular/router';
import 'rxjs/add/operator/map';
import { CommonService } from 'src/app/shared/services/common-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'demo';
  apiData: any;
  public pathString: string;
  showHeader = true;
  constructor(private getDataService: DashServiceService,
    public _location: Location, public route: ActivatedRoute, public _commonService: CommonService ) {
     
       }

  ngOnInit() {
   
    
  }
 
}


