import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidedrowerComponent } from './sidedrower.component';

const routes: Routes = [
    { path: '',component: SidedrowerComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SidedrowerRoutingModule { }
