import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidedrowerComponent } from './sidedrower.component';

describe('SidedrowerComponent', () => {
  let component: SidedrowerComponent;
  let fixture: ComponentFixture<SidedrowerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidedrowerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidedrowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
