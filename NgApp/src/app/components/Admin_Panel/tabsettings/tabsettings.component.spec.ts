import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsettingsComponent } from './tabsettings.component';

describe('TabsettingsComponent', () => {
  let component: TabsettingsComponent;
  let fixture: ComponentFixture<TabsettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsettingsComponent);
    component = fixture.componentInstance,
 
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
