import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { WINDOW_PROVIDERS, WINDOW } from 'src/app/shared/helpers/window.helper';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { UserareaComponent } from 'src/app/components/userarea/userarea.component';
import { Router } from '@angular/router';
import { LoginComponent } from 'src/app/components/login/login.component';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { from } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ProfileComponent } from 'src/app/components/profile/profile.component';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { ChangePasswordComponent } from 'src/app/components/change-password/change-password.component';
import { count, colorPickerChanged } from '../../../../node_modules/@syncfusion/ej2-richtexteditor';
//import { CategoryAddComponent } from 'src/app/components/Admin_Panel/master-category/category-add/category-add.component';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  showFiller = false;
  collapedSideBar: boolean;
  apiData: any;
  opn:boolean;
  visible = true;
  displayText = 'icon_section';
  MasterCategoryCount: any;
  collapsedCallCount: number = 0;
  expandedCallCount: number = 0;
  constructor(public dialog: MatDialog, @Inject(DOCUMENT)
  private document: Document, @Inject(WINDOW)
    private window: Window, private toastr: ToastrService,
    public getDataService: DashServiceService,
    private router: Router, private _commonService: CommonService) {

  }
  toggle() {
    this.visible = !this.visible;
    this.displayText =this.visible ?  'newicons' :'newicons'; 
  }

  receiveCollapsed($event) {
    this.collapedSideBar = $event;
  }


  isLogin = false;
  displayUserName: string;
  checkIsLogin(): void {
    if (sessionStorage.getItem("un") !== null) {
      this.isLogin = true;
      this.displayUserName = sessionStorage.getItem("un").toString();
    } else {
      this.isLogin = false;
    }
  }
  logoutuserdata() {
    this.toastr.success('Successfully!', 'Logout!', {
      timeOut: 1000
    });
    this.router.navigate(['/dashboard']);

    this.isLogin = false;
    sessionStorage.clear();
  }
  ngOnInit() {
    this.checkIsLogin();
    this.router.navigate(['/admin']);

  }

  // openLoginDialog() {
  //   this.dialog.open(UserareaComponent);
  // }
  // openRegistrationDialog() {
  //   this.dialog.open(LoginComponent);
  // }
  profile() {
    this.dialog.open(ProfileComponent);
  }
  password() {
    this.dialog.open(ChangePasswordComponent);
  }
  Master() {
    this.router.navigate(['/admin/master-category']);
  }
  Home() {
    this.router.navigate(['/admin']);
  }
  Category() {
    this.router.navigate(['/admin/category']);
  }
  SubCategory() {
    this.router.navigate(['/admin/sub-category']);
  }
  user() {
    this.router.navigate(['/admin/user']);
  }
  smtpsetting()
  {
    this.router.navigate(['/admin/setting']);
  }
  emailtemplate(){
    this.router.navigate(['/admin/emailtemplate']);
     
  }
  tabsettings(){
    this.router.navigate(['/admin/tabsettings']);
  }
  usersorder(){
    this.router.navigate(['/admin/userorder']);
  }
  myNewFunction(){  
    // var element = document.getElementById("myClickClass");   
    // element.classList.toggle("toggleClickEvent");
    var l =document.getElementById("settitle");
    var x = document.getElementById("myClickClass");
    
    l.classList.replace("addtitle","findTitle");
    if (x.style.display === "none") {
      x.style.display = "block";
      l.classList.replace("findTitle","findTitleone");

  } else {
     x.style.display = "none";
   // l.style.display = "none";
  //  x.classList.replace("icon_section","drawerhover");
    x.classList.remove("icon_section");
    l.classList.replace("findTitleone","findTitle");
  }
    
  }
  myFunction(){
    
  }
  onCollapse() {
    this.collapsedCallCount++;
  }
  onExpand() {
    this.expandedCallCount++;
  }
//   iconhover(){  
    
//     var a =document.getElementById("a");
//     var b = document.getElementById("b");
//     var c =document.getElementById("c");
//     var d = document.getElementById("d");
//     var e =document.getElementById("e");
//     var f = document.getElementById("f");
//     var g =document.getElementById("g");
//     if (a.focus) {
//       a.classList.replace("icon_section","drawerhover");
    

//   // } else {
//   //    x.style.display = "none";
//   //  // l.style.display = "none";
//   // //  x.classList.replace("icon_section","drawerhover");
//   //   x.classList.remove("icon_section");
    
//   // }
    
//   }
// }
}
