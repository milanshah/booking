import { ActivatedRoute } from '@angular/router';
import { CategoryAction } from 'src/app/shared/services/CategoryAction';
import { Component, OnInit, Inject, OnDestroy, Optional } from '@angular/core';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { UserProfile } from 'src/app/shared/services/user-profile';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-mastercategory-add-update-delete',
  templateUrl: './mastercategory-add-update-delete.component.html',
  styleUrls: ['./mastercategory-add-update-delete.component.less']
})
export class MastercategoryAddUpdateDeleteComponent implements OnInit {
  action: string;
  local_data: any;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  Category = new CategoryAction();
  apiData: Object;
  IsError: boolean;
  Message: any;
  constructor(public dialogRef: MatDialogRef<MastercategoryAddUpdateDeleteComponent>, public AddCategory: DashServiceService,
    private toastr: ToastrService, private _commonService: CommonService, @Optional() @Inject(MAT_DIALOG_DATA) public data: CategoryAction) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
  }

  ngOnInit() {
  }
  close(): void {
    this.dialogRef.close();
  }
  onBlurMethod() {

    var id = 0;
    if(this.local_data.masterCategoryId != 0)
    {
        id = this.local_data.masterCategoryId;
    }

    this.Category.categoryName = this.local_data.categoryName;
    this.AddCategory.MasterCategoryExist(this.Category.categoryName,id).subscribe(model => {
      this.IsError = true;
      this.apiData = model;
      this.Message = this.apiData;
      this.Message = this.Message.message;
      // console.log(this.Message);
    }, (err) => {
      this.Message = '';
      this.IsError = false;
    })
  }
  onKeydownEvent(event: KeyboardEvent): void {
    this.IsError = false;
  }
  doAction(detail: any): void {
    if (!detail.valid) {
      return;

    }
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }
}
