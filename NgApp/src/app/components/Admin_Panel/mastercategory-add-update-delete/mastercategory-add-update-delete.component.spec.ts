import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MastercategoryAddUpdateDeleteComponent } from './mastercategory-add-update-delete.component';

describe('MastercategoryAddUpdateDeleteComponent', () => {
  let component: MastercategoryAddUpdateDeleteComponent;
  let fixture: ComponentFixture<MastercategoryAddUpdateDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MastercategoryAddUpdateDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MastercategoryAddUpdateDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
