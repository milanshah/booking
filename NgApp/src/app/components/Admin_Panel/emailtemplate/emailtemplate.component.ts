import { Component, OnInit, ViewChild, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, RichTextEditorComponent, TableService } from '@syncfusion/ej2-angular-richtexteditor';
import { createElement, MouseEventArgs, addClass, removeClass, Browser } from '@syncfusion/ej2-base';
import * as CodeMirror from 'codemirror';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/css/css.js';
import 'codemirror/mode/htmlmixed/htmlmixed.js';
import { settingprofilemodel } from 'src/app/shared/services/settingprofile';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { template } from '../../../../../node_modules/@angular/core/src/render3';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { element } from '../../../../../node_modules/protractor';
import { EmailpopupComponent } from '../emailpopup/emailpopup.component';

@Component({
    selector: 'app-emailtemplate',
    templateUrl: './emailtemplate.component.html',
    styleUrls: ['./emailtemplate.component.less'],
    encapsulation: ViewEncapsulation.None,
    providers: [ToolbarService, LinkService, ImageService, RichTextEditorComponent, HtmlEditorService, TableService]
})
export class EmailtemplateComponent implements OnInit {
    templatesubject: any;
    value: any = '';
    apiDatalist: any[] = [];
    settingDetails = new settingprofilemodel();
    msg: any;
    WelcomeMail: any;
    settingId: any;
   dataSourceEmpty:string[];
    dataSource = new MatTableDataSource<settingprofilemodel>(this.apiDatalist);
    displayedColumns: string[] = ['No', 'Parameter', 'Templatesubject', 'Action'];
    counting: any = 1;
    @ViewChild(MatTable) table: MatTable<settingprofilemodel>;
    @ViewChild('toolsRTE') public rteObj: RichTextEditorComponent;
    public tools: object = {
        items: ['Bold', 'Italic', 'Underline', 'StrikeThrough',
            'FontName', 'FontSize', 'FontColor', 'BackgroundColor',
            'LowerCase', 'UpperCase', '|',
            'Formats', 'Alignments', 'OrderedList', 'UnorderedList',
            'Outdent', 'Indent', '|',
            'CreateTable', 'CreateLink', 'Image', '|', 'ClearFormat', 'Print',
            'SourceCode', 'FullScreen', '|', 'Undo', 'Redo']
    };
    public maxLength: number = 1000;
    textArea: HTMLElement;
    myCodeMirror: any;
    constructor(public dialog: MatDialog, public settingsevice: DashServiceService) {

    }

    ngOnInit() {

        this.settingsevice.gettemplate(this.settingDetails).subscribe((model: any) => {
            model.forEach((element: any) => {

                this.apiDatalist.push({

                    settingId: element.settingId,
                    parameter: element.parameter,
                    parameterValue: element.parameterValue,
                    type: element.type,
                    templatesubject: element.templatesubject,
                    action: element.action

                });

            });

            this.dataSource = new MatTableDataSource<settingprofilemodel>(this.apiDatalist);

        });
        console.log(this.apiDatalist);
        let rteObj: RichTextEditorComponent = this.rteObj;
        this.textArea = rteObj.contentModule.getEditPanel() as HTMLElement;
    }



    openDialog(action, obj) {
        
        obj.action = action;
        const dialogRef = this.dialog.open(EmailpopupComponent, {
            data: obj
        });
        
        localStorage.setItem('sessionsettingid', obj.settingId);
        this.settingDetails.settingId = obj.settingId;
        this.settingId = obj.settingId;
        this.settingsevice.getedittemplate(this.settingDetails.settingId).subscribe((model: any) => {
            model.forEach((element: any) => {


                this.settingId = element.parameter;
                this.WelcomeMail = element.parameter;
                this.templatesubject = element.templatesubject;
                this.value = element.parameterValue;


            });

            this.dataSource = new MatTableDataSource<settingprofilemodel>(this.apiDatalist);

        });
        dialogRef.afterClosed().subscribe(result => {
            if (result == 'submit') {
                this.apiDatalist = [];
                this.settingsevice.gettemplate(this.settingDetails).subscribe((model: any) => {
                    model.forEach((element: any) => {

                        this.apiDatalist.push({

                            settingId: element.settingId,
                            parameter: element.parameter,
                            parameterValue: element.parameterValue,
                            type: element.type,
                            templatesubject: element.templatesubject,
                            action: element.action

                        });

                    });

                    this.dataSource = new MatTableDataSource<settingprofilemodel>(this.apiDatalist);

                });
                //this.updateRowData(result.data);
            }
        });
    }
    applyFilter(filterValue: string) {
        var x = document.getElementById("nodisplay");
        // x.style.display = "none";
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if(this.dataSource.filteredData.length === 0)
        {
           
         this.dataSourceEmpty = [];
         x.style.display = "block";
        }
        else{
          x.style.display = "none";
        }
      }
    
    updateRowData(element) {
        
        this.settingDetails.parameterValue = element.value;
        this.settingDetails.templatesubject = element.templatesubject;
        this.settingDetails.type = 'template';
        this.settingsevice.updatetemplate(this.settingDetails).subscribe(model => {
            this.msg = model;
            this.apiDatalist = [];

            this.settingsevice.gettemplate(this.settingDetails).subscribe((model: any) => {
                model.forEach((element: any) => {

                    this.apiDatalist.push({

                        settingId: element.settingId,
                        parameter: element.parameter,
                        parameterValue: element.parameterValue,
                        type: element.type,
                        templatesubject: element.templatesubject,
                        action: element.action

                    });

                });

                this.dataSource = new MatTableDataSource<settingprofilemodel>(this.apiDatalist);

            });
        });

        this.value = '';
        this.templatesubject = '';
        localStorage.removeItem('sessionsettingid');
    }
}
