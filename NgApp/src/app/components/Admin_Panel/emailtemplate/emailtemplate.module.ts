import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailtemplateRoutingModule } from './emailtemplate-routing-module';
import { EmailtemplateComponent } from './emailtemplate.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule, } from '@angular/material/select';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatInputModule } from '@angular/material';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PortalModule } from '@angular/cdk/portal';
import { MatTabsModule } from '@angular/material/tabs';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { MatChipsModule } from '@angular/material/chips';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material';
import { WINDOW_PROVIDERS } from './../../../shared/helpers/window.helper';
import { TabModule } from '@syncfusion/ej2-angular-navigations';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { NumericTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { RadioButtonModule } from '@syncfusion/ej2-angular-buttons';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, RichTextEditorComponent, TableService } from '@syncfusion/ej2-angular-richtexteditor';
@NgModule({

    imports: [
        CommonModule,
        EmailtemplateRoutingModule,
        FormsModule,
        MatToolbarModule,
        MatIconModule, MatButtonModule, MatCheckboxModule,
        MatListModule,
        FlexLayoutModule,
        MatCardModule,
        MatSlideToggleModule,
        MatDialogModule,
        MatSnackBarModule,
        DragDropModule,
        MatInputModule,
        MatProgressBarModule,
        MatPaginatorModule,
        PortalModule, MatSelectModule, MatFormFieldModule,
        MatTabsModule,
        MatRadioModule,
        MatProgressSpinnerModule,
        ScrollDispatchModule,
        MatChipsModule,
        MatButtonToggleModule,
        MatMenuModule,
        MatTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSidenavModule,
        FormsModule,
        ReactiveFormsModule,
        TabModule,
        RichTextEditorAllModule,
        CheckBoxModule,
        DialogModule,
        NumericTextBoxModule,
        ButtonModule,
        RadioButtonModule,
        TextBoxModule,
        DropDownListModule
    ],
    declarations: [EmailtemplateComponent],
    providers: [ToolbarService, LinkService, RichTextEditorComponent, ImageService, HtmlEditorService, TableService,
        WINDOW_PROVIDERS],



})

export class EmailtemplateModule { }
