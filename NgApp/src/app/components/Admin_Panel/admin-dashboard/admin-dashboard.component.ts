import { Component, OnInit } from '@angular/core';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { GetCategoryAction } from 'src/app/shared/services/GetCategorylist';
@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.less']
})
export class AdminDashboardComponent implements OnInit {
  Category = new GetCategoryAction()
  MasterCategoryCount: any;
  apiData: any;
  CategoryCount: any;
  SubCategoyCount: any;
  constructor(public CategoryCountList: DashServiceService) { }

  ngOnInit() {
    this.CategoryCountList.MasterCategoryList(this.Category).subscribe((model: any) => {
      this.apiData = model;
      this.MasterCategoryCount = this.apiData.length
    })

    this.CategoryCountList.CategoryList(this.Category).subscribe((model: any) => {
      this.apiData = model;
      this.CategoryCount = this.apiData.length
    })
    this.CategoryCountList.SubCategoyList(this.Category).subscribe((model: any) => {
      this.apiData = model;
      this.SubCategoyCount = this.apiData.length
    })
  }
}