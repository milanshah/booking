import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserListProfile } from 'src/app/shared/services/useradmin';
import { MatDialog, MatDialogRef, MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-detail-view',
  templateUrl: './user-detail-view.component.html',
  styleUrls: ['./user-detail-view.component.less']
})
export class UserDetailViewComponent implements OnInit {
  userId: any;
  apiData: any;
  UserDetails = new UserListProfile();
  constructor(public dialog: MatDialog, public UserListsevice: DashServiceService, ) {

    this.UserDetails.userId = sessionStorage.getItem('euserId');

    this.UserListsevice.getsingleuserlist(this.UserDetails.userId).subscribe((model: any) => {
      this.apiData = model;
      console.log('apiData', this.apiData);
      this.UserDetails = this.apiData[0]

      this.UserDetails.name = this.UserDetails.name,
        this.UserDetails.userName = this.UserDetails.userName,
        this.UserDetails.password = this.UserDetails.password,
        this.UserDetails.contactNumber = this.UserDetails.contactNumber,
        this.UserDetails.email = this.UserDetails.email,
        this.UserDetails.address = this.UserDetails.address,
        this.UserDetails.occupation = this.UserDetails.occupation,
        this.UserDetails.image = this.UserDetails.image,
        this.UserDetails.roleId = this.UserDetails.roleId,
        this.UserDetails.roleName = this.UserDetails.roleName,
        this.UserDetails.token = this.UserDetails.token,
        this.UserDetails.isActive = this.UserDetails.isActive,
        this.UserDetails.Action = this.UserDetails.Action

    });


  }

  ngOnInit() {
    

  }

  setAddress(event) {
  }
  numberOnly(event) {
  }
}


