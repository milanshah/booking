import { ActivatedRoute } from '@angular/router';
import { GetCategoryAction } from 'src/app/shared/services/GetCategorylist';
import { Component, OnInit, Inject, OnDestroy, Optional } from '@angular/core';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { UserProfile } from 'src/app/shared/services/user-profile';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams, HttpRequest, HttpEventType } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-sub-category-add-update-delete',
  templateUrl: './sub-category-add-update-delete.component.html',
  styleUrls: ['./sub-category-add-update-delete.component.less']
})
export class SubCategoryAddUpdateDeleteComponent implements OnInit {

  action: string;
  local_data: any;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  Category = new GetCategoryAction();
  apiData: any;
  masterCategoryArray: any = [];
  IsError: boolean;
  Message: any;
  base64File: string = null;
  filename: string = null;
  public progress: number;
  public message: string;
  FileToUpload: any;

  constructor(private http: HttpClient, public dialogRef: MatDialogRef<SubCategoryAddUpdateDeleteComponent>, public AddCategory: DashServiceService,
    private toastr: ToastrService, private _commonService: CommonService, @Optional() @Inject(MAT_DIALOG_DATA) public data: GetCategoryAction) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
  }

  ngOnInit() {

    this.AddCategory.SubCheckboxList(this.Category).subscribe((model: any) => {
      this.apiData = model;
      this.apiData.forEach(element => {
        //   console.log(element);

        this.masterCategoryArray.push({
          categoryId: element.categoryId,
          subcategoryName: element.subcategoryName
        })

      });

    })
  }
  public onFileSelect(e: any): void {

    try {
    
      const file = e.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        // this._commonService.Image =  file.name;
        //   var str = file.name;
        //   this.filename = str.slice(0, 8);
        this.filename = file.name;
        this.base64File = _event.target.result;
        if (this.filename.length === 0)
          return;
       
        const formData = new FormData();
        formData.append(file.name, file);

        const uploadReq = new HttpRequest('POST', `http://localhost:5000/api/Dashboard/ImageUpload`, formData, {
          reportProgress: true,
        });
        // const uploadReq = new HttpRequest('POST', `http://192.168.0.16:7070/api/Dashboard/ImageUpload`, formData, {
        //   reportProgress: true,
        // });
        this.http.request(uploadReq).subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response)
            this.message = event.body.toString();
        });
      }
    } catch (error) {
      this.filename = null;
      this.base64File = null;
    }

  }

  close(): void {
    this.dialogRef.close();
  }
  onBlurMethod() {
    var id = 0;
    if (this.local_data.subCategoriesId != 0) {
      id = this.local_data.subCategoriesId;
    }

    this.Category.subcategoryName = this.local_data.subcategoryName;
    this.AddCategory.CategoryExist(this.Category.subcategoryName, id).subscribe(model => {
      this.IsError = true;
      this.apiData = model;
      this.Message = this.apiData;
      this.Message = this.Message.message;
      // console.log(this.Message);
    }, (err) => {
      this.Message = '';
      this.IsError = false;
    })
  }
  onKeydownEvent(event: KeyboardEvent): void {
    this.IsError = false;
  }
  doAction(detail: any): void {
    if (!detail.valid) {
      return;

    }

    this.dialogRef.close({ event: this.action, data: this.local_data, filedata: this.filename });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }
}
