import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubCategoryAddUpdateDeleteComponent } from './sub-category-add-update-delete.component';

describe('SubCategoryAddUpdateDeleteComponent', () => {
  let component: SubCategoryAddUpdateDeleteComponent;
  let fixture: ComponentFixture<SubCategoryAddUpdateDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubCategoryAddUpdateDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubCategoryAddUpdateDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
