import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserorderdetailsComponent } from './userorderdetails.component';


const routes: Routes = [
    { path: '',component: UserorderdetailsComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserorderdetailsRoutingModule { }
