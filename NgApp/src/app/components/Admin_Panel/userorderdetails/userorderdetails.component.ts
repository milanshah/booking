import { Component, OnInit,ViewChild } from '@angular/core';
import { Myorders } from 'src/app/shared/services/myorders';
import { MatDialog, MatTableDataSource, MatTable, MatSort, MatPaginator } from '@angular/material';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-userorderdetails',
  templateUrl: './userorderdetails.component.html',
  styleUrls: ['./userorderdetails.component.less']
})
export class UserorderdetailsComponent implements OnInit {
  apiData: Myorders[] = [];
  oreders = new Myorders();
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 100];
  dataSourceEmpty:string[];
  length:any
  dataSource = new MatTableDataSource<Myorders>(this.apiData);
  url:any="http://192.168.0.16:7071/#";
  displayedColumns: string[] = ['No', 'category', 'subcategoryName', 'Typeserivce', 'date', 'time', 'Address','price','GrandTotal','Action'];
  @ViewChild(MatTable) table: MatTable<Myorders>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  userId: any;
  constructor(public orderlist: DashServiceService,public datePipe: DatePipe,private route: ActivatedRoute,private router: Router) { 
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.userId = this.route.snapshot.paramMap.get('id');
    this.orderlist.Myorder(this.userId).subscribe((model: any) => {
      model.forEach((element: any) => {
        this.apiData.push({
          category:element.category,
          subcategoryName:JSON.parse(element.subcategoryName).join(','),
          // subcategoryName:element.subcategoryName,
          typeserivce:element.typeserivce,
           date:element.date=this.datePipe.transform(new Date(), 'dd-MM-yy'),
           time:element.time,
           address:element.address,
           price:element.price,
           grandTotal:element.grandTotal,
           orderId:element.orderId
               });
               
      });
      this.dataSource.paginator = this.paginator;
    });
  }
  applyFilter(filterValue: string) {
    var x = document.getElementById("nodisplay");
    // x.style.display = "none";
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if(this.dataSource.filteredData.length === 0)
    {
       
     this.dataSourceEmpty = [];
     x.style.display = "block";
    }
    else{
      x.style.display = "none";
    }
  }
  downloadPDF(orderId)
  {
    
    this.orderlist.download(orderId).subscribe(
      (res) => {
        
      var fileURL = URL.createObjectURL(res);
      window.open(fileURL);
      }
  );
 
  }
  // displayorder(orderId){
  //   debugger
  //   window.open('/admin/orderdetailsadmin/'+orderId)
  // }
  
}
