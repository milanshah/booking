import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { MatDialog,MatDialogRef, MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { UserListProfile } from 'src/app/shared/services/useradmin';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { FormBuilder } from '@angular/forms';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import {UserDetailViewComponent} from '../user-detail-view/user-detail-view.component';
import { element } from '../../../../../node_modules/protractor';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less']
})
export class UserComponent implements OnInit {

  userlist = new UserListProfile()
  status: false;
  apiData: UserListProfile[] = [];
  length =10;
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 50];
  dataSource = new MatTableDataSource<UserListProfile>(this.apiData);
  apimsg: Object;
  Message: any;
  api: Object;
  msg: Object;
  Message1: any;
  dataSourceEmpty:string[];
  constructor(public dialog: MatDialog, public UserListsevice: DashServiceService, private toastr: ToastrService, private fb: FormBuilder, http: HttpClient, private _commonService: CommonService) {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  displayedColumns: string[] = ['No','Name',  'Contact Number','Active','View'];
  @ViewChild(MatTable) table: MatTable<UserListProfile>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // new add for pagination
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    
    this.UserListsevice.getuserlist().subscribe((model: any) => {
      model.forEach((element: any) => {   
        this.apiData.push({
          userId      :element.userId,
          name         : element.name,
          userName     : element.userName,
          password     : element.password,
          contactNumber: element.contactNumber,
          email         : element.email,
          address       : element.address,
          occupation    : element.occupation,
          image         : element.image,
          roleId        : element.roleId,
          roleName      : element.roleName,
          token       : element.token,
          isActive:   element.isActive,
          Action        :element.Action
          
        });
      });
      this.dataSource.paginator = this.paginator;
      // new add for pagination
      this.dataSource.sort = this.sort;
    });
  }
  
  pagechange(pageEvent: any) {

    this.apiData = [];
    this.UserListsevice.Pagination(pageEvent.pageSize, pageEvent.pageIndex).subscribe((model: any) => {
      model.forEach((element: any) => {
        this.apiData.push({
          userId:element.userId,
          Action:element.Action,
          name: element.name,
          userName: element.userName,
          password: element.password,
          contactNumber: element.contactNumber,
          email: element.email,
          address: element.address,
          occupation: element.occupation,
          image: element.image,
          roleId: element.roleId,
          roleName      : element.roleName,
          isActive:   element.isActive,
          token: element.token

        });
      });
      console.log(this.apiData)
      this.dataSource.paginator = this.paginator;
    });
  }
  applyFilter(filterValue: string) {
    var x = document.getElementById("nodisplay");
    // x.style.display = "none";
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if(this.dataSource.filteredData.length === 0)
    {
       
     this.dataSourceEmpty = [];
     x.style.display = "block";
    }
    else{
      x.style.display = "none";
    }
  }

  // For Checkbox
  FieldsChange(values: any, userId: number) {
    let isActive = false;
    if (values) {
      isActive = true;
    }
    else {
      isActive = false;
    }
    let UpdateArry = {
      userId: userId,
      isActive: isActive,
      // Type: 'CategoryMaster'
    }
    this.UserListsevice.UserCheckBox(UpdateArry).subscribe(model => {
      let response = model;

    });
  }
  openDialog(euserId:any) 
  {
    
    console.log(euserId);
    sessionStorage.setItem('euserId',euserId);
    
    this.dialog.open(UserDetailViewComponent, {
         
    });
    
   
  }

}
