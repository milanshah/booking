import { Component, OnInit, Inject, OnDestroy, ViewChild } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { FormBuilder } from '@angular/forms';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { MatDialog, MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { SubCategoryAction } from 'src/app/shared/services/SubCategoryAction';
import { SubCategoryAddUpdateDeleteComponent } from '../sub-category-add-update-delete/sub-category-add-update-delete.component';
import { MatPaginator } from '@angular/material/paginator';
import { parse } from 'path';


@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.less']
})
export class SubCategoryComponent implements OnInit {
  Category = new SubCategoryAction()
  status: false;
  subData: any;
  apiData: SubCategoryAction[] = [];
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 100];
  dataSource = new MatTableDataSource<SubCategoryAction>(this.apiData);
  apimsg: Object;
  Message: any;
  api: Object;
  msg: Object;
  Message1: any;
  dataSourceEmpty:string[];
  cardDetails: Array<{ imageSrc: string }> = [];
  displayedColumns: string[] = ['No', 'category', 'subcategoryName', 'price', 'image', 'status', 'Action'];
  @ViewChild(MatTable) table: MatTable<SubCategoryAction>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog, public CategoryList: DashServiceService, private toastr: ToastrService, private fb: FormBuilder, http: HttpClient, private _commonService: CommonService) {  // new add for pagination 
    this.dataSource.paginator = this.paginator;

  }

  ngOnInit() {

    this.CategoryList.SubCategoyList(this.Category).subscribe((model: any) => {
      model.forEach((element: any) => {
        console.log('element', element)
        this.apiData.push({
          subCategoriesId: element.subCategoriesId,
          categoryId: element.categoryId,
          subcategoryName: element.subcategoryName,
          price: element.price,
          status: element.status,
          image: element.image,
          category: element.category
        });
      });
      this.dataSource.paginator = this.paginator;
    });
  }

  pagechange(pageEvent: any) {

    this.apiData = [];
    this.CategoryList.SubCategoryPagination(pageEvent.pageSize, pageEvent.pageIndex).subscribe((model: any) => {
      model.forEach((element: any) => {
        this.apiData.push({
          subCategoriesId: element.subCategoriesId,
          categoryId: element.categoryId,
          subcategoryName: element.subcategoryName,
          price: element.price,
          status: element.status,
          image: element.image,
          category: element.category

        });
      });
      this.dataSource.paginator = this.paginator;
    });
  }
  applyFilter(filterValue: string) {
    var x = document.getElementById("nodisplay");
    // x.style.display = "none";
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if(this.dataSource.filteredData.length === 0)
    {
       
     this.dataSourceEmpty = [];
     x.style.display = "block";
    }
    else{
      x.style.display = "none";
    }
  }


  openDialog(action, obj, filedata) {

    obj.action = action;
    obj.file = filedata;
    const dialogRef = this.dialog.open(SubCategoryAddUpdateDeleteComponent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Add') {
        //For Image
        
        result.data.file = result.filedata;
        this.addRowData(result.data);
      } else if (result.event == 'Update') {
        result.data.file = result.filedata;
        this.updateRowData(result.data);
      } else if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }
  addRowData(element) {
    
    this.Category.price = element.price;
    this.Category.image = "assets/img/" + element.file;
    this.Category.subcategoryName = element.subcategoryName;
    this.Category.categoryId = element.categoryId
    this.CategoryList.SubCategoryAdd(this.Category).subscribe(model => {
      this.msg = model;
      this.Message = this.msg
      this.Message = this.Message.message
      if (this.Message == "Category Add Successfully")
        this.toastr.success(this.Message, "", {
          timeOut: 3000
        });
      else if (this.Message == "Category Already exist") {
        this.toastr.error(this.Message, "", {
          timeOut: 3000
        });
      }
      this.apiData = [];
      //get List From Database
      this.CategoryList.SubCategoyList(this.Category).subscribe((model: any) => {
        model.forEach((element: any) => {
          this.apiData.push({
            subCategoriesId: element.subCategoriesId,
            categoryId: element.categoryId,
            subcategoryName: element.subcategoryName,
            price: element.price,
            status: element.status,
            image: element.image,
            category: element.category
          });
        });
        this.dataSource = new MatTableDataSource<SubCategoryAction>(this.apiData);
        this.dataSource.paginator = this.paginator;
      });
    });
  }
  updateRowData(element) {
    
   
    this.CategoryList.SubCategoryEdit(element).subscribe(model => {
      this.apimsg = model;
      this.Message1 = this.apimsg
      this.Message1 = this.Message1.message;
      if (this.Message1 == "Category Update Successfully")
        this.toastr.success(this.Message1, "", {
          timeOut: 3000
        });
      else if (this.Message1 == "Category Already exist") {
        this.toastr.error(this.Message1, "", {
          timeOut: 3000
        });
      }

      this.apiData = [];
      //get List From Database
      this.CategoryList.SubCategoyList(this.Category).subscribe((model: any) => {
        model.forEach((element: any) => {
          this.apiData.push({
            subCategoriesId: element.subCategoriesId,
            categoryId: element.categoryId,
            subcategoryName: element.subcategoryName,
            price: element.price,
            status: element.status,
            image: element.image,
            category: element.category
          });
        });
        this.dataSource = new MatTableDataSource<SubCategoryAction>(this.apiData);
        this.dataSource.paginator = this.paginator;
      });
    });

  }
  deleteRowData(element) {
    this.Category.subCategoriesId = element.subCategoriesId;
    this.CategoryList.SubCategoryDelete(this.Category.subCategoriesId).subscribe(model => {
      this.api = model
      this.Message = this.api
      this.Message = this.Message.message
      if (this.Message == "Category Delete Successfully")
        this.toastr.success(this.Message, "", {
          timeOut: 3000
        });
      else if (this.Message == "Category Already Use") {
        this.toastr.error(this.Message, "", {
          timeOut: 3000
        });
      }
      this.apiData = [];
      //get List From Database
      this.CategoryList.SubCategoyList(this.Category).subscribe((model: any) => {
        model.forEach((element: any) => {
          this.apiData.push({
            subCategoriesId: element.subCategoriesId,
            categoryId: element.categoryId,
            subcategoryName: element.subcategoryName,
            price: element.price,
            status: element.status,
            image: element.image,
            category: element.category
          });
        });
        this.dataSource = new MatTableDataSource<SubCategoryAction>(this.apiData);
        this.dataSource.paginator = this.paginator;
      });
    });
  }

  FieldsChange(values: any, Id: number) {
    let status = false;
    if (values) {
      status = true;
    }
    else {
      status = false;
    }
    let UpdateArry = {
      subCategoriesId: Id,
      Status: status,
      // Type: 'CategoryMaster'
    }
    this.CategoryList.SubCategoryCheckbox(UpdateArry).subscribe(model => {
      let response = model;

    });
  }

}

