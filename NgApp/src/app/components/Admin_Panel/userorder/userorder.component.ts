import { Component, OnInit, ViewChild } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { UserListProfile } from 'src/app/shared/services/useradmin';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog,MatDialogRef, MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { Router } from '@angular/router';
@Component({
  selector: 'app-userorder',
  templateUrl: './userorder.component.html',
  styleUrls: ['./userorder.component.less'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UserorderComponent implements OnInit {
  // dataSource = ELEMENT_DATA;
  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  // dataSourcedemo = ELEMENT_DATA;
  displayedColumns: string[] = ['No','Name',  'Contact Number','View'];
  // expandedElement: PeriodicElement | null;
  apiData: UserListProfile[] = [];
  userlist = new UserListProfile()
  length =10;
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 50];
  dataSourceEmpty:string[];
  dataSource = new MatTableDataSource<UserListProfile>(this.apiData);
  constructor(public UserListsevice: DashServiceService,private router: Router) { 
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  @ViewChild(MatTable) table: MatTable<UserListProfile>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // new add for pagination
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    
    this.UserListsevice.getuserlist().subscribe((model: any) => {
      model.forEach((element: any) => {
        console.log('elementcccccccccc',element);
      
        this.apiData.push({
          userId      :element.userId,
          name         : element.name,
          userName     : element.userName,
          password     : element.password,
          contactNumber: element.contactNumber,
          email         : element.email,
          address       : element.address,
          occupation    : element.occupation,
          image         : element.image,
          roleId        : element.roleId,
          roleName      : element.roleName,
          token       : element.token,
          isActive:   element.isActive,
          Action        :element.Action
          
        });
      });
      this.dataSource.paginator = this.paginator;
      // new add for pagination
      this.dataSource.sort = this.sort;
    });
  }
  orderdetails(userId){
  
   this.router.navigate(['/admin/userorderdetails', userId]);
  
}
applyFilter(filterValue: string) {
  var x = document.getElementById("nodisplay");
  // x.style.display = "none";
  this.dataSource.filter = filterValue.trim().toLowerCase();
  if(this.dataSource.filteredData.length === 0)
  {
     
   this.dataSourceEmpty = [];
   x.style.display = "block";
  }
  else{
    x.style.display = "none";
  }
}
}
  