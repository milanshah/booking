import { Component, OnInit } from '@angular/core';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { settingprofilemodel } from 'src/app/shared/services/settingprofile';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { NgModule } from '@angular/core';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.less']
})
export class SettingComponent implements OnInit {

  apiData: any;
  MailFrom: any;
  SmtpServer: any;
  SmtpUserName: any;
  SmtpPassword: any;
  SmtpPort: any;
  Message: any;
  msg: any;
  editForm: FormGroup;
  settingDetails = new settingprofilemodel();
  apismtpDatalist: settingprofilemodel[] = [];
  
  constructor(public formBuilder: FormBuilder, public settingsevice: DashServiceService, private toastr: ToastrService) { }

  ngOnInit() {
    this.settingsevice.getsetting(this.settingDetails).subscribe((model: any) => {
      model.forEach((element: any) => {

        this.apismtpDatalist.push({

          settingId: element.SettingId,
          parameter: element.parameter,
          parameterValue: element.parameterValue,
          type: element.type,
          action: element.action,
          templatesubject: element.templatesubject,
          
        });
        if (element.parameter == 'MailFrom') {
          this.MailFrom = element.parameterValue;
        }
        else if (element.parameter == 'SmtpServer') {
          this.SmtpServer = element.parameterValue;
        }
        else if (element.parameter == 'SmtpUserName') {
          this.SmtpUserName = element.parameterValue;
        }
        else if (element.parameter == 'SmtpPassword') {
          this.SmtpPassword = element.parameterValue;
        }
        else if (element.parameter == 'SmtpPort') {
          this.SmtpPort = element.parameterValue;
        }
      });
    });

  }
  updatesetting() {
    
    const propertyNamesOf = <TObj>(obj: TObj = null) => (name: keyof TObj) => name;


    if (this.hasOwnProperty("MailFrom")) {
      
      this.settingDetails.parameterValue = this.MailFrom;
      this.settingDetails.parameter = 'MailFrom';
      this.settingsevice.updatesetting(this.settingDetails).subscribe(model => {
        this.msg = model;
      });
    }
    if (this.hasOwnProperty("SmtpServer")) {
      
      this.settingDetails.parameterValue = this.SmtpServer;
      this.settingDetails.parameter = 'SmtpServer';
      this.settingsevice.updatesetting(this.settingDetails).subscribe(model => {
        this.msg = model;
      });
    }
    if (this.hasOwnProperty("SmtpUserName")) {
      
      this.settingDetails.parameterValue = this.SmtpUserName;
      this.settingDetails.parameter = 'SmtpUserName';
      this.settingsevice.updatesetting(this.settingDetails).subscribe(model => {
        this.msg = model;
      });
    }
    if (this.hasOwnProperty("SmtpPassword")) {
      
      this.settingDetails.parameterValue = this.SmtpPassword;
      this.settingDetails.parameter = 'SmtpPassword';
      this.settingsevice.updatesetting(this.settingDetails).subscribe(model => {
        this.msg = model;
      });
    }
    if (this.hasOwnProperty("SmtpPort")) {
      
      this.settingDetails.parameterValue = this.SmtpPort;
      this.settingDetails.parameter = 'SmtpPort';
      this.settingsevice.updatesetting(this.settingDetails).subscribe(model => {
        this.msg = model;
      });
    }
    this.Message == " Add Change Successfully"
        this.toastr.success(this.Message, " Add Change Successfully", {
          timeOut: 3000
        });
  }

}
