import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AdminDashboardModule } from './admin-dashboard/admin-dashboard.module';
import { SettingModule } from './setting/setting.module';
import { EmailtemplateModule } from './emailtemplate/emailtemplate.module';
import{TabsettingsModule} from './tabsettings/tabsettings.module';
import { UserorderModule } from './userorder/userorder.module';
import {UserorderdetailsModule} from './userorderdetails/userorderdetails.module';
import {OrderdetailsAdminModule} from './orderdetails-admin/orderdetails-admin-module';

const routes: Routes = [
    {
        path: '', component: LayoutComponent, children: [
            // { path: '', redirectTo: 'layout.component', pathMatch: 'prefix' },
            { path: '', redirectTo: 'admin-dashboard', pathMatch: 'prefix' },
            { path: 'master-category', loadChildren: './master-category/master-category.module#MasterCategoryModule' },
            { path: 'category', loadChildren: './category/category.module#CategoryModule' },
            { path: 'sub-category', loadChildren: './sub-category/sub-category.module#SubCategoryModule' },
            { path: 'admin-dashboard', loadChildren: './admin-dashboard/admin-dashboard.module#AdminDashboardModule' },
            { path: 'user', loadChildren: './user/user.module#UserModule' },
            { path: 'setting', loadChildren: './setting/setting.module#SettingModule' },
            {path:'emailtemplate',loadChildren:'./emailtemplate/emailtemplate.module#EmailtemplateModule' },
            {path:'tabsettings',loadChildren:'./tabsettings/tabsettings.module#TabsettingsModule'},
            {path:'sidedrower',loadChildren:'./sidedrower/sidedrower.module#SidedrowerModule'},
            {path:'userorder',loadChildren:'./userorder/userorder.module#UserorderModule'},
            {path:'userorderdetails/:id',loadChildren:'./userorderdetails/userorderdetails.module#UserorderdetailsModule'},
            {path:'orderdetailsadmin/:id',loadChildren:'./orderdetails-admin/orderdetails-admin-module#OrderdetailsAdminModule'}
           
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
