import { Component, OnInit, Inject, OnDestroy, ViewChild } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { FormBuilder } from '@angular/forms';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { MatDialog, MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { CategoryAction } from 'src/app/shared/services/CategoryAction';
import { MastercategoryAddUpdateDeleteComponent } from '../mastercategory-add-update-delete/mastercategory-add-update-delete.component';

import { MatPaginator } from '@angular/material/paginator';
import { parse } from 'path';

@Component({
  selector: 'app-master-category',
  templateUrl: './master-category.component.html',
  styleUrls: ['./master-category.component.less']
})
export class MasterCategoryComponent implements OnInit {
  Category = new CategoryAction()
  status: false;
  apiData: CategoryAction[] = [];
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 50];
  dataSource = new MatTableDataSource<CategoryAction>(this.apiData);
  apimsg: Object;
  Message: any;
  api: Object;
  msg: Object;
  Message1: any;
   dataSourceEmpty:string[];
  displayedColumns: string[] = ['No','categoryName', 'status', 'Action'];
  @ViewChild(MatTable) table: MatTable<CategoryAction>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // new add for pagination
  @ViewChild(MatSort) sort: MatSort;
  constructor(public dialog: MatDialog, public CategoryList: DashServiceService, private toastr: ToastrService, private fb: FormBuilder, http: HttpClient, private _commonService: CommonService) 
  {  // new add for pagination 
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    
  }
  ngOnInit() {
    
    // display Master Category Data
    this.CategoryList.MasterCategoryList(this.Category).subscribe((model: any) => {
      model.forEach((element: any) => {
        this.apiData.push({
          masterCategoryId: element.masterCategoryId,
          Action: element.Action,
          categoryName: element.categoryName,
          status: element.status,

        });
       
      });
      
      this.dataSource = new MatTableDataSource<CategoryAction>(this.apiData);
    this.dataSource.paginator = this.paginator;
    // new add for pagination
    this.dataSource.sort = this.sort;
    });
  }

  pagechange(pageEvent: any) {

    this.apiData = [];
    this.CategoryList.Pagination(pageEvent.pageSize, pageEvent.pageIndex).subscribe((model: any) => {
      model.forEach((element: any) => {
        this.apiData.push({
          masterCategoryId: element.masterCategoryId,
          Action: element.Action,
          categoryName: element.categoryName,
          status: element.status,

        });
      });
      this.dataSource.paginator = this.paginator;
    });
  }
  applyFilter(filterValue: string) {
    var x = document.getElementById("nodisplay");
    // x.style.display = "none";
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if(this.dataSource.filteredData.length === 0)
    {
       
     this.dataSourceEmpty = [];
     x.style.display = "block";
    }
    else{
      x.style.display = "none";
    }
  }



  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(MastercategoryAddUpdateDeleteComponent, {
      data: obj
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Add') {
        this.addRowData(result.data);
      } else if (result.event == 'Update') {
        this.updateRowData(result.data);
      } else if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(element) {
    this.Category.categoryName = element.categoryName;
    this.CategoryList.AddMasterCategory(this.Category).subscribe(model => {
      this.msg = model;
      this.Message = this.msg
      this.Message = this.Message.message
      if(this.Message == "Category Add Successfully")
      this.toastr.success(this.Message, "", {
        timeOut: 3000
      });
      else if(this.Message == "Category Already exist")
      {
        this.toastr.error(this.Message, "", {
          timeOut: 3000
        });
      }
      this.apiData = [];
      //get List From Database
      this.CategoryList.MasterCategoryList(this.Category.Action).subscribe((model: any) => {
        model.forEach((element: any) => {
          this.apiData.push({
            masterCategoryId: element.masterCategoryId,
            Action: element.Action,
            categoryName: element.categoryName,
            status: element.status,
          });
        });
        this.dataSource = new MatTableDataSource<CategoryAction>(this.apiData);
        this.dataSource.paginator = this.paginator;
      });
    });
  }
  updateRowData(element) {
   
    this.CategoryList.MasterCategoryEdit(element).subscribe(model => {
      this.apimsg = model;
      this.Message1 = this.apimsg
      this.Message1 = this.Message1.message;
      if(this.Message1 == "Category Update Successfully")
      this.toastr.success(this.Message1, "", {
        timeOut: 3000
      });
      else if(this.Message1 == "Category Already exist")
      {
        this.toastr.error(this.Message1, "", {
          timeOut: 3000
        });
      }
      //Array Null
      this.apiData = [];
      //get List From Database
      this.CategoryList.MasterCategoryList(this.Category.Action).subscribe((model: any) => {
        model.forEach((element: any) => {
          this.apiData.push({
            masterCategoryId: element.masterCategoryId,
            Action: element.Action,
            categoryName: element.categoryName,
            status: element.status,
          });
        });
        this.dataSource = new MatTableDataSource<CategoryAction>(this.apiData);
        this.dataSource.paginator = this.paginator;
      });
    });
  }
  deleteRowData(element) {
    this.Category.masterCategoryId = element.masterCategoryId;
    this.CategoryList.MasterCategoryDelete(this.Category.masterCategoryId).subscribe(model => {
      this.api = model
      this.Message = this.api
      this.Message = this.Message.message
      if(this.Message == "Category Delete Successfully")
      this.toastr.success(this.Message, "", {
        timeOut: 3000
      });
      else if(this.Message == "Category Already Use")
      {
        this.toastr.error(this.Message, "", {
          timeOut: 3000
        });
      }
      //Array Null
      this.apiData = [];
      //get List From Database
      this.CategoryList.MasterCategoryList(this.Category.Action).subscribe((model: any) => {
        model.forEach((element: any) => {
          this.apiData.push({
            masterCategoryId: element.masterCategoryId,
            Action: element.Action,
            categoryName: element.categoryName,
            status: element.status,
          });
        });
        this.dataSource = new MatTableDataSource<CategoryAction>(this.apiData);
        this.dataSource.paginator = this.paginator;
      });
    });
  }

  // For Checkbox
  FieldsChange(values: any, Id: number) {
    let status = false;
    if (values) {
      status = true;
    }
    else {
      status = false;
    }
    let UpdateArry = {
      masterCategoryId: Id,
      Status: status,
      // Type: 'CategoryMaster'
    }
    this.CategoryList.CheckBox(UpdateArry).subscribe(model => {
      let response = model;

    });
  }

}
