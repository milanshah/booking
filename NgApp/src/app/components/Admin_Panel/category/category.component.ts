import { Component, OnInit, Inject, OnDestroy, ViewChild } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { FormBuilder } from '@angular/forms';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { MatDialog, MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { GetCategoryAction } from 'src/app/shared/services/GetCategorylist';
import { CategoryAddUpdateDeleteComponent } from '../category-add-update-delete/category-add-update-delete.component';
import { MatPaginator } from '@angular/material/paginator';
import { parse } from 'path';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.less']
})
export class CategoryComponent implements OnInit {
  Category = new GetCategoryAction()
  status: false;
  apiData: GetCategoryAction[] = [];
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 100];
  dataSource = new MatTableDataSource<GetCategoryAction>(this.apiData);
  apimsg: Object;
  Message: any;
  api: Object;
  msg: Object;
  Message1: any;
  dataSourceEmpty:string[];
  displayedColumns: string[] = ['No', 'categoryName', 'subcategoryName', 'status', 'Action'];
  @ViewChild(MatTable) table: MatTable<GetCategoryAction>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // new add for pagination
  @ViewChild(MatSort) sort: MatSort;
  name: any;
  constructor(public dialog: MatDialog, public CategoryList: DashServiceService, private toastr: ToastrService, private fb: FormBuilder, http: HttpClient, private _commonService: CommonService) {  // new add for pagination 
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  ngOnInit() {
    this.CategoryList.CategoryList(this.Category).subscribe((model: any) => {
      model.forEach((element: any) => {
        this.apiData.push({
          masterCategoryId: element.masterCategoryId,
          categoryId: element.categoryId,
          Action: element.Action,
          subcategoryName: element.subcategoryName,
          categoryName: element.categoryName,
          status: element.status,
        });
      });
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }
  pagechange(pageEvent: any) {

    this.apiData = [];
    this.CategoryList.CategoryPagination(pageEvent.pageSize, pageEvent.pageIndex).subscribe((model: any) => {
      model.forEach((element: any) => {
        this.apiData.push({
          masterCategoryId: element.masterCategoryId,
          categoryId: element.categoryId,
          Action: element.Action,
          subcategoryName: element.subcategoryName,
          categoryName: element.categoryName,
          status: element.status,

        });
      });
      this.dataSource.paginator = this.paginator;
    });
  }
  applyFilter(filterValue: string) {
    var x = document.getElementById("nodisplay");
    // x.style.display = "none";
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if(this.dataSource.filteredData.length === 0)
    {
       
     this.dataSourceEmpty = [];
     x.style.display = "block";
    }
    else{
      x.style.display = "none";
    }
  }



  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(CategoryAddUpdateDeleteComponent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Add') {
        this.addRowData(result.data);
      } else if (result.event == 'Update') {
        this.updateRowData(result.data);
      } else if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(element) {


    this.Category.subcategoryName = element.subcategoryName;
    this.Category.masterCategoryId = element.masterCategoryId
    this.CategoryList.AddCategory(this.Category).subscribe(model => {
      this.msg = model;
      this.Message = this.msg
      this.Message = this.Message.message
      if (this.Message == "Category Add Successfully")
        this.toastr.success(this.Message, "", {
          timeOut: 3000
        });
      else if (this.Message == "Category Already exist") {
        this.toastr.error(this.Message, "", {
          timeOut: 3000
        });
      }
      this.apiData = [];
      //get List From Database
      this.CategoryList.CategoryList(this.Category).subscribe((model: any) => {
        model.forEach((element: any) => {
          this.apiData.push({
            masterCategoryId: element.masterCategoryId,
            categoryId: element.categoryId,
            Action: element.Action,
            subcategoryName: element.subcategoryName,
            categoryName: element.categoryName,
            status: element.status,
          });
        });
        this.dataSource = new MatTableDataSource<GetCategoryAction>(this.apiData);
        this.dataSource.paginator = this.paginator;
      });
    });
  }
  updateRowData(element) {
    // this.Category.categoryName = element.categoryName;
      this.CategoryList.CategoryEdit(element).subscribe(model => {
      this.apimsg = model;
      console.log(' this.apimsg', this.apimsg);
      this.Message1 = this.apimsg
      this.Message1 = this.Message1.message;
      if (this.Message1 == "Category Update Successfully")
        this.toastr.success(this.Message1, "", {
          timeOut: 3000
        });
      else if (this.Message1 == "Category Already exist") {
        this.toastr.error(this.Message1, "", {
          timeOut: 3000
        });
      }
      //Array Null
      this.apiData = [];
      //get List From Database
      this.CategoryList.CategoryList(this.Category).subscribe((model: any) => {
        model.forEach((element: any) => {
          this.apiData.push({
            masterCategoryId: element.masterCategoryId,
            categoryId: element.categoryId,
            Action: element.Action,
            subcategoryName: element.subcategoryName,
            categoryName: element.categoryName,
            status: element.status,
          });
        });
        this.dataSource = new MatTableDataSource<GetCategoryAction>(this.apiData);
        this.dataSource.paginator = this.paginator;
      });
    });
  }
  deleteRowData(element) {
    this.Category.categoryId = element.categoryId;
    this.CategoryList.DeleteCategory(this.Category.categoryId).subscribe(model => {
      this.api = model
      this.Message = this.api
      this.Message = this.Message.message
      if (this.Message == "Category Delete Successfully")
        this.toastr.success(this.Message, "", {
          timeOut: 3000
        });
      else if (this.Message == "Category Already Use") {
        this.toastr.error(this.Message, "", {
          timeOut: 3000
        });
      }
      //Array Null
      this.apiData = [];
      //get List From Database
      this.CategoryList.CategoryList(this.Category).subscribe((model: any) => {
        model.forEach((element: any) => {
          this.apiData.push({
            masterCategoryId: element.masterCategoryId,
            categoryId: element.categoryId,
            Action: element.Action,
            subcategoryName: element.subcategoryName,
            categoryName: element.categoryName,
            status: element.status,
          });
        });
        this.dataSource = new MatTableDataSource<GetCategoryAction>(this.apiData);
        this.dataSource.paginator = this.paginator;
      });
    });
  }

  // For Checkbox
  FieldsChange(values: any, Id: number) {
    let status = false;
    if (values) {
      status = true;
    }
    else {
      status = false;
    }
    let UpdateArry = {
      categoryId: Id,
      Status: status,
      // Type: 'CategoryMaster'
    }
    this.CategoryList.CategoryCheckbox(UpdateArry).subscribe(model => {
      let response = model;

    });
  }

}

