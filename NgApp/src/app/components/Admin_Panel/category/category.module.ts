import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryRoutingModule } from './category-routing-module';
import { CategoryComponent } from './category.component';
import { FormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule, } from '@angular/material/select';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatInputModule } from '@angular/material';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PortalModule } from '@angular/cdk/portal';
import { MatTabsModule } from '@angular/material/tabs';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { MatChipsModule } from '@angular/material/chips';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material';
import { WINDOW_PROVIDERS } from './../../../shared/helpers/window.helper';


@NgModule({

    imports: [
        CommonModule,
        CategoryRoutingModule,
        FormsModule,
        MatToolbarModule,
        MatIconModule, MatButtonModule, MatCheckboxModule,
        MatListModule,
        FlexLayoutModule,
        MatCardModule,
        MatSlideToggleModule,
        MatDialogModule,
        MatSnackBarModule,
        DragDropModule,
        MatInputModule,
        MatProgressBarModule,
        MatPaginatorModule,
        PortalModule, MatSelectModule, MatFormFieldModule,
        MatTabsModule,
        MatRadioModule,
        MatProgressSpinnerModule,
        ScrollDispatchModule,
        MatChipsModule,
        MatButtonToggleModule,
        MatMenuModule,
        MatTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSidenavModule
    ],
    declarations: [CategoryComponent],
    // entryComponents: [CategoryDeleteComponent],
    providers: [
        WINDOW_PROVIDERS,

    ],
})

export class CategoryModule { }
