import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryAddUpdateDeleteComponent } from './category-add-update-delete.component';

describe('CategoryAddUpdateDeleteComponent', () => {
  let component: CategoryAddUpdateDeleteComponent;
  let fixture: ComponentFixture<CategoryAddUpdateDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryAddUpdateDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryAddUpdateDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
