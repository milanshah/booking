import { ActivatedRoute } from '@angular/router';
import { GetCategoryAction } from 'src/app/shared/services/GetCategorylist';
import { Component, OnInit, Inject, OnDestroy, Optional } from '@angular/core';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { UserProfile } from 'src/app/shared/services/user-profile';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-category-add-update-delete',
  templateUrl: './category-add-update-delete.component.html',
  styleUrls: ['./category-add-update-delete.component.less']
})
export class CategoryAddUpdateDeleteComponent implements OnInit {
  action: string;
  local_data: any;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  Category = new GetCategoryAction();
  apiData: any;
  masterCategoryArray: any = [];
  IsError: boolean;
  Message: any;
  name: any;
  constructor(public dialogRef: MatDialogRef<CategoryAddUpdateDeleteComponent>, public AddCategory: DashServiceService,
    private toastr: ToastrService, private _commonService: CommonService, @Optional() @Inject(MAT_DIALOG_DATA) public data: GetCategoryAction) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
  }

  ngOnInit() {
    this.AddCategory.CheckboxList(this.Category).subscribe((model: any) => {
      this.apiData = model;
      this.apiData.forEach(element => {
        console.log(element);

        this.masterCategoryArray.push({
          masterCategoryId: element.masterCategoryId,
          categoryName: element.categoryName
        })

      });
      // this.name = this.name.categoryName;

      console.log('name', this.name);

    })
  }
  close(): void {
    this.dialogRef.close();
  }
  onBlurMethod() {
    var id = 0;
    if (this.local_data.categoryId != 0) {
      id = this.local_data.categoryId;
    }

    this.Category.subcategoryName = this.local_data.subcategoryName;
    this.AddCategory.CategoryExist(this.Category.subcategoryName, id).subscribe(model => {
      this.IsError = true;
      this.apiData = model;
      this.Message = this.apiData;
      this.Message = this.Message.message;
      // console.log(this.Message);
    }, (err) => {
      this.Message = '';
      this.IsError = false;
    })
  }
  onKeydownEvent(event: KeyboardEvent): void {
    this.IsError = false;
  }
  doAction(detail: any): void {
    if (!detail.valid) {
      return;

    }
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }
}

