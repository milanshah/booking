import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderdetailsAdminComponent } from './orderdetails-admin.component';

describe('OrderdetailsAdminComponent', () => {
  let component: OrderdetailsAdminComponent;
  let fixture: ComponentFixture<OrderdetailsAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderdetailsAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderdetailsAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
