import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
@Component({
  selector: 'app-orderdetails-admin',
  templateUrl: './orderdetails-admin.component.html',
  styleUrls: ['./orderdetails-admin.component.less']
})
export class OrderdetailsAdminComponent implements OnInit {

  name: any;
  data: any;
  category:any;
  date:any;
  time:any;
  address:any;
  orderId:any;
  price:any;
  grandTotal:any;
  subcategoryName:any;
  typeserivce: any;
  constructor(public datePipe: DatePipe,private route: ActivatedRoute,public orderlist: DashServiceService) { 
  
  }
  

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.orderlist.downloadorderinpdf(id).subscribe((res: any) => {
      console.log(res)   
      // this.name=
      // this.data
      this.category=res.category;
      this.date=res.date=this.datePipe.transform(new Date(), 'dd-MM-yy')
      this.time=res.time;
      this.address=res.address;
      this.orderId=res.orderId;
      this.price=res.price;
      this.grandTotal=res.grandTotal;
      this.typeserivce=res.typeserivce;
      this.subcategoryName= JSON.parse(res.subcategoryName).join(',');
      // JSON.parse(this.subcategoryName).join(',');
      // var splits = this.subcategoryName.split(",");
      // console.log("split sub category",splits);
      // console.log("split sub category",splits[0]);
    });
  }
  

}
