import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderdetailsAdminComponent } from './orderdetails-admin.component';

const routes: Routes = [
    { path: '',component: OrderdetailsAdminComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OrderdetailsAdminRoutingModule { }
