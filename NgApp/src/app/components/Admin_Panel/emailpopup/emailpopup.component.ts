import { Component, ViewChild, OnInit,Output,EventEmitter } from '@angular/core';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { settingprofilemodel } from 'src/app/shared/services/settingprofile';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, RichTextEditorComponent, TableService } from '@syncfusion/ej2-angular-richtexteditor';
import { createElement, MouseEventArgs, addClass, removeClass, Browser } from '@syncfusion/ej2-base';
import * as CodeMirror from 'codemirror';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/css/css.js';
import 'codemirror/mode/htmlmixed/htmlmixed.js';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { MatDialog, MatTableDataSource, MatTable, MatSort } from '@angular/material';

@Component({
    selector: 'app-emailpopup',
    templateUrl: './emailpopup.component.html',
    styleUrls: ['./emailpopup.component.less']
})
export class EmailpopupComponent implements OnInit {
    @Output() valueChange = new EventEmitter();
    msg: any;
    WelcomeMail: any;
    settingId: any;
    templatesubject: any;
    value: any = '';
    apiDatalist: any[] = [];
    settingDetails = new settingprofilemodel();
    emailtempalateArray: any = [];
    dataSource = new MatTableDataSource<settingprofilemodel>(this.apiDatalist);
    displayedColumns: string[] = ['No', 'parameter', 'templatesubject', 'Actoin'];
    counting: any = 1;
    @ViewChild(MatTable) table: MatTable<settingprofilemodel>;
    @ViewChild('toolsRTE') public rteObj: RichTextEditorComponent;
    action: string;
    local_data: any;
    public tools: object = {
        items: ['Bold', 'Italic', 'Underline', 'StrikeThrough',
            'FontName', 'FontSize', 'FontColor', 'BackgroundColor',
            'LowerCase', 'UpperCase', '|',
            'Formats', 'Alignments', 'OrderedList', 'UnorderedList',
            'Outdent', 'Indent', '|',
            'CreateTable', 'CreateLink', 'Image', '|', 'ClearFormat', 'Print',
            'SourceCode', 'FullScreen', '|', 'Undo', 'Redo']
    };
    public maxLength: number = 1000;
    textArea: HTMLElement;
    myCodeMirror: any;

    constructor(public dialogRef: MatDialogRef<EmailpopupComponent>, public settingsevice: DashServiceService) {
        //   this.local_data = { ...data };
        //   this.action = this.local_data.action;
    }

    ngOnInit() {
        debugger;
        this.settingDetails.settingId = localStorage.getItem('sessionsettingid');
        this.settingsevice.getedittemplate(this.settingDetails.settingId).subscribe((model: any) => {
            this.apiDatalist = model;
            console.log(model);
            this.apiDatalist.forEach(element => {


                this.settingId = element.settingId;
                this.WelcomeMail = element.parameter;
                this.templatesubject = element.templatesubject;
                this.value = element.parameterValue;
            });


        })

    }
    ngAfterViewInit(): void {

    }
    public mirrorConversion(e?: any): void {
        let id: string = this.rteObj.getID() + 'mirror-view';
        let mirrorView: HTMLElement = this.rteObj.element.querySelector('#' + id) as HTMLElement;
        let charCount: HTMLElement = this.rteObj.element.querySelector('.e-rte-character-count') as HTMLElement;
        if (e.targetItem === 'Preview') {
            this.textArea.style.display = 'block';
            mirrorView.style.display = 'none';
            this.textArea.innerHTML = this.myCodeMirror.getValue();
            charCount.style.display = 'block';
        } else {
            if (!mirrorView) {
                mirrorView = createElement('div', { className: 'e-content' });
                mirrorView.id = id;
                this.textArea.parentNode.appendChild(mirrorView);
            } else {
                mirrorView.innerHTML = '';
            }
            this.textArea.style.display = 'none';
            mirrorView.style.display = 'block';
            this.renderCodeMirror(mirrorView, this.rteObj.value);
            charCount.style.display = 'none';
        }
    }

    public renderCodeMirror(mirrorView: HTMLElement, content: string): void {
        this.myCodeMirror = CodeMirror(mirrorView, {
            value: content,
            lineNumbers: true,
            mode: 'text/html',
            lineWrapping: true,

        });
    }
    public handleFullScreen(e: any): void {
        const sbCntEle: HTMLElement = document.querySelector('.sb-content.e-view');
        const sbHdrEle: HTMLElement = document.querySelector('.sb-header.e-view');
        const leftBar: HTMLElement = document.querySelector('#left-sidebar');
        if (e.targetItem === 'Maximize') {
            if (Browser.isDevice && Browser.isIos) {
                addClass([sbCntEle, sbHdrEle], ['hide-header']);
            }
            addClass([leftBar], ['e-close']);
            removeClass([leftBar], ['e-open']);
        } else if (e.targetItem === 'Minimize') {
            if (Browser.isDevice && Browser.isIos) {
                removeClass([sbCntEle, sbHdrEle], ['hide-header']);
            }
            removeClass([leftBar], ['e-close']);
            if (!Browser.isDevice) {
                addClass([leftBar], ['e-open']);
            }
        }
    }
    public actionCompleteHandler(e: any): void {
        if (e.targetItem && (e.targetItem === 'SourceCode' || e.targetItem === 'Preview')) {
            (this.rteObj.sourceCodeModule.getPanel() as HTMLTextAreaElement).style.display = 'none';
            this.mirrorConversion(e);
        } else {
            setTimeout(() => { this.rteObj.toolbarModule.refreshToolbarOverflow(); }, 400);
        }
    }
    doAction(detail: any): void {
        if (!detail.valid) {
            return;

        }
        this.dialogRef.close({ event: this.action, data: this.local_data });
    }
    close(): void {
        this.dialogRef.close();
    }
    updateemailtamplate() {
        this.settingDetails.parameterValue = this.value;
        this.settingDetails.templatesubject = this.templatesubject;
        this.settingDetails.type = 'template';
        this.settingsevice.updatetemplate(this.settingDetails).subscribe(model => {
            this.msg = model;
            this.apiDatalist = [];

            this.settingsevice.gettemplate(this.settingDetails).subscribe((model: any) => {
                model.forEach((element: any) => {

                    this.apiDatalist.push({
                        settingId: element.settingId,
                        parameter: element.parameter,
                        parameterValue: element.parameterValue,
                        type: element.type,
                        templatesubject: element.templatesubject,
                        action: element.action
                    });
                });

                this.dataSource = new MatTableDataSource<settingprofilemodel>(this.apiDatalist);

            });
        });
        this.valueChange.emit(null);
        this.dialogRef.close('submit');

    }
    
}
