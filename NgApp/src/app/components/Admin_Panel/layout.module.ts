import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatInputModule, MatOptionModule } from '@angular/material';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PortalModule } from '@angular/cdk/portal';
import { MatTabsModule } from '@angular/material/tabs';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { MatChipsModule } from '@angular/material/chips';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material';
import { WINDOW_PROVIDERS } from './../../shared/helpers/window.helper';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MastercategoryAddUpdateDeleteComponent } from '../Admin_Panel/mastercategory-add-update-delete/mastercategory-add-update-delete.component';
import { CategoryComponent } from './category/category.component';
import { CategoryAddUpdateDeleteComponent } from './category-add-update-delete/category-add-update-delete.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { SubCategoryAddUpdateDeleteComponent } from './sub-category-add-update-delete/sub-category-add-update-delete.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import {MatExpansionModule} from '@angular/material/expansion'; 
import { EmailpopupComponent } from './emailpopup/emailpopup.component';
import { SidedrowerComponent } from './sidedrower/sidedrower.component';
import { TabsettingsComponent } from './tabsettings/tabsettings.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

// import { OrderdetailsAdminComponent } from './orderdetails-admin/orderdetails-admin.component';
// import { UserorderdetailsComponent } from './userorderdetails/userorderdetails.component';
//import { UserorderComponent } from './userorder/userorder.component';
 //import {SettingComponent} from './setting/setting.component';
// import { UserDetailViewComponent } from 'src/app/user-detail-view/user-detail-view.component';
@NgModule({
    declarations: [EmailpopupComponent,LayoutComponent, MastercategoryAddUpdateDeleteComponent, CategoryAddUpdateDeleteComponent, SubCategoryAddUpdateDeleteComponent, TabsettingsComponent],

    imports: [
        CommonModule, MatButtonModule, MatCheckboxModule,PdfViewerModule,
        MatFormFieldModule, MatSelectModule,
        LayoutRoutingModule,
        TranslateModule,
        MatIconModule,
        FormsModule,RichTextEditorAllModule,
        MatToolbarModule,
        MatListModule,
        FlexLayoutModule,
        MatCardModule,
        MatSlideToggleModule,
        MatDialogModule,
        MatSnackBarModule,
        DragDropModule,
        MatInputModule,
        MatProgressBarModule,
        MatPaginatorModule,
        PortalModule,
        MatTabsModule,
        MatRadioModule,
        MatProgressSpinnerModule,
        ScrollDispatchModule,
        MatChipsModule,
        MatButtonToggleModule,
        MatMenuModule,
        MatTableModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatNativeDateModule,
       
        MatSidenavModule,MatTooltipModule
    ],

    entryComponents: [EmailpopupComponent,MastercategoryAddUpdateDeleteComponent,CategoryAddUpdateDeleteComponent,SubCategoryAddUpdateDeleteComponent]

})
export class LayoutModule { }
