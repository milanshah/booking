import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { ActivatedRoute } from '@angular/router';
import { CreatePassword } from 'src/app/shared/services/Create-Password';
import { CommonService } from 'src/app/shared/services/common-service.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}
@Component({
  selector: 'app-create-password',
  templateUrl: './create-password.component.html',
  styleUrls: ['./create-password.component.less']
})
export class CreatePasswordComponent implements OnInit, OnDestroy {
  // UserDetailsFormControl = new FormControl('', [
  //   Validators.required,
  //   Validators.email,
  // ]);

  myForm: FormGroup;
  Password = new CreatePassword();
  matcher = new MyErrorStateMatcher();
  apiData: Object;
  Show_Con_Btn: boolean;
  Show_Con_eye: boolean;
  show_button: boolean;
  show_eye: boolean;
  param1: string;
  param2: string;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder, public Profile: DashServiceService, private toastr: ToastrService, public _commonService: CommonService) {
    this._commonService.showHeader = false;
    this.myForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    }, { validator: this.checkPasswords });

  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      const token = params['token'];
      console.log(token);
      this.Password.token = token;
    });
  }
  ngOnDestroy() {
    this._commonService.showHeader = true;
  }
  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true }
  }
  onSubmit(detail: any): void {
    if (!detail.valid) {
      return;

    }

    this.Profile.CreatePassword(this.Password).subscribe(model => {
      this.apiData = model;
      // this.refresh();
      this.toastr.success('Your Pasword has been updated!', 'Congratulations!', {
        timeOut: 2000
      });
    });
    this.router.navigate(['/dashboard']);
  }
  // onSubmit(detail: any): void {
  //   if (!detail.valid) {
  //     return;
  //   }
  //   this.Profile.CreatePassword(this.Password).subscribe(model => {
  //     this.apiData = model;
  //     this.toastr.success('Your Pasword has been updated!', 'Congratulations!', {
  //       timeOut: 2000

  //     });
  //     this.refresh();
  //   });
  // }
  showNewPass() {
    this.show_button = !this.show_button
    this.show_eye = !this.show_eye
  }
  ShowConPass() {
    this.Show_Con_Btn = !this.Show_Con_Btn
    this.Show_Con_eye = !this.Show_Con_eye;
  }
  refresh(): void {
    window.location.reload();
  }
}
