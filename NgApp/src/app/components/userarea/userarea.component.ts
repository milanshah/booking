import { Component, OnInit } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Routes, RouterModule, Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-userarea',
  templateUrl: './userarea.component.html',
  styleUrls: ['./userarea.component.less']
})
export class UserareaComponent implements OnInit {
  title = 'angular-material-tab-router';
  navLinks: any[];
  activeLinkIndex = -1;
  constructor(public dialogRef: MatDialogRef<UserareaComponent>,private router: Router, private _commonService: CommonService) {
    this.navLinks = [
      {
        label: 'registration',
        link: './registration',
        index: 0
      }, {
        label: 'userlogin',
        link: './userlogin',
        index: 1
      }
    ];
  }
  ngOnInit(): void {
    this.router.events.subscribe((res) => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
    });
  }
  close(): void {
    this.dialogRef.close();
  }
}
