import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { DashServiceService } from '../../shared/services/dash-service.service';


@Component({
  selector: 'app-day-time',
  templateUrl: './day-time.component.html',
  styleUrls: ['./day-time.component.less']
})
export class DayTimeComponent implements OnInit {
  nextDays: any;
  prevDays: any;
  Mydate: any;
  datestore: any;
  apiData: any;
  daytime: any;
  constructor(public DateTimeService: DashServiceService, private router: Router, private _commonService: CommonService) { }
  bookingDate: Array<{ weeklist: Date, dayClass: string, selectedDate: string, Changedate: string }> = [];
  ngOnInit() {
    this.datestore = sessionStorage.getItem('selectedDate');
    this._commonService.time = sessionStorage.getItem('SetTime');
    if (this.datestore) {
      this.Mydate = moment().format("DD-MMM-YYYY");
      this.nextDays = moment(this.datestore, 'DD-MMM-YYYY').clone().add(7, "days");
      this.prevDays = moment(this.datestore, 'DD-MMM-YYYY').clone().subtract(7, "days");
      this.getDates(this.prevDays, this.nextDays);
      var Time = moment(this.datestore).day();
      this.getTime(Time);
    }
  }
  getTime(dayNumber: number) {
    this.DateTimeService.getTimeSchedule(dayNumber).subscribe(data => {
      this.apiData = data;
      let itemList: any;
      itemList = { time: this.apiData[0].time, dayClass: 'Today' };
      //for next page by default set time
      // sessionStorage.setItem('SetTime', itemList.time);
      this.apiData[0] = itemList;
      this._commonService.time = this.apiData[0].time;
      if (sessionStorage.getItem('SetTime') !== null && sessionStorage.getItem('SetTime') !== undefined) {
        this.ChangeTime(sessionStorage.getItem('SetTime').toString());
      } else { sessionStorage.setItem('SetTime', itemList.time); }
    });
  }
  getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    let start = moment();
    while (currentDate <= stopDate) {
      dateArray.push(moment(currentDate).format("ddd, MMM.Do"));
      if (this.datestore == moment(currentDate).format("DD-MMM-YYYY")) {
        this.bookingDate.push({
          weeklist: moment(currentDate).toDate(), dayClass: 'Today colCom', selectedDate: moment(currentDate).format("ddd, MMM.Do"), Changedate: moment(currentDate).format("DD-MMM-YYYY")
        })
      }
      else if (start.isBefore(currentDate)) {
        this.bookingDate.push({
          weeklist: moment(currentDate).toDate(), dayClass: 'selected colCom', selectedDate: moment(currentDate).format("ddd, MMM.Do"), Changedate: moment(currentDate).format("DD-MMM-YYYY")
        })
      }
      else if (start.isAfter(currentDate)) {
        this.bookingDate.push({
          weeklist: moment(currentDate).toDate(), dayClass: 'disable  colCom', selectedDate: moment(currentDate).format("ddd, MMM.Do"), Changedate: moment(currentDate).format("DD-MMM-YYYY")
        })
      }
      currentDate = moment(currentDate).add(1, "days");
    }
  }
  ChangeHover(selectedDate: string, day: number, Changedate: string) {
    if (!Changedate) return;
    let previousDateIndex = this.bookingDate.findIndex(x => x.dayClass.includes('Today colCom'));
    let selecteIndex = this.bookingDate.findIndex(x => x.Changedate == Changedate);
    if (previousDateIndex >= 0 && selecteIndex >= 0) {
      this.bookingDate[selecteIndex]['dayClass'] = 'Today colCom';
      this.bookingDate[previousDateIndex]['dayClass'] = 'selected  colCom';
    }
    this._commonService.date = Changedate;
    sessionStorage.setItem('selectedDate', Changedate);
    this.getTime(moment(Changedate).day());
  }
  ChangeTime(time: string) {
    let itemList: any;
    this.apiData.forEach((element, index) => {
      if (element.time == time) {
        this._commonService.time = element.time;
        itemList = { time: element.time, dayClass: 'Today' };
      }
      else {
        itemList = { time: element.time, dayClass: 'selected' };
      }
      this.apiData[index] = itemList;
     // console.log('abc', time);

    });
    sessionStorage.setItem('SetTime', time);
  }
  Next() {
    this.router.navigate(['/address-info']);
  }
  Previous() {
    this.router.navigate(['/calendar']);
  }
}

