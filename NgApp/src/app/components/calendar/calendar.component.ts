import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/common-service.service';

@Component({
      selector: 'app-calendar',
      templateUrl: './calendar.component.html',
      styleUrls: ['./calendar.component.less']
})
export class CalendarComponent implements OnInit {
      Isselected: boolean;
      @Input()
      set configurations(config: any) {
            if (config) {
                  this.defaultConfigurations = config;
            }
      }
      @Input() eventData: any;
      defaultConfigurations: any;
      Week1: Array<{ day: number, dayName: string, dayClass: string, selectedDate: boolean, Changedate: String }> = [];
      Week2: Array<{ day: number, dayName: string, dayClass: string, selectedDate: boolean, Changedate: String }> = [];
      Week3: Array<{ day: number, dayName: string, dayClass: string, selectedDate: boolean, Changedate: String }> = [];
      Week4: Array<{ day: number, dayName: string, dayClass: string, selectedDate: boolean, Changedate: String }> = [];
      Week5: Array<{ day: number, dayName: string, dayClass: string, selectedDate: boolean, Changedate: String }> = [];
      Week6: Array<{ day: number, dayName: string, dayClass: string, selectedDate: boolean, Changedate: String }> = [];
      currentMonthNumber: number;
      DayArr = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
      MonthArr = ['Jan', 'Feb', 'March', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      monthNo: number;
      monthName: string;
      currMonthName: string;
      year: number;
      today: any;
      currentDate: any;
      Mydate: any;
      previousMonth: number;
      nextDays: any;
      prevDays: any;
      arr: any;
      date: any;
      dateArr: any[];
      constructor(private router: Router, private _commonService: CommonService) { }
      ngOnInit() {
            this.today = moment().date();
            this.Mydate = moment().format("DD-MMM-YYYY");
            this.Mydate = sessionStorage.getItem('selectedDate');
            this.year = moment().year();
            this.monthNo = moment().month();
            this.monthName = moment().startOf('month').format('MMMM');
            this.currMonthName = moment().startOf('month').format('MMMM');
            this.previousMonth = this.monthNo - 1;
            this.getMonths(this.monthName, this.year);
            sessionStorage.setItem("selectedDate", this.Mydate);
            this.currentMonthNumber = 0;
            if (this.Mydate !== null) {
                  this.Isselected = true;
            }
            else {
                  this.Isselected = false;
            }
      }
      getMonths(month, year): void {


            while (this.Week1.length > 0) {
                  this.Week1.pop();
            }

            while (this.Week2.length > 0) {
                  this.Week2.pop();
            }

            while (this.Week3.length > 0) {
                  this.Week3.pop();
            }

            while (this.Week4.length > 0) {
                  this.Week4.pop();
            }

            while (this.Week5.length > 0) {
                  this.Week5.pop();
            }
            while (this.Week6.length > 0) {
                  this.Week6.pop();
            }
            let index = 0;
            let start = moment(year + "-" + month, "YYYY-MMM");

            for (let end = moment(start).add(1, 'month'); start.isBefore(end); start.add(1, 'day')) {

                  let loopMonthNumber = + moment().month(start.format("MMMM")).format("M");
                  let itemList = { day: +start.format('D'), dayName: start.format('dddd'), dayClass: 'prev ', selectedDate: false, Changedate: start.format("DD-MMM-YYYY") };

                  //SelectedDate
                  if (this.Mydate == start.format("DD-MMM-YYYY")) {
                        itemList = { day: +start.format('D'), dayName: start.format('dddd'), dayClass: 'Today colCom', selectedDate: true, Changedate: start.format("DD-MMM-YYYY") };
                  }

                  //today
                  else if (moment().format('DD-MM-YYYY') == start.format('DD-MM-YYYY')) {
                        itemList = { day: +start.format('D'), dayName: start.format('dddd'), dayClass: 'selected  colCom ', selectedDate: true, Changedate: start.format("DD-MMM-YYYY") };

                  }
                  //Prev
                  else if (start.isBefore(moment())) {
                        itemList = { day: +start.format('D'), dayName: start.format('dddd'), dayClass: 'disable colCom', selectedDate: false, Changedate: null };

                  }
                  //next
                  else if (start.isAfter(moment())) {
                        itemList = { day: +start.format('D'), dayName: start.format('dddd'), dayClass: 'selected colCom', selectedDate: true, Changedate: start.format("DD-MMM-YYYY") };
                  }
                  if (index == 0) {
                        this.Week1.push();
                        this.Week1.push(itemList);
                  }
                  if (index == 1) {
                        this.Week2.push(itemList);
                  }
                  if (index == 2) {
                        this.Week3.push(itemList);
                  }
                  if (index == 3) {
                        this.Week4.push(itemList);
                  }
                  if (index == 4) {
                        this.Week5.push(itemList);
                  }
                  if (index == 5) {
                        this.Week6.push(itemList);
                  }
                  if (start.format('dddd') === 'Saturday') {
                        index++;
                  }
            }
            if (this.Week1.length != 7) {
                  let prevMonthNumber = moment([this.year, this.previousMonth]).daysInMonth();
                  for (let i = 7 - this.Week1.length; i > 0; i--) {
                        this.Week1.unshift({ day: prevMonthNumber, dayName: this.DayArr[i], dayClass: 'disable colCom', selectedDate: false, Changedate: null });
                        prevMonthNumber = prevMonthNumber - 1;
                  }
            }
            // console.log(this.Week1);
            // console.log(this.Week2);
            // console.log(this.Week3);
            // console.log(this.Week4);
            // console.log(this.Week5);
            // console.log(this.Week6);
      }
      Next() {

           
            this.monthNo = this.monthNo + 1;
          //  console.log('monthNo', this.monthNo);
            // december
            if (this.monthNo > 11) {
                  this.monthNo = 0;
            }

            this.previousMonth = this.monthNo - 1;
         //   console.log('previousMonth', this.previousMonth);
            this.monthName = moment.months(this.monthNo);
            if (this.monthName == 'January') {
                  this.year = this.year + 1;
            }
            this.getMonths(this.monthName, this.year);


      }
      Prev() {
            this.monthNo = this.monthNo - 1;
            if (this.monthNo < 0) {
                  this.monthNo = 11;
            }
            // jan
            if (this.monthNo == 0) {
                  this.previousMonth = 11; // jan
            } else {
                  this.previousMonth = this.monthNo - 1;
            }

            this.monthName = moment.months(this.monthNo);
            if (this.monthName == 'December') {
                  this.year = this.year - 1
            }
            this.getMonths(this.monthName, this.year);

      }
      ChangeHover(selectedDate: boolean, day: number, Changedate: string) {
            if (selectedDate && Changedate != null) {
                  this._commonService.date = Changedate;
                  sessionStorage.setItem("selectedDate", Changedate);
                  this.Isselected = true;
                  let itemList: any;

                  this.Week1.forEach((element, index) => {
                        if (element.dayClass.includes('disable')) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'disable colCom', selectedDate: true, Changedate: element.Changedate };
                        } else {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'selected colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        if (element.day == day) {
                              // this used when disable class is applyed for the privious days
                              if (!element.dayClass.includes('disable')) {
                                    itemList = { day: element.day, dayName: element.dayName, dayClass: 'Today colCom', selectedDate: true, Changedate: element.Changedate };
                              }
                        }
                        this.Week1[index] = itemList;
                        this.Mydate = Changedate;

                  });
                  this.Week2.forEach((element, index) => {
                        if (element.dayClass.includes('disable')) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'disable colCom', selectedDate: true, Changedate: element.Changedate };
                        } else {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'selected colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        if (element.day == day) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'Today colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        this.Week2[index] = itemList;
                        this.Mydate = Changedate;


                  });
                  this.Week3.forEach((element, index) => {
                        if (element.dayClass.includes('disable')) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'disable colCom', selectedDate: true, Changedate: element.Changedate };
                        } else {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'selected colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        if (element.day == day) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'Today colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        this.Week3[index] = itemList;
                        this.Mydate = Changedate;
                  });
                  this.Week4.forEach((element, index) => {
                        if (element.dayClass.includes('disable')) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'disable colCom', selectedDate: true, Changedate: element.Changedate };
                        } else {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'selected colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        if (element.day == day) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'Today colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        this.Week4[index] = itemList;
                        this.Mydate = Changedate;
                  });
                  this.Week5.forEach((element, index) => {
                        if (element.dayClass.includes('disable')) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'disable colCom', selectedDate: true, Changedate: element.Changedate };
                        } else {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'selected colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        if (element.day == day) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'Today colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        this.Week5[index] = itemList;
                        this.Mydate = Changedate;
                  });
                  this.Week6.forEach((element, index) => {
                        if (element.dayClass.includes('disable')) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'disable colCom', selectedDate: true, Changedate: element.Changedate };
                        } else {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'selected colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        if (element.day == day) {
                              itemList = { day: element.day, dayName: element.dayName, dayClass: 'Today colCom', selectedDate: true, Changedate: element.Changedate };
                        }
                        this.Week6[index] = itemList;
                        this.Mydate = Changedate;
                  });


            }

      }
      Next1() {

            this.router.navigate(['/day-time']);

      }
      Previous() {

            this.router.navigate(['/steptwo']);

      }

}











