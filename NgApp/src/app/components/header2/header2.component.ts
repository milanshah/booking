import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { runInThisContext } from 'vm';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header2',
  templateUrl: './header2.component.html',
  styleUrls: ['./header2.component.less']
})
export class Header2Component implements OnInit {

  constructor(public _commonService: CommonService, private router: Router) { }
  ngOnInit() {
    // this._commonService.time = sessionStorage.getItem('SetTime');
  }

  category() {
    if (sessionStorage.getItem('selectedcategory') !== null) {
      this._commonService.services = sessionStorage.getItem("Number_of_selected_item");
      this._commonService.date = sessionStorage.removeItem("selectedDate");
      this._commonService.time = sessionStorage.removeItem("SetTime");
      this._commonService.services = sessionStorage.removeItem("Number_of_selected_item");
      this._commonService.address = sessionStorage.removeItem("Address");
      sessionStorage.removeItem('AdrsForReceipt');
      this._commonService.cardid = sessionStorage.getItem("id_of_card");
      sessionStorage.removeItem("itemList");
      sessionStorage.removeItem('selectedcategory')


      this._commonService.Apartment = sessionStorage.removeItem("Apartment");
      this._commonService.ZipCode = sessionStorage.removeItem('ZipCode');
      this._commonService.special_Notes = sessionStorage.removeItem('special_Notes');
      this._commonService.price = sessionStorage.removeItem("price");
      this._commonService.keyInfo = sessionStorage.removeItem('keyInfo');
      this._commonService.Typeserivce = sessionStorage.removeItem("Typeserivce");
      sessionStorage.removeItem('taxtotal');
      sessionStorage.removeItem('GrandTotal');
      sessionStorage.removeItem('__paypal_storage__');
      sessionStorage.removeItem('orderId');
      sessionStorage.removeItem('TransactionId');
      if (sessionStorage.getItem("un") !== null) {
        sessionStorage.getItem("un");
        sessionStorage.getItem("userId");
      }

    }

    this.router.navigate(['/dashboard']);

  }

  services() {
    if (sessionStorage.getItem("Number_of_selected_item") !== null) {
      //this._commonService.services = sessionStorage.getItem("Number_of_selected_item");
      //  this._commonService.date = sessionStorage.removeItem("selectedDate");
      //  this._commonService.time = sessionStorage.removeItem("SetTime");
      //  this._commonService.address = sessionStorage.removeItem("Address");
      this._commonService.Apartment = sessionStorage.removeItem("Apartment");
      this._commonService.ZipCode = sessionStorage.removeItem('ZipCode');
      this._commonService.special_Notes = sessionStorage.removeItem('special_Notes');
      this._commonService.keyInfo = sessionStorage.removeItem('keyInfo');
      this._commonService.Typeserivce = sessionStorage.removeItem("Typeserivce");
      sessionStorage.removeItem('__paypal_storage__');
    }
    this.router.navigate(['/steptwo']);
  }
  selecteddate() {
    if (sessionStorage.getItem("selectedDate") !== null) {
      this._commonService.date = sessionStorage.getItem("selectedDate");
      //  this._commonService.address = sessionStorage.removeItem("Address");
      //  this._commonService.time = sessionStorage.removeItem("SetTime");
      this._commonService.Apartment = sessionStorage.removeItem("Apartment");
      this._commonService.ZipCode = sessionStorage.removeItem('ZipCode');
      this._commonService.special_Notes = sessionStorage.removeItem('special_Notes');
      this._commonService.keyInfo = sessionStorage.removeItem('keyInfo');
      this._commonService.Typeserivce = sessionStorage.removeItem("Typeserivce");
      sessionStorage.removeItem('__paypal_storage__');
    }
    this.router.navigate(['/calendar']);
  }
  ScheduleTime() {
    if (sessionStorage.getItem("SetTime") !== null) {
      this._commonService.time = sessionStorage.getItem('SetTime');
      //  this._commonService.address = sessionStorage.removeItem("Address");
      this._commonService.Apartment = sessionStorage.removeItem("Apartment");
      this._commonService.ZipCode = sessionStorage.removeItem('ZipCode');
      this._commonService.special_Notes = sessionStorage.removeItem('special_Notes');
      this._commonService.keyInfo = sessionStorage.removeItem('keyInfo');
      this._commonService.Typeserivce = sessionStorage.removeItem("Typeserivce");
      sessionStorage.removeItem('__paypal_storage__');
    }
    this.router.navigate(['/day-time']);
  }
  addressget() {
    if (sessionStorage.getItem("Address") !== null) {
      this._commonService.address = sessionStorage.getItem('Address');
      this._commonService.ZipCode = sessionStorage.getItem('ZipCode');
      this._commonService.special_Notes = sessionStorage.getItem('special_Notes');
      this._commonService.Apartment = sessionStorage.removeItem("Apartment");
      sessionStorage.removeItem('__paypal_storage__');
    }
    this.router.navigate(['/address-info']);
  }

}
