import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { DashServiceService } from '../../shared/services/dash-service.service';
import { Router } from '@angular/router';
import { IsUserExist } from '../../shared/services/isUserExist';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-forgot-pass',
  templateUrl: './forgot-pass.component.html',
  styleUrls: ['./forgot-pass.component.less']
})
export class ForgotPassComponent implements OnInit {

  UserDetailsFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  loading: boolean;
  apiData: Object;
  model = new IsUserExist();
  isNotError: boolean;
  isError: boolean;
  constructor(public dialogRef: MatDialogRef<ForgotPassComponent>, private toastr: ToastrService, private fb: FormBuilder, http: HttpClient, public ForPass: DashServiceService, private router: Router, private _commonService: CommonService) { }

  ngOnInit() {
  }
  close(): void {
    this.dialogRef.close();
  }
  Forgot(detail: any): void {
    if (!detail.valid) {
      return;
    }
    this.loading = true;
    this.ForPass.ForgotPass(this.model).subscribe(model1 => {
      this.apiData = model1;
      this.loading = false;
      this.toastr.success('Your Register EmailAddress Please Check', 'Link has been Send');
      this.dialogRef.close();
    }, err => {
      this.toastr.success('Please Register On Booking Site', 'User Not Exist');
      this.loading = false;
    });

  }
}
