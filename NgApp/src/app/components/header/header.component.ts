import { Component, OnInit, HostListener, HostBinding, Inject, Input } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { WINDOW_PROVIDERS, WINDOW } from '../../shared/helpers/window.helper';
import { DashServiceService } from '../../shared/services/dash-service.service';
import { UserloginComponent } from '../userlogin/userlogin.component';
import { UserareaComponent } from '../userarea/userarea.component';
import { Router } from '@angular/router';
import { logging } from '../../../../node_modules/protractor';
import { LoginComponent } from '../login/login.component';
import { RegistrationComponent } from '../registration/registration.component';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { from } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ProfileComponent } from '../profile/profile.component';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { ChangePasswordComponent } from '../change-password/change-password.component';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  isFixed;
  logoutstatus = false;

  constructor(public dialog: MatDialog, @Inject(DOCUMENT)
  private document: Document, @Inject(WINDOW)
    private window: Window, private toastr: ToastrService,
    public getDataService: DashServiceService,
    private router: Router, private _commonService: CommonService) {
  }
  isLogin = false;
  displayUserName: string;
  openLoginDialog() {
    this.dialog.open(UserareaComponent);
  }
  openRegistrationDialog() {
    this.dialog.open(LoginComponent);
  }
  ngOnInit() {
    this.checkIsLogin();
  }
  @HostListener('window:scroll', [])
  onWindowScroll() {
    const offset = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    if (offset > 10) {
      this.isFixed = true;
    } else {
      this.isFixed = false;
    }
  }

  @HostBinding("class.menu-opened") menuOpened = false;
  toggleMenu() {
    this.menuOpened = !this.menuOpened;
  }
  logoutuserdata() {
    this.toastr.success('Successfully!', 'Logout!', {
      timeOut: 1000
    });
    this.router.navigate(['/dashboard']);

    //this.refreshPage();
    this.isLogin = false;
    sessionStorage.clear();
  }
  // refreshPage(): void { 
  //   window.location.reload();
  // }
  checkIsLogin(): void {
    if (sessionStorage.getItem("un") !== null) {
      this.isLogin = true;
      this.displayUserName = sessionStorage.getItem("un").toString();
    } else {
      this.isLogin = false;
    }
  }
  profile() {
    this.dialog.open(ProfileComponent);
  }
  myorders() {
    this.router.navigate(['myorder']);
  }
  password() {
    this.dialog.open(ChangePasswordComponent);
  }
  orderdetails(){
    this.router.navigate(['orderdetails']);
  }
  clicklogo() {
    //sessionStorage.clear();
  }
  // Admin(){
  //   this.router.navigate(['/admin']);
  // }

}
