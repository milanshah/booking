import { Component, OnInit, HostListener, HostBinding, Inject, Input, ɵConsole } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common-service.service';

import { DashServiceService } from '../../shared/services/dash-service.service';
import 'rxjs/add/operator/map';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

  title = 'demo';
  myData: any;
  subData: any;
  catData: any;
  apiData: any;
  categoryName: any;
  subcategoryName: any;
  subcatName: any;
  selectservice: boolean;
  displayservice: string;
  subscription: any;
  browserRefresh: boolean;


  constructor(private getDataService: DashServiceService, private router: Router, private _commonService: CommonService) 
  {


    this.getDataService.getData().subscribe(data => {this.myData = data;     
      this.categoryName = data[0];   
      sessionStorage.setItem('selectedcategory', this.categoryName.categoryName)  
      this.masterCategoryChanged(this.myData[0]);
      //common service for display data header
      this._commonService.category = this.categoryName.categoryName; 
    });

  }

  getDataFromService(): void {
    this.getDataService.getData();
  } 
  // this api for get data on click button  like card
  selectCategory() {
    this.getDataService.setSelectedData(this.subcategoryName.categoryId);
    this._commonService.category = this.categoryName.categoryName;  
  }

  // this is get get subcategory name api
  masterCategoryChanged(item: any) {
    this.getDataService.getCategory(item.masterCategoryId).subscribe(data => {
      this.subData = data;
      this.subcategoryName = data[0];
      this.selectCategory();
    });
  }
  ngOnInit() {
    sessionStorage.getItem("id_of_card");
    if (sessionStorage.getItem("un") !== null) {
      sessionStorage.getItem("un");
      sessionStorage.getItem("userId");
    }
    this._commonService.date = sessionStorage.removeItem("selectedDate");
    this._commonService.time = sessionStorage.removeItem("SetTime");
    this._commonService.services = sessionStorage.removeItem("Number_of_selected_item");
    this._commonService.Apartment = sessionStorage.removeItem("Apartment");
    this._commonService.cardid = sessionStorage.removeItem("id_of_card");
    this._commonService.address = sessionStorage.removeItem("Address");
    sessionStorage.removeItem('AdrsForReceipt');
    this._commonService.price = sessionStorage.removeItem("price");
    this._commonService.ZipCode = sessionStorage.removeItem('ZipCode');
    this._commonService.special_Notes = sessionStorage.removeItem('special_Notes');
    sessionStorage.removeItem("itemList");
    sessionStorage.removeItem('selectedcategory')
    this._commonService.keyInfo = sessionStorage.removeItem('keyInfo');
    this._commonService.Typeserivce = sessionStorage.removeItem("Typeserivce");
    sessionStorage.removeItem('GrandTotal');
    sessionStorage.removeItem('__paypal_storage__');
    sessionStorage.removeItem("bookingService");
    sessionStorage.removeItem('orderId');
    sessionStorage.removeItem('TransactionId');
    sessionStorage.removeItem('taxtotal');

  }



  bookService() {
    sessionStorage.setItem("bookingService", this.subcategoryName.categoryId);
    this.router.navigate(['/steptwo']);
  }
  checkservice(): void {
    if (sessionStorage.getItem("service") !== null) {
      this.selectservice = true;
      this.displayservice = sessionStorage.getItem("service").toString();
    } else {
      this.selectservice = false;
    }
  }
}
