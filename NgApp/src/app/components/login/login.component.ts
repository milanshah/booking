import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { DashServiceService } from './../../shared/services/dash-service.service';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { Services } from '@angular/core/src/view';
import { copyObj } from '@angular/animations/browser/src/util';
import { post } from 'selenium-webdriver/http';
import { RegistrationComponent } from '../registration/registration.component';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  //  showLogin = false;
  dataFromServer: string;
  angForm: FormGroup;
  title = 'login';
  apiData: any;
  model: any = {
    email: '',
    password: '',
  };


  // showHideDiv(flag) {
  //    this.showLogin = flag;
  //  }

  constructor(public dialogRef: MatDialogRef<LoginComponent>, private loginunitservice: DashServiceService, private fb: FormBuilder, http: HttpClient) { this.createForm(); }
  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.angForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  onSubmit(): void {
    this.loginunitservice.postloginUnit(this.model).subscribe(model => {
      //  console.log(model);
      this.apiData = model;


    });
  }
  close(): void {
    this.dialogRef.close();
  }
}



