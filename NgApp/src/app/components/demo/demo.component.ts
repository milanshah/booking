import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.less']
})
export class DemoComponent implements OnInit {
  holidayDates: any = [];
  startDate: Date;
  endDate: Date;
  number: number;
  date: Date;
  display: Date;


  constructor() { }

  ngOnInit() {
    this.getSchoolVacationData();
  }
  public holidayArray: any = [
    { "start": "2019-02-25T00:00", "end": "2019-03-02T00:00", "year": 2019, "stateCode": "RP", "name": "winterferien", "slug": "winterferien-2019-RP", "list_of_dates": [] },
    { "start": "2019-04-23T00:00", "end": "2019-05-01T00:00", "year": 2019, "stateCode": "RP", "name": "osterferien", "slug": "osterferien-2019-RP", "list_of_dates": [] },
    { "start": "2019-07-01T00:00", "end": "2019-08-10T00:00", "year": 2019, "stateCode": "RP", "name": "sommerferien", "slug": "sommerferien-2019-RP", "list_of_dates": [] },
    { "start": "2019-09-30T00:00", "end": "2019-10-12T00:00", "year": 2019, "stateCode": "RP", "name": "herbstferien", "slug": "herbstferien-2019-RP", "list_of_dates": [] },
    { "start": "2019-12-23T00:00", "end": "2020-01-07T00:00", "year": 2019, "stateCode": "RP", "name": "weihnachtsferien", "slug": "weihnachtsferien-2019-RP", "list_of_dates": [] },
    { "start": "2018-10-01T00:00", "end": "2018-10-13T00:00", "year": 2018, "stateCode": "RP", "name": "herbstferien", "slug": "herbstferien-2018-RP", "list_of_dates": [] },
    { "start": "2018-12-20T00:00", "end": "2019-01-05T00:00", "year": 2018, "stateCode": "RP", "name": "weihnachtsferien", "slug": "weihnachtsferien-2018-RP", "list_of_dates": [] },
    { "start": "2018-06-26T00:00", "end": "2018-08-04T00:00", "year": 2018, "stateCode": "RP", "name": "sommerferien", "slug": "sommerferien-2018-RP", "list_of_dates": [] },
    { "start": "2018-03-26T00:00", "end": "2018-04-07T00:00", "year": 2018, "stateCode": "RP", "name": "osterferien", "slug": "osterferien-2018-RP", "list_of_dates": [] },
    { "start": "2020-02-17T00:00", "end": "2020-02-22T00:00", "year": 2020, "stateCode": "RP", "name": "winterferien", "slug": "winterferien-2020-RP", "list_of_dates": [] },
    { "start": "2020-04-09T00:00", "end": "2020-04-18T00:00", "year": 2020, "stateCode": "RP", "name": "osterferien", "slug": "osterferien-2020-RP", "list_of_dates": [] },
    { "start": "2020-07-06T00:00", "end": "2020-08-15T00:00", "year": 2020, "stateCode": "RP", "name": "sommerferien", "slug": "sommerferien-2020-RP", "list_of_dates": [] },
    { "start": "2020-10-12T00:00", "end": "2020-10-24T00:00", "year": 2020, "stateCode": "RP", "name": "herbstferien", "slug": "herbstferien-2020-RP", "list_of_dates": [] },
    { "start": "2020-12-21T00:00", "end": "2021-01-01T00:00", "year": 2020, "stateCode": "RP", "name": "weihnachtsferien", "slug": "weihnachtsferien-2020-RP", "list_of_dates": [] },
    { "start": "2017-04-10T00:00", "end": "2017-04-22T00:00", "year": 2017, "stateCode": "RP", "name": "osterferien", "slug": "osterferien-2017-RP", "list_of_dates": [] },
    { "start": "2017-07-03T00:00", "end": "2017-08-12T00:00", "year": 2017, "stateCode": "RP", "name": "sommerferien", "slug": "sommerferien-2017-RP", "list_of_dates": [] },
    { "start": "2017-10-02T00:00", "end": "2017-10-14T00:00", "year": 2017, "stateCode": "RP", "name": "herbstferien", "slug": "herbstferien-2017-RP", "list_of_dates": [] },
    { "start": "2017-12-22T00:00", "end": "2018-01-10T00:00", "year": 2017, "stateCode": "RP", "name": "weihnachtsferien", "slug": "weihnachtsferien-2017-RP", "list_of_dates": [] }
  ]

  getSchoolVacationData() {
    let i = 0;    
    this.holidayArray.forEach(element => {

      this.startDate = new Date(element.start);
      this.endDate = new Date(element.end);

      this.number = 0;      

      for (this.date = this.startDate; this.date <= this.endDate; this.date.setDate(this.date.getDate() + 1)) {        
        this.holidayDates.push(new Date(this.date));

        console.log(this.date);

        if (this.date === this.endDate) { break; }
        this.number++;
        this.display = this.date;
      }
            
      console.log(this.holidayDates);

      this.holidayArray[i].list_of_dates = this.holidayDates;
      this.holidayDates = [];
      i++;
    });

    this.holidayArray;
  }
}
