import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { DashServiceService } from './../../shared/services/dash-service.service';
import { Ng6NotifyPopupService } from 'ng6-notify-popup';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ForgotPassComponent } from '../forgot-pass/forgot-pass.component'
import { CommonService } from 'src/app/shared/services/common-service.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef } from '@angular/material';
// import {HeaderComponent} from '../header/header.component';
@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.less'],
  providers: [Ng6NotifyPopupService]
})
export class UserloginComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  // showLogin = false;
  show_button: Boolean = false;
  show_eye: Boolean = false;

  iserrorshow: boolean;
  dataFromServer: string;
  angForm: FormGroup;
  title = 'login';
  loginleble = false;
  userName: string;
  apiData: any;
  loginmsg: any;
  model: any = {
    email: '',
    password: '',
    userName: ''
  };
  isNotError: boolean;
  loginError: any;
  isError: boolean;

  constructor(public dialogRef: MatDialogRef<ForgotPassComponent>, private loginunitservice: DashServiceService, private router: Router,
    private notify: Ng6NotifyPopupService, private toastr: ToastrService, public dialog: MatDialog, private _commonService: CommonService) { }
  ngOnInit() { }

  onSubmit(loginForm: any): void {
    if (!loginForm.valid) {
      return;
    }
    this.loginunitservice.postloginUnit(this.model).subscribe(model1 => {
      this.apiData = model1;
      //  console.log('apiData', this.apiData);
      this.loginunitservice.userData = this.apiData.users;
      //  console.log('Login', this.loginunitservice.userData);
      sessionStorage.setItem('roleId', this.apiData.users.roleId)
      sessionStorage.setItem("un", this.apiData.users.userName);
      sessionStorage.setItem("userId", this.apiData.users.userId);

      if (this.apiData.users.roleId === 2)// admin
      {
        this.router.navigate(['/admin'])
        this.dialogRef.close();
      }
      else {
        this.refresh();
        sessionStorage.getItem("userId");
        sessionStorage.getItem('roleId');
        this.isNotError = true;
        this.toastr.success('Successfully!', 'login!', {
          timeOut: 1000
        });
        //  this.loginmsg = 'login Successfully';
      }
    }, err => {
      this.apiData = { message: '' };
      this.isError = true;
      this.toastr.success('Username or Password!', 'Invalid!', {
        timeOut: 1000
      });
      // this.loginmsg = 'Invalid Username or Password';
    });

  }

  refresh(): void {
    window.location.reload();

  }
  // toggleShow() {
  //   this.isError = false;
  //   this.isNotError = false;
  // }
  forgot() {
    this.dialog.open(ForgotPassComponent);
  }
  showPassword() {
    this.show_button = !this.show_button;
    this.show_eye = !this.show_eye;
  }
}


