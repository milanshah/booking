import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { DashServiceService } from './../../shared/services/dash-service.service';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { Services } from '@angular/core/src/view';
import { copyObj } from '@angular/animations/browser/src/util';
import { post } from 'selenium-webdriver/http';
import { resetComponentState } from '../../../../node_modules/@angular/core/src/render3/state';
import { IsUserExist } from 'src/app/shared/services/isUserExist';
import { from } from 'rxjs';
import { AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms'
import { Observable } from "rxjs/Observable";
// import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less'],
  // providers: [ToastrService]
})
export class RegistrationComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  loading: boolean;
  show_button: Boolean = false;
  show_eye: Boolean = false;
  IsError: boolean;
  //isEmailExist = new IsUserExist()
  apiData: any;
  model: any = {
    name: '',
    contactNumber: '',
    email: '',
    address: '',
    image: '',
    occupation: 'test',
    roleId: 1,
    userName: '',
    password: '',
    status: ''
  };
  msg: string;

  Message: any;
  ErrMsg: boolean;
  constructor(private toastr: ToastrService, private adunitservice: DashServiceService, private fb: FormBuilder, http: HttpClient, private _commonService: CommonService) { }
  ngOnInit() {

  }
  onSubmit(registrationForm: any): void {
    if (!registrationForm.valid) {
      return;
    }
    this.loading = true;
    this.adunitservice.postaddAdUnit(this.model).subscribe(model => {
      debugger
      this.apiData = model;
      this.loading = false;
      if(this.apiData.status==200)
      {
   // this.msg = 'Register Successfully';
   this.toastr.success('Register Successfully', 'Congratulation', {
    timeOut: 2000
    
  });
      } 
      else
      {
        return false
      }  
      this.refresh();
    });
  }

  refresh(): void {
    window.location.reload();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

//for Email Alredy Exist{
  onBlurMethod() {
    this.adunitservice.isUserExist(this.model.email).subscribe(model => {
      this.IsError = true;
      this.apiData = model;
      this.Message = this.apiData.message;
      console.log(this.apiData);
    }, (err) => {
      this.Message = '';
      this.IsError = false;
    })
  }
  onKeydownEvent(event: KeyboardEvent): void {
    this.IsError = false;
  }
///}

   showPassword() {
    this.show_button = !this.show_button;
    this.show_eye = !this.show_eye;
  }
}

