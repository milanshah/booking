import { Component, OnInit, ViewChild } from '@angular/core';
import { Myorders } from 'src/app/shared/services/myorders';
import { MatDialog, MatTableDataSource, MatTable, MatSort, MatPaginator } from '@angular/material';
import { DashServiceService } from 'src/app/shared/services/dash-service.service';
import { UserDetailViewComponent } from '../Admin_Panel/user-detail-view/user-detail-view.component';
import { DatePipe } from '@angular/common';
import { OrdercancelpopupComponent } from '../ordercancelpopup/ordercancelpopup.component';
import { FinalPaymentService } from 'src/app/shared/services/PaymentReceipt';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-myorder',
  templateUrl: './myorder.component.html',
  styleUrls: ['./myorder.component.less']
})
export class MyorderComponent implements OnInit {
  apiData: Myorders[] = [];
  apiDatacancel: Myorders[] = [];
  oreders = new Myorders();
  api: Object;
  msg: Object;
  Message1: any;
  Message: any;
  pageSize = 5;
  FinalPayment= new FinalPaymentService();
  pageSizeOptions = [5, 10, 25, 100];
  dataSourceEmpty:string[];
  url:any="http://192.168.0.16:7071/#";
  dataSource = new MatTableDataSource<Myorders>(this.apiData);
  dataSource2 = new MatTableDataSource<Myorders>(this.apiDatacancel);
  displayedColumns: string[] = ['No', 'category', 'subcategoryName', 'Typeserivce', 'date', 'time', 'Address','price','GrandTotal','Action'];
  @ViewChild(MatTable) table: MatTable<Myorders>;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;
  constructor(public dialog: MatDialog,public orderlist: DashServiceService,public datePipe: DatePipe, private toastr: ToastrService) { 
    this.dataSource.paginator = this.paginator;
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource2.paginator = this.paginator2;
  }
  _setDataSource(indexNumber) {
    setTimeout(() => {
      switch (indexNumber) {
        case 0:
          !this.dataSource.paginator ? this.dataSource.paginator = this.paginator : null;
          break;
        case 1:
          !this.dataSource2.paginator ? this.dataSource2.paginator = this.paginator2 : null;
      }
    });
  }
  ngOnInit() {
    
   var userId= sessionStorage.getItem("userId");
    this.orderlist.Myorder(userId).subscribe((model: any) => {
      model.forEach((element: any) => {
        this.apiData.push({
          category:element.category,
          subcategoryName:JSON.parse(element.subcategoryName).join(','),
          typeserivce:element.typeserivce,
           date:element.date=this.datePipe.transform(new Date(), 'dd-MM-yy HH:Mm'),
           time:element.time,
           address:element.address,
           price:element.price,
           grandTotal:element.grandTotal,
           orderId:element.orderId 
               });
                
      });
      this.dataSource.paginator = this.paginator;
    });
    this.orderlist.MyCancelorder(userId).subscribe((model: any) => {
      model.forEach((element: any) => {
        this.apiDatacancel.push({
          category:element.category,
          subcategoryName:JSON.parse(element.subcategoryName).join(','),
          typeserivce:element.typeserivce,
           date:element.date=this.datePipe.transform(new Date(), 'dd-MM-yy'),
           time:element.time,
           address:element.address,
           price:element.price,
           grandTotal:element.grandTotal,
           orderId:element.orderId
               });
      });
      this.dataSource.paginator = this.paginator;
    });
  }
  downloadPDF(orderId)
  {
    this.orderlist.download(orderId).subscribe(
      (res) => {
        
      var fileURL = URL.createObjectURL(res);
      window.open(fileURL);
      }
  );
 
  }
  orderdetails(orderId)
  {
    this.orderlist.downloadorderinpdf(orderId).subscribe((model: any) => {

    });
  }
  displaymyorder(orderId){
    window.open(this.url+'/orderdetails/'+orderId)
  }
  openDialog(action, obj) { 
    obj.action = action;
    const dialogRef = this.dialog.open(OrdercancelpopupComponent, {
      data: obj

    });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }
  deleteRowData(element) {
    debugger
    this.FinalPayment.orderId = element.orderId;
    this.orderlist.userorderDelete(this.FinalPayment.orderId).subscribe(model => {
      console.log("kjadcbkambk",model);
      debugger
      this.api = model
      this.Message = this.api 
      this.Message = this.Message.message
      if(model == 200)
      this.Message="Order Cancel Successfully";
      this.toastr.success(this.Message, "Order Cancel Successfully", {
        timeOut: 3000
      });
     
      //Array Null
      this.apiData = [];
      this.apiDatacancel=[];
      this.ngOnInit();
      console.log("1",this.apiData);
      console.log("2",this.apiDatacancel);
      this.dataSource = new MatTableDataSource<Myorders>(this.apiData);
      this.dataSource2 = new MatTableDataSource<Myorders>(this.apiDatacancel);
    });
  } 
  // pagechange(pageEvent: any) {

  //   this.apiData = [];
  //   this.orderlist.SubCategoryPagination(pageEvent.pageSize, pageEvent.pageIndex).subscribe((model: any) => {
  //     model.forEach((element: any) => {
  //       this.apiData.push({
  //         category:element.category,
  //         subcategoryName:JSON.parse(element.subcategoryName).join(','),
  //         typeserivce:element.typeserivce,
  //          date:element.date=this.datePipe.transform(new Date(), 'dd-MM-yy'),
  //          time:element.time,
  //          address:element.address,
  //          price:element.price,
  //          grandTotal:element.grandTotal,
  //          orderId:element.orderId 
  //              });
  //     });
  //     this.dataSource.paginator = this.paginator;
  //   });
  // }
}
