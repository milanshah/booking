import { Component, OnInit, Inject } from '@angular/core';
import { DashServiceService } from './../../shared/services/dash-service.service';
import { UserProfile } from 'src/app/shared/services/user-profile';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  name = 'Angular 4';
  url = '';

  apiData: any;
  UserDetails = new UserProfile();
  msg: string;
  isLoaded = true;
  onClose: any;
  Address: any;



  constructor(public dialogRef: MatDialogRef<ProfileComponent>, public Profile: DashServiceService, private toastr: ToastrService, private fb: FormBuilder, http: HttpClient, private _commonService: CommonService) {

    this.UserDetails.userId = + sessionStorage.getItem('userId');
    this.Profile.getProfile(this.UserDetails.userId).subscribe(model1 => {

      this.apiData = model1;
      this.UserDetails = this.apiData;

      this.UserDetails.userId = + sessionStorage.getItem('userId');
      this.UserDetails.userName = this.apiData.userName;
      this.UserDetails.email = this.apiData.email;
      this.UserDetails.address = this.apiData.address;
      this.UserDetails.contactNumber = this.apiData.contactNumber;


    }, error => {
      console.log(error)
    });


  }

  ngOnInit() {

  }
  onSubmit(detail: any): void {
    if (!detail.valid) {
      return;

    }

    this.Profile.EditProfile(this.UserDetails).subscribe(model => {
      this.apiData = model;

      this.toastr.success('Profile updated Successfully!', 'Congratulations!', {
        timeOut: 2000
      });
    });
  }
  // refresh(): void {
  //   window.location.reload();
  // }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  close(): void {
    this.dialogRef.close();
  }
  // for Google Api Address
  setAddress(addrObj) {
    this.Address = addrObj.formatted_address;
    this.UserDetails.address = this.Address;
  }
  //for profile image
  // onSelectFile(event) {
    
  //   if (event.target.files && event.target.files[0]) {
  //     console.log("-->>",event.target.files[0].name)
  //     var reader = new FileReader();

  //     reader.readAsDataURL(event.target.files[0]); // read file as data url

  //     reader.onload = (event) => { // called once readAsDataURL is completed
  //        this.url = event.target.result;
  //     }
  //   }
  // }
  // public delete(){
  //   this.url = null;
  // }

}
