import { Component, OnInit } from '@angular/core';
import { DashServiceService } from '../../shared/services/dash-service.service';
import { environment } from '../../../environments/environment';
import { NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoryDetails } from 'src/app/shared/services/category-details';
import 'rxjs/add/operator/map';
import { style } from '../../../../node_modules/@angular/animations';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { element } from 'protractor';
import { from } from 'rxjs';
@Component({
  selector: 'app-steptwo',
  templateUrl: './steptwo.component.html',
  styleUrls: ['./steptwo.component.less']
})
export class SteptwoComponent implements OnInit {
  subData: any;
  subcategoryName: any;
  apiData: any;
  categoryName: any;
  Isselected: boolean;
  sum = 0;
  getId: any;
  cards: Array<{ id: number, price: number }> = [];
  cardDetails: Array<{ id: number, price: number, subcategoryName: string, active: boolean, imageSrc: string }> = [];
  totalPrice = 0;
  itemList = [];
  constructor(private getDataService: DashServiceService, private router: Router, public _commonService: CommonService) { this.getId = sessionStorage.getItem("bookingService"); }
  ngOnInit() {

    if (sessionStorage.getItem("id_of_card") !== null) {
      sessionStorage.getItem("itemList");
      sessionStorage.getItem("id_of_card");
    }

    this.redirctTosubcategoryName()


  }
  redirctTosubcategoryName() {
debugger
    this.getDataService.getstep(this.getId).subscribe(data => {
      this.subData = data;
      // console.log('Images', this.subData);
      this.subData.forEach((element, index) => {
        this.cardDetails.push({ id: element.subCategoriesId, price: element.price, subcategoryName: element.subcategoryName, active: false, imageSrc: element.image })
      });
      // check user select Id or not
      if (sessionStorage.getItem("id_of_card") !== null) {
        this.totalPrice = + sessionStorage.getItem("price");
        this.totalPrice = 0;
        let ArryOFCatIds = this._commonService.cardid.split(',').map(Number);
        this.cardDetails.forEach((element, index) => {
          if (this.isInArray(ArryOFCatIds, element.id)) {
            this.totalPrice = this.totalPrice + element.price;
            element.active = true;
            this.Isselected = true;

            this.cards.push({ id: element.id, price: element.price });

          } else { element.active = false; }
          this.cardDetails[index] = element;
        });
        this._commonService.price = this.totalPrice.toString();
      }
      //  console.log(this.subData);
    });
  }
  isInArray(array, search) {
    return array.indexOf(search) >= 0;
  }
  Total(subCategoriesId, price, indexOfcard, active) {
    let findFromArray = this.cards.find(x => x.id == subCategoriesId);
    if (findFromArray == undefined) {
      this.cards.push({ id: subCategoriesId, price: price });
      this.totalPrice = this.totalPrice + price;
      this.Isselected = true;
      this.cardDetails[indexOfcard].active = true;
    }
    else {
      let index = this.cards.findIndex(x => x.id == subCategoriesId);
      this.cards.splice(index, 1);
      this.totalPrice = this.totalPrice - price;
      if (this.totalPrice == 0) {
        this.Isselected = false;
      }
      this.cardDetails[indexOfcard].active = false;
    }
    this._commonService.services = this.cards.length;
    sessionStorage.setItem("Number_of_selected_item", this.cards.length.toString());
    sessionStorage.setItem("price", this.totalPrice.toString())
    this._commonService.price = this.totalPrice.toString();
  }

  Next() {
    if (this.totalPrice > 0) {

      let cardIds = '';
      this.cards.forEach(element => {
        {
          cardIds = cardIds + element.id.toString() + (',');

        }
        this._commonService.cardid = cardIds;

        sessionStorage.setItem("id_of_card", this._commonService.cardid.replace(/,\s*$/, ""));
      });
      //for subcategory 
      if (cardIds.endsWith(',')) {
        cardIds = cardIds.replace(/,\s*$/, "");
      }
      this.getDataService.cardDetails(cardIds).subscribe(data => {
        this.apiData = data;
        this.apiData.forEach((element, index) => {
          let y = this.itemList.push(element.subCategoryName);
          if (this.apiData.length == [index + 1]) {
            let x = sessionStorage.setItem("itemList", JSON.stringify(this.itemList));
          }
        });
      })
      this.router.navigate(['/calendar']);
    }
  }
  Previous() {
    this.router.navigate(['/dashboard']);
  }

}





