
import { Component, OnInit } from '@angular/core';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import { FinalPaymentService } from 'src/app/shared/services/PaymentReceipt';
import { DashServiceService } from '../../shared/services/dash-service.service';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { PaymentDetails } from 'src/app/shared/services/payment-details.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.less']
})
export class PaymentDetailComponent implements OnInit {
  public payPalConfig?: IPayPalConfig;
  showSuccess: boolean;

  PaymentFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);


  Receipt = new FinalPaymentService()
  Payment = new PaymentDetails()
  Tax = 18;
  apiData: any;
  category: string;
  Address: string;
  Selectedservice: string;
  GrandTotal: number;
  Tip_SubTotal: number;
  list: any;
  model: any;
  WeekdayName: string;
  AdrsForReceipt: string;
  Apartment: string;


  constructor(public Paymentuser: DashServiceService, private router: Router, public _commonService: CommonService) { }
  PriceArray: Array<{ Title: string, DayClass: string }> = [];
  ngOnInit() {

    // for date format in Receipt
    let DateWeekName = this._commonService.date;
    this.WeekdayName = moment(DateWeekName).format("ddd, Do.MMM");
    //for sub category
    this.list = JSON.parse(sessionStorage.getItem("itemList"));
    this.category = sessionStorage.getItem('selectedcategory')
    //this.Address = sessionStorage.getItem('Address')
    this.AdrsForReceipt = sessionStorage.getItem('AdrsForReceipt')
    this.Apartment = sessionStorage.getItem('Apartment')

    this._commonService.keyInfo = sessionStorage.getItem('keyInfo');
    this._commonService.Typeserivce = sessionStorage.getItem("Typeserivce");
    // this._commonService.Apartment = sessionStorage.getItem("Apartment");
    // this.Selectedservice = sessionStorage.getItem('Selectedservice')
    //Total Price With Tax
    let total = + sessionStorage.getItem('price');
    this.GrandTotal = total + (total * 0.18);
    sessionStorage.setItem("GrandTotal", this.GrandTotal.toString());
    sessionStorage.setItem("taxtotal", this.GrandTotal.toString());

    this.PriceArray.push({ Title: ' 0.00', DayClass: 'selected' });
    this.PriceArray.push({ Title: ' 5.00', DayClass: 'selected' });
    this.PriceArray.push({ Title: ' 10.00', DayClass: 'selected' });
    this.PriceArray.push({ Title: ' 15.00', DayClass: 'selected ' });
    this.PriceArray.push({ Title: '20.00', DayClass: 'selected ' });

    this.initConfig();
  }

  Add_Tip(Title: string) {
    let itemlist: any;
    this.PriceArray.forEach((element, index) => {
      if (element.Title == Title) {
        itemlist = { Title: element.Title, DayClass: 'Today' };
      }
      else {
        itemlist = { Title: element.Title, DayClass: 'selected' };
      }
      this.PriceArray[index] = itemlist;
    })
    //With Tip Total
    // let Tip_Total = +  sessionStorage.getItem("GrandTotal");
    // let Tip_Total1 = + Title;
    // this.GrandTotal = Tip_Total + Tip_Total1;

    let Tip_Total = +  sessionStorage.getItem("taxtotal");
    let Tip_Total1 = + Title;
    this.GrandTotal = Tip_Total + Tip_Total1;
    sessionStorage.setItem("GrandTotal", this.GrandTotal.toString());

  }

  Previous() {
    this.router.navigate(['/address-info']);
  }
  //PayPal Code

  private initConfig(): void {
    this.payPalConfig = {
      currency: 'USD',
      clientId: 'Aci21UTrqwsQ0IPUl9ktHo7_O3nh3N5iNlfLFkIR2s0XKCxO4qi_rp9E6vIF5ar2QdJubS0miDhgjRu4',
      createOrderOnClient: (data) => <ICreateOrderRequest>{
        intent: 'CAPTURE',
        purchase_units: [
          {
            amount: {
              currency_code: 'USD',
              value: sessionStorage.getItem("GrandTotal"),
              breakdown: {
                item_total: {
                  currency_code: 'USD',
                  value: sessionStorage.getItem("GrandTotal"),
                }
              }
            },
          }
        ]
      },
      advanced: {
        commit: 'true'
      },
      style: {
        label: 'paypal',
        layout: 'vertical'
      },
      onApprove: (data, actions) => {
        // console.log('onApprove - transaction was approved, but not authorized', data, actions);
        this.Receipt.orderId = data.orderID;
        sessionStorage.setItem('orderId', this.Receipt.orderId);
        this.Receipt.TransactionId = data.payerID;
        sessionStorage.setItem('TransactionId', this.Receipt.TransactionId);
        actions.order.get().then(details => {
          //   console.log('onApprove - you can get full order details inside onApprove: ', details);

        });
        //
        this.onSubmit();
        //
      },
      onClientAuthorization: (data) => {
        // console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
        this.Receipt.Status = data.status;
        sessionStorage.setItem('Status', this.Receipt.Status);
        this.showSuccess = true;
      },
      onCancel: (data, actions) => {
        // console.log('OnCancel', data, actions);
      },
      onError: err => {
        // console.log('OnError', err);
      },
      onClick: (data, actions) => {
        //  console.log('onClick', data, actions);
      },
    };
  }
  onSubmit(): void {

    this.Receipt.userId = sessionStorage.getItem("userId");
    this.Receipt.GrandTotal = sessionStorage.getItem('GrandTotal');
    this.Receipt.category = sessionStorage.getItem('selectedcategory');
    this.Receipt.date = sessionStorage.getItem('selectedDate');
    let x = this.Receipt.date;
    // console.log('x',x);
    this.Receipt.time = sessionStorage.getItem('SetTime');
    this.Receipt.price = sessionStorage.getItem('price');
    this.Receipt.Address = sessionStorage.getItem('Address');
    this.Receipt.Apartment = sessionStorage.getItem('Apartment');
    this.Receipt.subCategory = sessionStorage.getItem('itemList');
    this.Receipt.keyInfo = sessionStorage.getItem('keyInfo');
    this.Receipt.Typeserivce = sessionStorage.getItem("Typeserivce");
    this.Receipt.orderId = sessionStorage.getItem('orderId');
    this.Receipt.TransactionId = sessionStorage.getItem('TransactionId');
    this.Receipt.Status = sessionStorage.getItem('Status');

    this.Paymentuser.PaymentReceipt(this.Receipt).subscribe(model => {
      this.apiData = model;
    });
    this.router.navigate(['/thank-you']);


  }

  //Paypal Cod 
}
