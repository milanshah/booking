import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ChangePwd } from 'src/app/shared/services/ChngPswd';
import { DashServiceService } from './../../shared/services/dash-service.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef } from '@angular/material';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.less']
})
export class ChangePasswordComponent implements OnInit {
  myForm: FormGroup;
  Password = new ChangePwd();
  matcher = new MyErrorStateMatcher();
  apiData: Object;
  // msg: string;
  show_button: Boolean = false;
  show_eye: Boolean = false;
  Show_Con_Btn: boolean;
  Show_Con_eye: boolean;

  constructor(public dialogRef: MatDialogRef<ChangePasswordComponent>, private formBuilder: FormBuilder, public Profile: DashServiceService, private toastr: ToastrService) {
    this.myForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    }, { validator: this.checkPasswords });
  }

  ngOnInit() {
  }
  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true }
  }
  onSubmit(detail: any): void {
    if (!detail.valid) {
      return;
    }
    this.Password.userId = + sessionStorage.getItem('userId');
    this.Profile.ChngPass(this.Password).subscribe(model => {
      this.apiData = model;
      // this.refresh();
      this.toastr.success('Your Pasword has been updated!', 'Congratulations!', {
        timeOut: 1000
      });
    });
  }
  close(): void {
    this.dialogRef.close();
  }
  showNewPass() {
    this.show_button = !this.show_button;
    this.show_eye = !this.show_eye;
  }
  ShowConPass() {
    this.Show_Con_Btn = !this.Show_Con_Btn
    this.Show_Con_eye = !this.Show_Con_eye;
  }
}
