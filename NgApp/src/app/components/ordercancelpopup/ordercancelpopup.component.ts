import { Component, OnInit,Inject,Optional } from '@angular/core';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FinalPaymentService } from 'src/app/shared/services/PaymentReceipt';
@Component({
  selector: 'app-ordercancelpopup',
  templateUrl: './ordercancelpopup.component.html',
  styleUrls: ['./ordercancelpopup.component.less']
})
export class OrdercancelpopupComponent implements OnInit {
  action: string;
  local_data: any;
  FinalPayment= new FinalPaymentService();
  apiData: Object;
  IsError: boolean; 
  Message: any;
  constructor(public dialogRef: MatDialogRef<OrdercancelpopupComponent>, @Optional() @Inject(MAT_DIALOG_DATA) public data: FinalPaymentService) {
    this.local_data = { ...data };
  this.action = this.local_data.action;
   }
  
  ngOnInit() {
  }
  close(): void {
    this.dialogRef.close();
  }
  
  onKeydownEvent(event: KeyboardEvent): void {
    this.IsError = false;
  }
  doAction(detail: any): void {
    if (!detail.valid) {
      return;

    }
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }
}
