import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdercancelpopupComponent } from './ordercancelpopup.component';

describe('OrdercancelpopupComponent', () => {
  let component: OrdercancelpopupComponent;
  let fixture: ComponentFixture<OrdercancelpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdercancelpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdercancelpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
