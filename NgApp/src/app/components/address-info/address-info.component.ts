import { Component, OnInit, NgZone } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { Router } from '@angular/router';
import { element } from '@angular/core/src/render3';
import { DashServiceService } from './../../shared/services/dash-service.service';
import { Add_Address } from 'src/app/shared/services/user-details.service'
import { from } from 'rxjs';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserareaComponent } from '../userarea/userarea.component';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpResponse, HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import { GooglePlacesDirective } from 'src/app/shared/google-api/google-places.directive'
//import {PaymentDetailComponent}from 'src/app/components/payment-detail/payment-detail.component';
@Component({
  selector: 'app-address-info',
  templateUrl: './address-info.component.html',
  styleUrls: ['./address-info.component.less']
})
export class AddressInfoComponent implements OnInit {

  public title = 'Places';
  public addrKeys: string[];
  public postal: any;
  UserDetailsFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  address: any;
  textbox = false;
  apiData: any;
  UserAdd = new Add_Address()
  Address: any;
  Typeserivce: string;
  HomeType: string;
  keyInfo: string;
  addr: any;
  constructor(private zone: NgZone, private _commonService: CommonService, private fb: FormBuilder, http: HttpClient, public dialog: MatDialog, private router: Router, private AddUser: DashServiceService) {
  }
  Bookservice: Array<{ Title: string, DayClass: string }> = [];
  Home: Array<{ homekey: string, DayClass: string, isopen: boolean }> = [];

  ngOnInit() {
    if (sessionStorage.getItem("Apartment") !== null) {
      this.UserAdd.Apartment = sessionStorage.getItem("Apartment");
    }
    if (sessionStorage.getItem("Address") !== null) {
      this.UserAdd.Address = sessionStorage.getItem("Address");
    }
    if (sessionStorage.getItem("ZipCode") !== null) {
      this.UserAdd.ZipCode = sessionStorage.getItem("ZipCode");
    }
    if (sessionStorage.getItem("special_Notes") !== null) {
      this.UserAdd.special_Notes = sessionStorage.getItem("special_Notes");
    }
    if (sessionStorage.getItem("about_Key") !== null) {
      this.UserAdd.about_Key = sessionStorage.getItem("about_Key");
    }
    if (sessionStorage.getItem("Typeserivce") !== null) {
      this.UserAdd.Typeserivce = sessionStorage.getItem("Typeserivce");
    }
    if (sessionStorage.getItem("keyInfo") !== null) {
      this.UserAdd.keyInfo = sessionStorage.getItem("keyInfo");
    }

    this.Typeserivce = 'One Time';
    this._commonService.Typeserivce = sessionStorage.setItem('Typeserivce', this.Typeserivce);
    this.keyInfo = 'Someone is Home';
    this._commonService.keyInfo = sessionStorage.setItem('keyInfo', this.keyInfo);

    this.Bookservice.push({ Title: 'One Time', DayClass: 'Today' });
    this.Bookservice.push({ Title: 'Weekly', DayClass: 'selected' });
    this.Bookservice.push({ Title: 'Every 2 Weeks', DayClass: 'selected' });
    this.Bookservice.push({ Title: 'Every 4 Weeks', DayClass: 'selected' });

    this.Home.push({ homekey: 'Someone is Home', DayClass: 'Today', isopen: false });
    this.Home.push({ homekey: 'Doorman', DayClass: 'selected', isopen: false });
    this.Home.push({ homekey: 'Hidden Key', DayClass: 'selected ', isopen: true });
    this.Home.push({ homekey: 'Other', DayClass: 'selected ', isopen: true });

  }
  ChangeService(Title: string) {
    let itemlist: any;
    this.Typeserivce = Title;
    sessionStorage.setItem("Typeserivce", this.Typeserivce);
    this._commonService.Typeserivce = this.Typeserivce;

    this.Bookservice.forEach((element, index) => {
      if (element.Title == Title) {
        itemlist = { Title: element.Title, DayClass: 'Today mc-radio' };
      }
      else {
        itemlist = { Title: element.Title, DayClass: 'selected' };
      }
      this.Bookservice[index] = itemlist;
    });
  }
  homeService(homekey: string, isopen: boolean) {
    this.HomeType = homekey;
    this._commonService.keyInfo = this.HomeType;
    sessionStorage.setItem('keyInfo', this.HomeType);
    let itemlist: any;
    this.Home.forEach((element, index) => {
      if (element.homekey == homekey) {
        if (element.isopen) {
          itemlist = { homekey: element.homekey, DayClass: 'Today mc-radio ', isopen: element.isopen };
          // console.log('4', itemlist);
        } else {
          itemlist = { homekey: element.homekey, DayClass: 'Today mc-radio ', isopen: element.isopen };
        }
      }
      else {
        itemlist = { homekey: element.homekey, DayClass: 'selected ', isopen: element.isopen };
      }
      this.Home[index] = itemlist;
    });
    if (isopen) {
      this.textbox = true;

    } else {
      this.textbox = false;
    }
  }
  onSubmit(detail: any): void {
    if (!detail.valid) {
      return;
    }
    this.UserAdd.userId = sessionStorage.getItem("userId");
    this.UserAdd.Apartment = sessionStorage.getItem("Apartment");
    this.UserAdd.Address = sessionStorage.getItem("Address");
    this.UserAdd.ZipCode = sessionStorage.getItem("ZipCode");
    this.UserAdd.serviceTime = sessionStorage.getItem("Typeserivce");
    this.UserAdd.keyInfo = sessionStorage.getItem('keyInfo');

    this.AddUser.postAddUser(this.UserAdd).subscribe(model => {
      this.apiData = model;
    });

    if (sessionStorage.getItem("userId") !== null) {
     // this.dialog.open(PaymentDetailComponent);
      this.router.navigate(['/payment-detail']);
    }
    else {
      this.dialog.open(UserareaComponent)
    }
  }
  Previous() {
    this.router.navigate(['/day-time']);
  }
  // For Pin Number Format
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  //for google APi
  setAddress(addrObj) {

    this.Address = addrObj.formatted_address;
    let Aptmnt1 = this.Address.split(',');
    let Aptmnt2 = Aptmnt1[0] + Aptmnt1[1];
    sessionStorage.setItem('Apartment', Aptmnt2);
    this.UserAdd.Apartment = Aptmnt2;
    // console.log('Aptmnt2', Aptmnt2);
    sessionStorage.setItem('Address', this.Address);
    let Add1 = this.Address.split(',');
    // this.UserAdd.Address = Add1[2] + Add1[3] + Add1[4];
    // let pass = Add1[2] + Add1[3];
    // for Splite Address
    let Adrs = Add1[2] + Add1[3] + Add1[4] + Add1[5] + Add1[6];
    if (Add1[2]) {
      Adrs = (Add1)[2]
    }
    if (Add1[3]) {
      Adrs = Adrs + (Add1)[3]
    }
    if (Add1[4]) {
      Adrs = Adrs + (Add1)[4]
    }
    if (Add1[5]) {
      Adrs = Adrs + (Add1)[5]
    }
    if (Add1[6]) {
      Adrs = Adrs + (Add1)[6]
    }
    this.UserAdd.Address = Adrs;
    sessionStorage.setItem('AdrsForReceipt',  this.UserAdd.Address);


    this._commonService.address = this.Address;
    this.postal = addrObj.postal_code;
    sessionStorage.setItem('ZipCode', this.postal);
    this.zone.run(() => {
      //for pincode
      this.UserAdd.ZipCode = addrObj.postal_code;

    });

  }

}
