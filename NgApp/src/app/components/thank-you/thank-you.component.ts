import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common-service.service';
import { element } from 'protractor';

@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.less']
})
export class ThankYouComponent implements OnInit {

  constructor(public _commonService: CommonService) { }

  ngOnInit() {
    sessionStorage.getItem('un');
    sessionStorage.getItem('userId');
    this._commonService.date = sessionStorage.removeItem("selectedDate");
    this._commonService.time = sessionStorage.removeItem("SetTime");
    this._commonService.services = sessionStorage.removeItem("Number_of_selected_item");
    this._commonService.address = sessionStorage.removeItem("Address");
    sessionStorage.removeItem("id_of_card")
    this._commonService.price = sessionStorage.removeItem("price");
    this._commonService.ZipCode = sessionStorage.removeItem('ZipCode');
    this._commonService.special_Notes = sessionStorage.removeItem('special_Notes');
    sessionStorage.removeItem("itemList");
    sessionStorage.removeItem('selectedcategory')
    this._commonService.keyInfo = sessionStorage.removeItem('keyInfo');
    this._commonService.Typeserivce = sessionStorage.removeItem("Typeserivce");
    sessionStorage.removeItem('GrandTotal');
    sessionStorage.removeItem('__paypal_storage__');
    sessionStorage.removeItem("bookingService");




  }

}
