import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserareaComponent } from './components/userarea/userarea.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SteptwoComponent } from './components/steptwo/steptwo.component';
import { MyorderComponent } from './components/myorder/myorder.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { DayTimeComponent } from './components/day-time/day-time.component';
import { AddressInfoComponent } from './components/address-info/address-info.component';
import { PaymentDetailComponent } from './components/payment-detail/payment-detail.component';
import { ThankYouComponent } from './components/thank-you/thank-you.component';
import { CreatePasswordComponent } from './components/create-password/create-password.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { injectRootLimpMode } from '@angular/core/src/di/injector_compatibility';
import { UserloginComponent } from './components/userlogin/userlogin.component';
import { MasterCategoryComponent } from './components/Admin_Panel/master-category/master-category.component';
import { ForgotPassComponent } from './components/forgot-pass/forgot-pass.component'
import { OrderdetailsComponent } from 'src/app/components/orderdetails/orderdetails.component';


const routes: Routes = [

  { path: 'admin', loadChildren: './components/Admin_Panel/layout.module#LayoutModule' },
  { path: 'create-password', component: CreatePasswordComponent },
  { path: 'thank-you', component: ThankYouComponent },
  { path: 'payment-detail', component: PaymentDetailComponent },
  { path: 'address-info', component: AddressInfoComponent },
  { path: 'day-time', component: DayTimeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'steptwo', component: SteptwoComponent },
  { path: 'calendar', component: CalendarComponent },
  { path: 'userarea', component: UserareaComponent },
  { path: 'myorder', component: MyorderComponent },
  { path: 'orderdetails', component: OrderdetailsComponent },
  { path: 'orderdetails/:id', component: OrderdetailsComponent },
  { path: '**', component: DashboardComponent },
  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
