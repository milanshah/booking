import { MbscModule } from '@mobiscroll/angular-lite';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatSelectModule } from '@angular/material/select';
import { HeaderComponent } from './components/header/header.component';
import { WINDOW_PROVIDERS } from './shared/helpers/window.helper';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatInputModule } from '@angular/material';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PortalModule } from '@angular/cdk/portal';
import { MatTabsModule } from '@angular/material/tabs';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { MatChipsModule } from '@angular/material/chips';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { UserareaComponent } from './components/userarea/userarea.component';
import { UserloginComponent } from './components/userlogin/userlogin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SteptwoComponent } from './components/steptwo/steptwo.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { NgxMaskModule, MaskService } from 'ngx-mask';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { CalendarComponent } from './components/calendar/calendar.component';
import { MatMenuModule } from '@angular/material/menu';
import { Ng6NotifyPopupModule } from 'ng6-notify-popup';
import { MatTableModule } from '@angular/material';
import { ForgotPassComponent } from './components/forgot-pass/forgot-pass.component'
import { SlideshowModule } from 'ng-simple-slideshow';
import { DayTimeComponent } from './components/day-time/day-time.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AddressInfoComponent } from './components/address-info/address-info.component';
import { Header2Component } from './components/header2/header2.component';
import { Ng2OdometerModule } from 'ng2-odometer';
import { DragScrollModule } from 'ngx-drag-scroll';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { GooglePlacesDirective } from './shared/google-api/google-places.directive';
import { PaymentDetailComponent } from './components/payment-detail/payment-detail.component';
import { NgxPayPalModule } from 'ngx-paypal';
import { ThankYouComponent } from './components/thank-you/thank-you.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { CreatePasswordComponent } from './components/create-password/create-password.component';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MatSidenavModule } from '@angular/material';
import { DemoComponent } from './components/demo/demo.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import{UserComponent} from '../app/components/Admin_Panel/user/user.component';
import { MyorderComponent } from 'src/app/components/myorder/myorder.component';
//import { SettingComponent } from 'src/app/components/Admin_Panel/setting/setting.component';
import {DatePipe} from '@angular/common';
import { OrderdetailsComponent } from 'src/app/components/orderdetails/orderdetails.component';
import { OrdercancelpopupComponent } from './components/ordercancelpopup/ordercancelpopup.component';
// import { CategoryAddComponent } from './components/Admin_Panel/category-add/category-add.component';



export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent, HeaderComponent, RegistrationComponent,
    LoginComponent, UserareaComponent, UserloginComponent,
    DashboardComponent, SteptwoComponent, CalendarComponent,
    ForgotPassComponent, DayTimeComponent, ProfileComponent,
    AddressInfoComponent, Header2Component, GooglePlacesDirective,
    PaymentDetailComponent, ThankYouComponent, ChangePasswordComponent,
    CreatePasswordComponent, 
    DemoComponent, MyorderComponent, OrderdetailsComponent, OrdercancelpopupComponent 


  ],
  imports: [
    MbscModule, MatNativeDateModule, NgxPayPalModule, BrowserModule,
    MatTableModule, MatDatepickerModule, AppRoutingModule, PortalModule,
    MatButtonToggleModule, MatDialogModule, SlideshowModule,
    NgxPasswordToggleModule, HttpClientModule, Ng6NotifyPopupModule,
    MatIconModule, MatSidenavModule, MatToolbarModule, MatProgressBarModule,
    MatInputModule, ToastrModule, Ng2OdometerModule.forRoot(), MatTabsModule,
    MatRadioModule, NgxMaskModule.forRoot(), CommonModule, Ng6NotifyPopupModule.forRoot(),
    MatListModule, MatMenuModule, MatSnackBarModule, MatSlideToggleModule,
    ToastrModule.forRoot(),MatTooltipModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule, FormsModule, DragDropModule, ScrollDispatchModule,
    ReactiveFormsModule, MatSelectModule, MatSelectModule, MatFormFieldModule, MatChipsModule,
    MatCardModule, CommonModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatButtonModule, MatCheckboxModule, FlexLayoutModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader, useFactory: HttpLoaderFactory, deps: [HttpClient]
      }
    }),

    DragScrollModule
  ],

  providers: [WINDOW_PROVIDERS,DatePipe, MaskService, ToastrService, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  entryComponents: [ProfileComponent, UserloginComponent, ChangePasswordComponent,OrdercancelpopupComponent, 
    UserareaComponent, RegistrationComponent,ForgotPassComponent, LoginComponent],
  exports: [GooglePlacesDirective],
  bootstrap: [AppComponent],
  // schemas: [
                
  //   NO_ERRORS_SCHEMA
  // ]
})
export class AppModule { }
