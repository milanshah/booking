USE [master]
GO
/****** Object:  Database [myclean]    Script Date: 03-01-2020 12:34:22 ******/
CREATE DATABASE [myclean]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'myclean', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MTSQLEXPRESS\MSSQL\DATA\myclean.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'myclean_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MTSQLEXPRESS\MSSQL\DATA\myclean_log.ldf' , SIZE = 1536KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [myclean] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [myclean].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [myclean] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [myclean] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [myclean] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [myclean] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [myclean] SET ARITHABORT OFF 
GO
ALTER DATABASE [myclean] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [myclean] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [myclean] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [myclean] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [myclean] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [myclean] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [myclean] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [myclean] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [myclean] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [myclean] SET  DISABLE_BROKER 
GO
ALTER DATABASE [myclean] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [myclean] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [myclean] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [myclean] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [myclean] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [myclean] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [myclean] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [myclean] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [myclean] SET  MULTI_USER 
GO
ALTER DATABASE [myclean] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [myclean] SET DB_CHAINING OFF 
GO
ALTER DATABASE [myclean] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [myclean] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [myclean] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [myclean] SET QUERY_STORE = OFF
GO
USE [myclean]
GO
/****** Object:  User [developer]    Script Date: 03-01-2020 12:34:22 ******/
CREATE USER [developer] FOR LOGIN [developer] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [developer]
GO
/****** Object:  Table [dbo].[CRUD]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CRUD](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Contact_No] [nvarchar](50) NULL,
 CONSTRAINT [PK_CRUD] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[FirstView]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[FirstView]
as select * from CRUD where FirstName='John'
GO
/****** Object:  Table [dbo].[Address]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[addressId] [int] IDENTITY(1,1) NOT NULL,
	[customerId] [int] NOT NULL,
	[address] [varchar](50) NULL,
	[zipCode] [numeric](18, 0) NULL,
	[Apartment] [nvarchar](100) NULL,
 CONSTRAINT [PK__Address__26A111AD14A4E4DD] PRIMARY KEY CLUSTERED 
(
	[addressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AddUser]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddUser](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[Address] [varchar](200) NULL,
	[ZipCode] [int] NULL,
	[Apartment] [varchar](100) NULL,
	[userId] [int] NULL,
	[serviceTime] [nvarchar](50) NULL,
	[keyInfo] [nvarchar](50) NULL,
	[about_Key] [nvarchar](50) NULL,
	[special_Notes] [nvarchar](200) NULL,
 CONSTRAINT [PK_AddUser] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BookingDetails]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookingDetails](
	[bookingId] [int] IDENTITY(1,1) NOT NULL,
	[totalAmount] [numeric](18, 0) NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[userId] [int] NULL,
	[addressId] [int] NULL,
	[paymentId] [int] NULL,
	[status] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[bookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[categoryId] [int] IDENTITY(1,1) NOT NULL,
	[subcategoryName] [varchar](100) NULL,
	[status] [bit] NULL,
	[masterCategoryId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[categoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[masterCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[categoryName] [varchar](50) NULL,
	[status] [bit] NULL,
	[Type] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[masterCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[payment]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payment](
	[paymentId] [int] IDENTITY(1,1) NOT NULL,
	[paymentType] [varchar](50) NULL,
	[status] [varchar](50) NULL,
	[datetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[paymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaymentDetails]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentDetails](
	[PaymentId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NULL,
	[Credit_Card_No] [nvarchar](50) NULL,
	[Expire_Date] [datetime] NULL,
	[CVV] [int] NULL,
	[CardHolder_Name] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[ContactNo] [int] NULL,
 CONSTRAINT [PK_PaymentDetails] PRIMARY KEY CLUSTERED 
(
	[PaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaymentReceipt]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentReceipt](
	[ReceiptId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[category] [nvarchar](50) NULL,
	[subCategory] [nvarchar](50) NULL,
	[Typeserivce] [nvarchar](50) NULL,
	[date] [nvarchar](50) NULL,
	[keyInfo] [nvarchar](50) NULL,
	[Apartment] [nvarchar](50) NULL,
	[time] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[price] [nvarchar](50) NULL,
	[GrandTotal] [nvarchar](50) NULL,
	[orderID] [nvarchar](50) NULL,
	[TransactionID] [nvarchar](50) NULL,
 CONSTRAINT [PK_PaymentReceipt] PRIMARY KEY CLUSTERED 
(
	[ReceiptId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[policy]    Script Date: 03-01-2020 12:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[policy](
	[policyId] [int] IDENTITY(1,1) NOT NULL,
	[policyName] [varchar](50) NULL,
	[vistingCharge] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[policyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[roleId] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ScheduleTime]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleTime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dayName] [nvarchar](50) NULL,
	[dayNumber] [int] NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[interval] [float] NULL,
 CONSTRAINT [PK_ScheduleTime] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[setting]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[setting](
	[SettingId] [int] IDENTITY(1,1) NOT NULL,
	[parameter] [nvarchar](100) NULL,
	[parameterValue] [nvarchar](max) NULL,
	[type] [nvarchar](20) NULL,
	[templatesubject] [nvarchar](50) NULL,
 CONSTRAINT [PK_setting] PRIMARY KEY CLUSTERED 
(
	[SettingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategories]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategories](
	[SubCategoriesId] [int] IDENTITY(1,1) NOT NULL,
	[categoryId] [int] NULL,
	[subCategoryName] [varchar](100) NULL,
	[status] [bit] NULL,
	[image] [varchar](max) NULL,
	[price] [int] NULL,
	[approxTime] [int] NULL,
	[policyid] [int] NULL,
 CONSTRAINT [PK_SubCategories] PRIMARY KEY CLUSTERED 
(
	[SubCategoriesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_login]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_login](
	[loginId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[userName] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[password] [varchar](50) NULL,
	[roleId] [int] NULL,
 CONSTRAINT [PK_user_login] PRIMARY KEY CLUSTERED 
(
	[loginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[userdetails]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userdetails](
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[userName] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NULL,
	[contactNumber] [nvarchar](50) NULL,
	[email] [nvarchar](50) NOT NULL,
	[address] [varchar](50) NULL,
	[occupation] [varchar](50) NULL,
	[image] [varchar](max) NULL,
	[roleId] [int] NOT NULL,
	[token] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK__userdeta__CB9A1CFF3E2327BF] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AddUser] ON 
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (1, N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', 380015, N'4th Floor Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'sadsaasdsdsa')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (2, N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', 380015, N'4th Floor Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'sadsaasdsdsa')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (3, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'sadsaasdsdsa')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (4, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'hjgjgj')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (5, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'sdsd')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (6, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'bvb')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (7, N'1275 Pennsylvania Ave NW, Washington, DC 20004, US', 20004, N'1275 Pennsylvania Ave NW Washington', 707, N'One Time', N'Someone is Home', NULL, N'cxc')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (8, N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', 380015, N'4th Floor Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'vcbvb')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (9, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'jk')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (10, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'xc')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (11, N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', 380015, N'4th Floor Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'hjgjh')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (12, N'A-6, Staff Quarter, Govt. Polytechnic, Manesar, NH', 122051, N'A-6 Staff Quarter', 895, N'Weekly', N'Doorman', NULL, N'no')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (13, N'A-6, Staff Quarter, Govt. Polytechnic, Manesar, NH', 122051, N'A-6 Staff Quarter', 895, N'One Time', N'Someone is Home', NULL, N'no')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (14, N'Manek Chowk Rd, Old City, Danapidth, Khadia, Ahmed', 380001, N'Manek Chowk Rd Old City', 895, N'One Time', N'Someone is Home', NULL, N'no')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (15, N'Manek Chowk Rd, Old City, Danapidth, Khadia, Ahmed', 380001, N'Manek Chowk Rd Old City', 895, N'One Time', N'Someone is Home', NULL, N'no')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (16, N'Manek Chowk Rd, Old City, Danapidth, Khadia, Ahmed', 380001, N'Manek Chowk Rd Old City', 895, N'Weekly', N'Doorman', NULL, N'no')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (17, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'fdgdf')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (18, N'101 Greenwich St, New York, NY 10006, USA', 10006, N'101 Greenwich St New York', 707, N'One Time', N'Someone is Home', NULL, N'ghfg')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (19, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 707, N'One Time', N'Someone is Home', NULL, N'dss')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (20, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'fgdg')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (21, N'Panch Marg, Mangal Nagar, Versova, Andheri West, M', 400061, N'Panch Marg Mangal Nagar', 895, N'Every 2 Weeks', N'Doorman', NULL, N'no')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (22, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'SADSAD', 895, N'One Time', N'Someone is Home', NULL, N'at door')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (23, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'12121')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (24, N'Bapunagar, Ahmedabad, Gujarat 380038, India', 380038, N'Bapunagar Ahmedabad', 707, N'One Time', N'Someone is Home', NULL, N'cv')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (25, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'ghfg')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (26, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 707, N'One Time', N'Someone is Home', NULL, N'sds')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (27, N'101 Greenwich St, New York, NY 10006, USA', 10006, N'101 Greenwich St New York', 707, N'One Time', N'Someone is Home', NULL, N'dsa')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (28, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 897, N'One Time', N'Someone is Home', NULL, N'121')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (29, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 897, N'One Time', N'Someone is Home', NULL, N'5')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (30, N'1216 5th Ave, New York, NY 10029, USA', 10029, N'1216 5th Ave New York', 897, N'One Time', N'Someone is Home', NULL, N'h')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (31, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, N'cx')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (32, N'135 Madison Ave, New York, NY 10016, USA', 10016, N'135 Madison Ave New York', 897, N'One Time', N'Someone is Home', NULL, N';'''';;')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (33, N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', 380015, N'4th Floor Timber Point', 897, N'One Time', N'Someone is Home', NULL, N'fdsfd')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (34, N'135 Madison Ave, New York, NY 10016, USA', 10016, N'135 Madison Ave New York', 897, N'One Time', N'Someone is Home', NULL, N'fgdfgdf')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (35, N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', 380015, N'4th Floor Timber Point', 897, N'One Time', N'Someone is Home', NULL, N'gfgfd')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (36, N'1216 5th Ave, New York, NY 10029, USA', 10029, N'1216 5th Ave New York', 897, N'Weekly', N'Doorman', NULL, N'14545')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (37, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 897, N'One Time', N'Someone is Home', NULL, N'gdfg')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (38, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 897, N'One Time', N'Someone is Home', NULL, N'ghgh')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (39, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, N'121')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (40, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, N'444')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (41, N'135 Madison Ave, New York, NY 10016, USA', 10016, N'135 Madison Ave New York', 897, N'One Time', N'Someone is Home', NULL, N'lkjl')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (42, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, N'2323')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (43, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 897, N'One Time', N'Someone is Home', NULL, N'k')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (44, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 897, N'One Time', N'Someone is Home', NULL, N'vcv')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (45, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, N'4141')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (46, N'1484 3rd Ave, New York, NY 10028, USA', 10028, N'1484 3rd Ave New York', 897, N'One Time', N'Someone is Home', NULL, N'14')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (47, N'1441 Broadway, New York, NY 10018, USA', 10018, N'1441 Broadway New York', 897, N'One Time', N'Someone is Home', NULL, N'41')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (48, N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', 380015, N'4th Floor Timber Point', 895, N'One Time', N'Someone is Home', NULL, N'sdf')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (49, N'245 Park Ave, New York, NY 10029, USA', 10029, N'245 Park Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (50, N'135 Madison Ave, New York, NY 10016, USA', 10016, N'135 Madison Ave New York', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (51, N'135 Madison Ave, New York, NY 10016, USA', 10016, N'135 Madison Ave New York', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (52, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (53, N'135 Madison Ave, Brentwood, NY 11717, USA', 11717, N'135 Madison Ave Brentwood', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (54, N'183 Madison Ave, New York, NY 10016, USA', 10016, N'183 Madison Ave New York', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (55, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 906, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (56, N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', 2118, N'1 1010 Massachusetts Ave', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (57, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (58, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (59, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (60, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (61, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (62, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (63, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (64, N'1623 3rd Ave, New York, NY 10128, USA', 10128, N'1623 3rd Ave New York', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (65, N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', 380015, N'4th Floor Timber Point', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (66, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (67, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (68, N'1095 6th Ave, New York, NY 10036, USA', 10036, N'1095 6th Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (69, N'1250 Waters Pl, The Bronx, NY 10461, USA', 10461, N'1250 Waters Pl The Bronx', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (70, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (71, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 916, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (72, N'1250 Waters Pl, The Bronx, NY 10461, USA', 10461, N'1250 Waters Pl The Bronx', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (73, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (74, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (75, N'107 A Jolly Rd, Deville, LA 71328, USA', 71328, N'107 A Jolly Rd Deville', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (76, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (77, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (78, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (79, N'150 E 42nd St, New York, NY 10017, USA', 10017, N'150 E 42nd St New York', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (80, N'Janta Nagar Kakoldiya Rd, Vishwas City 1, Chanakya', 380061, N'Janta Nagar Kakoldiya Rd Vishwas City 1', 916, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (81, N'101 Federal St, Boston, MA 02110, USA', 2110, N'101 Federal St Boston', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (82, N'Gujarat Soc. Road, Gujarat Society, Cooperative Ho', 364290, N'Gujarat Soc. Road Gujarat Society', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (83, N'Gujarat 383305, India', 383305, N'Gujarat 383305 India', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (84, N'Manek Chowk, Old City, Khadia, Ahmedabad, Gujarat ', 380001, N'Manek Chowk Old City', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (85, N'Nadiadwala Colony Rd Number 2, Malad, Nadiyawala C', 400064, N'Nadiadwala Colony Rd Number 2 Malad', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (86, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 895, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (87, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (88, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (89, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (90, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (91, N'Ahmedabad, Gujarat 382350, India', 382350, N'Ahmedabad Gujarat 382350', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (92, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (93, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (94, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (95, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 728, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (96, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 728, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (97, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 728, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (98, N'100 Centre St, New York, NY 10013, USA', 10013, N'100 Centre St New York', 728, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (99, N'Electric Ave, Sparks, NV 89434, USA', 89434, N'Electric Ave Sparks', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (100, N'1500 Market St, Philadelphia, PA 19107, USA', 19107, N'1500 Market St Philadelphia', 728, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (101, N'1468 Madison Ave, New York, NY 10029, USA', 10029, N'1468 Madison Ave New York', 728, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (102, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (103, N'1212 4th St SE, Washington, DC 20003, USA', 20003, N'1212 4th St SE Washington', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (104, N'112 Ocean Ave, Amityville, NY 11701, USA', 11701, N'112 Ocean Ave Amityville', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (105, N'100 Centre St, New York, NY 10013, USA', 10013, N'100 Centre St New York', 728, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (106, N'1468 Madison Ave, New York, NY 10029, USA', 10029, N'1468 Madison Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (107, N'100 Centre St, New York, NY 10013, USA', 10013, N'100 Centre St New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (108, N'1468 Madison Ave, New York, NY 10029, USA', 10029, N'1468 Madison Ave New York', 897, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (109, N'2400 Aviation Dr, DFW Airport, TX 75261, USA', 75261, N'2400 Aviation Dr DFW Airport', 927, N'One Time', N'Someone is Home', NULL, NULL)
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (110, N'Desai Nagar, Bhavnagar Para, Bhavnagar, Gujarat 36', 364004, N'Desai Nagar Bhavnagar Para', 916, N'One Time', N'Someone is Home', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[AddUser] OFF
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (2, N'Switches', 1, 8)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (3, N'Fans', 1, 1)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (4, N'Blocks & Leakages', 1, 2)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (5, N'Bathroom Fittings', 1, 2)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (6, N'Home Cleaning', 0, 3)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (7, N'Apartment', 1, 4)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (8, N'Bungalow', 1, 4)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (9, N'Offices', 1, 4)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (38, N'fdfds', 1, 31)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (39, N'Service 455', 1, 8)
GO
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[CategoryMaster] ON 
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (1, N'Electrician', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (2, N'Plumber', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (3, N'Cleaner', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (4, N'Clean', 1, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (8, N'Bunglows', 1, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (31, N'Mason', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (152, N'Driver', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (153, N'Carpenter', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (154, N'Cook', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (155, N'Painter', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (156, N'Maid', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (157, N'PestControl', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (158, N'Event', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (159, N'Astrologer', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (160, N'Doctor', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (161, N'Advocate', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (163, N'Office', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (164, N'Kitchen', 0, N'CategoryMaster')
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (193, N'vcvcx', 0, NULL)
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status], [Type]) VALUES (194, N'sdfdf', 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[CategoryMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[CRUD] ON 
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (2, N'john', N'john', N'john@gmail.com', N'123456789')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (4, N'Naitik', N'Bhatt', N'naitik@gmail.com', N'123456782')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (5, N'Hardik', N'Panchal', N'hardik@gmail.com', N'123456789')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (11, N'kathan', N'shah', N'test@123.com', N'1234567890')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (13, N'Jayeshbhai', N'Jayeshbhai', N'bgdzfrg@dfv.okk', N'1234567892')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (14, N'Jayeshbhai', N'Jayeshbhai', N'bgdzfrg@dfv.okk', N'1234567892')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (15, N'Jaye', N'hvghnb', N'sgahdvxcfyhjg@jsadhv.ckhj', N'534')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (16, N'erft', N're', N'er', N'rew')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (17, N'wqwq', N'wqwq', N'wqwq', N'1212')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (18, N'tghj', N'lkjk', N'bgdzfrg@dfv.okk', N'1234567892')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (19, N'naitik', N'bhatt', N'naitikbhatt25@gmail.com', N'95474845')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (20, N'nan', N'nana', N'nanan', N'anna')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (21, N'dcsa', N'sdsa', N'sadas', N'sada')
GO
INSERT [dbo].[CRUD] ([id], [FirstName], [LastName], [Email], [Contact_No]) VALUES (22, N'ww', N'ww', N'ww', N'ww')
GO
SET IDENTITY_INSERT [dbo].[CRUD] OFF
GO
SET IDENTITY_INSERT [dbo].[payment] ON 
GO
INSERT [dbo].[payment] ([paymentId], [paymentType], [status], [datetime]) VALUES (2, N'online', N'Success', CAST(N'1900-01-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[payment] ([paymentId], [paymentType], [status], [datetime]) VALUES (3, N'online', N'Success', CAST(N'1900-01-12T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[payment] OFF
GO
SET IDENTITY_INSERT [dbo].[PaymentDetails] ON 
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (8, 704, N'32132121', CAST(N'2019-07-10T00:00:00.000' AS DateTime), 32132, N'Kathan', N'test@gmail.com', 3131510)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (9, 707, N'1235555555555555', CAST(N'2019-07-09T18:30:00.000' AS DateTime), 123, N'hardik', N'hardik@gmail.com', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (10, 707, N'1235555555555555', CAST(N'2019-07-11T18:30:00.000' AS DateTime), 123, N'hardik panchal', N'hardik1@gmail.com', 1234566789)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (11, 707, N'2131113331513151', CAST(N'2019-07-17T18:30:00.000' AS DateTime), 123, N'hardik', N'hardik@gmail.com', 1211313131)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (12, 707, N'1234567891234567', CAST(N'2019-07-17T18:30:00.000' AS DateTime), 456, N'hardik', N'hardik@gmail.com', 1234567891)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (13, 704, N'1312312312312312', CAST(N'2019-07-08T18:30:00.000' AS DateTime), 123, N'johny bhai', N'john@gmail.com', 1231231231)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (24, 707, N'2222222222222222', CAST(N'2019-07-18T18:30:00.000' AS DateTime), 111, N'1111111111111', N'11111111111111', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (25, 704, N'3232323232323232', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 321, N'kathan shah', N'kathan@gmail.com', 2132132132)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (27, 707, N'string', CAST(N'2019-07-23T10:43:48.627' AS DateTime), 0, N'string', N'string', 0)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (28, 707, N'0515153158131361', CAST(N'2019-07-23T18:30:00.000' AS DateTime), 454, N'213213123123123123', N'13123123213123123123', 1231231232)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (29, 707, N'0515153158131361', CAST(N'2019-07-23T18:30:00.000' AS DateTime), 454, N'213213123123123123', N'13123123213123123123', 1231231232)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (30, 707, N'1321153151111531', CAST(N'2019-07-10T18:30:00.000' AS DateTime), 113, N'211131121111321313211', N'111212132131', 1132131321)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (31, 707, N'1321153151111531', CAST(N'2019-07-10T18:30:00.000' AS DateTime), 113, N'211131121111321313211', N'111212132131', 1132131321)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (32, 707, N'1321153151111531', CAST(N'2019-07-10T18:30:00.000' AS DateTime), 113, N'211131121111321313211', N'111212132131', 1132131321)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (33, 707, N'2132321312312312', CAST(N'2019-07-17T18:30:00.000' AS DateTime), 312, N'12312312312', N'123123123', 1312312323)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (34, 707, N'string', CAST(N'2019-07-24T04:44:44.277' AS DateTime), 0, N'string', N'string', 0)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (35, 707, N'2323231231231231', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 131, N'3123123123', N'123123123', 1312313123)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (36, 707, N'3263131231231231', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 111, N'312312321312', N'3123123', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (37, 707, N'3263131231231231', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 111, N'312312321312', N'3123123', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (38, 707, N'3263131231231231', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 111, N'312312321312', N'3123123', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (39, 707, N'2121212121212121', CAST(N'2019-07-16T18:30:00.000' AS DateTime), 121, N'1212121', N'12121', 1212121212)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (40, 707, N'1111111111111111', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 111, N'1111111111111111', N'1111111111111', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (43, 707, N'1111111111111111', CAST(N'2019-07-16T18:30:00.000' AS DateTime), 111, N'11111111', N'1111111', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (44, 707, N'string', CAST(N'2019-07-23T10:43:48.627' AS DateTime), 123, N'string', N'string', 1234567892)
GO
SET IDENTITY_INSERT [dbo].[PaymentDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[PaymentReceipt] ON 
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (1, 704, N'string', N'string', N'string', N'Jul 23 2019 11:34AM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (124, 707, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Aug 22 2019 12:00AM', N'Someone is Home', NULL, N'04:00 PM', N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', N'2200', N'2596', N'46X10234EM739224F', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (125, 707, N'Electician', N'["Service 1","Service 2"]', N'One Time', N'Aug 15 2019 12:00AM', N'Someone is Home', N'4th Floor Timber Point', N'01:30 PM', N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', N'2200', N'2596', N'1K70478027185320U', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (126, 707, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Aug 23 2019 12:00AM', N'Someone is Home', N'1 1010 Massachusetts Ave', N'05:30 PM', N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', N'2200', N'2596', N'69F42696FS153940H', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (127, 707, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Aug 22 2019 12:00AM', N'Someone is Home', N'1 1010 Massachusetts Ave', N'01:30 PM', N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', N'2200', N'2596', N'7WS79525LA753432J', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (128, 707, N'Electician', N'["Service 3"]', N'One Time', N'Aug 23 2019 12:00AM', N'Someone is Home', N'1 1010 Massachusetts Ave', N'02:30 PM', N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', N'1200', N'1416', N'6NH21515HV7730100', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (129, 897, N'Electician', N'["Service 1","Service 2","Service 3"]', N'One Time', N'Aug 23 2019 12:00AM', N'Someone is Home', N'1 1010 Massachusetts Ave', N'02:30 PM', N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', N'3612', N'4282.16', N'5AA96412DD923751T', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (130, 897, N'Electician', N'["Service 1","Service 2"]', N'One Time', N'Aug 22 2019 12:00AM', N'Someone is Home', N'1 1010 Massachusetts Ave', N'04:00 PM', N'1, 1010 Massachusetts Ave, Boston, MA 02118, USA', N'2200', N'2596', N'9H007549KK466203G', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (131, 897, N'Clean', N'["1 BHK","2 BHK"]', N'One Time', N'Aug 14 2019 12:00AM', N'Someone is Home', N'1095 6th Ave New York', N'02:30 PM', N'1095 6th Ave, New York, NY 10036, USA', N'2900', N'3442', N'2WN132987H555262T', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (132, 895, N'Clean', N'["3 BHK","4 BHK"]', N'One Time', N'Aug 23 2019 12:00AM', N'Someone is Home', N'183 Madison Ave New York', N'11:30 AM', N'183 Madison Ave, New York, NY 10016, USA', N'4500', N'5330', N'6MB707255W6460518', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (133, 897, N'Clean', N'["1 BHK"]', N'One Time', N'Aug 22 2019 12:00AM', N'Someone is Home', N'1095 6th Ave New York', N'01:30 PM', N'1095 6th Ave, New York, NY 10036, USA', N'1300', N'1534', N'8BG13699Y5323584P', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (134, 897, N'Clean', N'["2 BHK"]', N'One Time', N'Aug 31 2019 12:00AM', N'Someone is Home', N'Av. Callao 1065 C1023AAD CABA', N'03:30 PM', N'Av. Callao 1065, C1023AAD CABA, Argentina', N'1600', N'1888', N'3CG214454U529673X', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (135, 916, N'Clean', N'["4 BHK"]', N'One Time', N'Aug 22 2019 12:00AM', N'Someone is Home', N'112 Ocean Ave Amityville', N'04:00 PM', N'112 Ocean Ave, Amityville, NY 11701, USA', N'2500', N'2950', N'77U219924G3105726', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (136, 895, N'Clean', N'["3 BHK","4 BHK"]', N'One Time', N'Aug 30 2019 12:00AM', N'Someone is Home', N'1250 Waters Pl The Bronx', N'08:30 PM', N'1250 Waters Pl, The Bronx, NY 10461, USA', N'4500', N'5310', N'08K63895BH9298903', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (137, 895, N'Clean', N'["4 BHK"]', N'One Time', N'Aug 23 2019 12:00AM', N'Someone is Home', N'112 Ocean Ave Amityville', N'05:30 PM', N'112 Ocean Ave, Amityville, NY 11701, USA', N'2500', N'2950', N'3TF33114HP9882621', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (138, 897, N'Clean', N'["2 BHK","3 BHK","4 BHK"]', N'One Time', N'Aug 28 2019 12:00AM', N'Someone is Home', N'112 Ocean Ave Amityville', N'02:30 PM', N'112 Ocean Ave, Amityville, NY 11701, USA', N'6100', N'7198', N'9BV9760666400201L', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (139, 897, N'Clean', N'["1 BHK","2 BHK","3 BHK","4 BHK"]', N'One Time', N'Aug 22 2019 12:00AM', N'Someone is Home', N'112 Ocean Ave Amityville', N'04:00 PM', N'112 Ocean Ave, Amityville, NY 11701, USA', N'7400', N'8732', N'2KK05915S0999681L', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (140, 916, N'Clean', N'["Duplex","Villa","Penthouse"]', N'One Time', N'Aug 23 2019 12:00AM', N'Someone is Home', N'Janta Nagar Kakoldiya Rd Vishwas City 1', N'02:30 PM', N'Janta Nagar Kakoldiya Rd, Vishwas City 1, Chanakya', N'3690', N'4369.2', N'80618911JJ937853D', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (141, 895, N'Clean', N'["3 BHK","4 BHK"]', N'One Time', N'Aug 23 2019 12:00AM', N'Someone is Home', N'Nadiadwala Colony Rd Number 2 Malad', N'05:30 PM', N'Nadiadwala Colony Rd Number 2, Malad, Nadiyawala C', N'4500', N'5310', N'75B70313U5470100Y', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (142, 895, N'Clean', N'["1 BHK","2 BHK","3 BHK","4 BHK"]', N'One Time', N'Aug 30 2019 12:00AM', N'Someone is Home', N'112 Ocean Ave Amityville', N'08:30 PM', N'112 Ocean Ave, Amityville, NY 11701, USA', N'7400', N'8732', N'8N789663EJ4916508', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (143, 895, N'Clean', N'["3 BHK"]', N'One Time', N'Aug 30 2019 12:00AM', N'Someone is Home', N'Cerrito 1010 C1010AAV CABA', N'08:30 PM', N'Cerrito 1010, C1010AAV CABA, Argentina', N'2000', N'2360', N'4WF18453UV7186159', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (144, 895, N'Clean', N'["2 BHK","3 BHK"]', N'One Time', N'Aug 23 2019 12:00AM', N'Someone is Home', N'Cerrito 1010 C1010AAV CABA', N'05:30 PM', N'Cerrito 1010, C1010AAV CABA, Argentina', N'3600', N'4248', N'3Y019146RN9102943', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (145, 897, N'Clean', N'["4 BHK"]', N'One Time', N'Aug 31 2019 12:00AM', N'Someone is Home', N'112 Ocean Ave Amityville', N'07:00 PM', N'112 Ocean Ave, Amityville, NY 11701, USA', N'2500', N'2950', N'22415571M07387538', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (146, 897, N'Clean', N'["3 BHK","4 BHK"]', N'One Time', N'Aug 30 2019 12:00AM', N'Someone is Home', N'Av. Callao 1065 C1023AAD CABA', N'02:30 PM', N'Av. Callao 1065, C1023AAD CABA, Argentina', N'4500', N'5310', N'6LJ13155LD683945T', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (147, 897, N'Clean', N'["1 BHK","2 BHK","3 BHK","4 BHK"]', N'One Time', N'Aug 30 2019 12:00AM', N'Someone is Home', N'Av. Callao 1065 C1023AAD CABA', N'08:30 PM', N'Av. Callao 1065, C1023AAD CABA, Argentina', N'7400', N'8737', N'0B049703VM9458424', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (148, 707, N'Clean', N'["1 BHK","2 BHK","3 BHK"]', N'One Time', N'Sep 26 2019 12:00AM', N'Someone is Home', N'Av. Callao 1065 C1023AAD CABA', N'01:30 PM', N'Av. Callao 1065, C1023AAD CABA, Argentina', N'4900', N'5782', N'0CY08063TD720042H', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (149, 897, N'Clean', N'["3 BHK"]', N'One Time', N'Sep 24 2019 12:00AM', N'Someone is Home', N'112 Ocean Ave Amityville', N'01:00 PM', N'112 Ocean Ave, Amityville, NY 11701, USA', N'2000', N'2360', N'88267063PU096572X', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (150, 897, N'Clean', N'["1 BHK","2 BHK","3 BHK"]', N'One Time', N'Sep 25 2019 12:00AM', N'Someone is Home', N'Cerrito 1010 C1010AAV CABA', N'04:30 PM', N'Cerrito 1010, C1010AAV CABA, Argentina', N'4900', N'5802', N'0B431941AL7184114', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (151, 897, N'Clean', N'["1 BHK","2 BHK","3 BHK","4 BHK"]', N'One Time', N'Sep 25 2019 12:00AM', N'Someone is Home', N'112 Ocean Ave Amityville', N'12:30 PM', N'112 Ocean Ave, Amityville, NY 11701, USA', N'8340', N'9861.2', N'47U35311DL177281U', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (152, 927, N'Electrician', N'["Service 1"]', N'One Time', N'Oct 17 2019 12:00AM', N'Someone is Home', N'Perdriel 74 C1280AEB CABA', N'01:30 PM', N'Perdriel 74, C1280AEB CABA, Argentina', N'1200', N'1416', N'28173906M56949046', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (153, 897, N'Electrician', N'["1 BHK","2 BHK","3 BHK","4 BHK"]', N'One Time', N'Oct 17 2019 12:00AM', N'Someone is Home', N'100 Centre St New York', N'01:30 PM', N'100 Centre St, New York, NY 10013, USA', N'8340', N'9841.2', N'4SD90244F3699922C', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (154, 897, N'Clean', N'["Duplex","Villa","Penthouse","Row Houses"]', N'One Time', N'Oct 24 2019 12:00AM', N'Someone is Home', N'1468 Madison Ave New York', N'01:30 PM', N'1468 Madison Ave, New York, NY 10029, USA', N'4765', N'5622.7', N'82V070955Y7942133', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (155, 916, N'Clean', N'["1 BHK","2 BHK","3 BHK","4 BHK"]', N'One Time', N'Dec 31 2019 12:00AM', N'Doorman', N'Ghatlodiya Chanakyapuri', N'11:30 AM', N'Ghatlodiya, Chanakyapuri, Ahmedabad, Gujarat, Indi', N'8340', N'9861.2', N'3RF853128E4192829', N'RE74FMTFHYPSS')
GO
SET IDENTITY_INSERT [dbo].[PaymentReceipt] OFF
GO
SET IDENTITY_INSERT [dbo].[policy] ON 
GO
INSERT [dbo].[policy] ([policyId], [policyName], [vistingCharge]) VALUES (1, N'standared', CAST(96 AS Numeric(18, 0)))
GO
INSERT [dbo].[policy] ([policyId], [policyName], [vistingCharge]) VALUES (2, N'standaredplus', CAST(99 AS Numeric(18, 0)))
GO
SET IDENTITY_INSERT [dbo].[policy] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 
GO
INSERT [dbo].[Role] ([roleId], [name]) VALUES (1, N'customer')
GO
INSERT [dbo].[Role] ([roleId], [name]) VALUES (2, N'employee')
GO
INSERT [dbo].[Role] ([roleId], [name]) VALUES (3, N'admin')
GO
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[ScheduleTime] ON 
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (1, N'Sun', 0, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 0.5)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (2, N'Mon', 1, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 1)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (3, N'Tue', 2, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 1.5)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (4, N'Wed', 3, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 2)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (5, N'Thu', 4, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 2.5)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (6, N'Fri', 5, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 3)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (7, N'Sat', 6, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 3.5)
GO
SET IDENTITY_INSERT [dbo].[ScheduleTime] OFF
GO
SET IDENTITY_INSERT [dbo].[setting] ON 
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (1, N'host', N'CHANGEd', N'smtp', NULL)
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (2, N'host', N'CHANGEd', N'smtp', NULL)
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (3, N'typescript', N'category', N'smtp', NULL)
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (4, N'MailFrom', N'demoinsta559@gmail.com', N'smtp', NULL)
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (5, N'SmtpServer', N'smtp.gmail.com', N'smtp', NULL)
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (6, N'SmtpUserName', N'piza556@yahoomail.com', N'smtp', NULL)
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (7, N'SmtpPassword', N'Super#17mfd', N'smtp', NULL)
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (8, N'CreateStaff', N'                <!-- NAME: TELL A STORY -->    <!--[if gte mso 15]>    <xml>     <o:OfficeDocumentSettings>     <o:AllowPNG/>     <o:PixelsPerInch>96</o:PixelsPerInch>     </o:OfficeDocumentSettings>    </xml>   <![endif]-->   <meta charset="UTF-8">   <meta http-equiv="X-UA-Compatible" content="IE=edge">   <meta name="viewport" content="width=device-width, initial-scale=1">   <title>Kubosh Law</title>     <style type="text/css">   p{    margin:10px 0;    padding:0;   }   table{    border-collapse:collapse;   }   h1,h2,h3,h4,h5,h6{    display:block;    margin:0;    padding:0;   }   img,a img{    border:0;    height:auto;    outline:none;    text-decoration:none;   }   body,#bodyTable,#bodyCell{    height:100%;    margin:0;    padding:0;    /*width:100%;*/ width:auto;    /*border: 1px solid #1e40ac;*/   }   .mcnPreviewText{    display:none !important;   }   #outlook a{    padding:0;   }   img{    -ms-interpolation-mode:bicubic;   }   table{    mso-table-lspace:0pt;    mso-table-rspace:0pt;   }   .ReadMsgBody{    width:100%;   }   .ExternalClass{    width:100%;   }   p,a,li,td,blockquote{    mso-line-height-rule:exactly;   }   a[href^=tel],a[href^=sms]{    color:inherit;    cursor:default;    text-decoration:none;   }   p,a,li,td,body,table,blockquote{    -ms-text-size-adjust:100%;    -webkit-text-size-adjust:100%;   }   body{ background: #eee; }   .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{    line-height:100%;   }   a[x-apple-data-detectors]{    color:inherit !important;    text-decoration:none !important;    font-size:inherit !important;    font-family:inherit !important;    font-weight:inherit !important;    line-height:inherit !important;   }   .templateContainer{    max-width:600px !important;   }   a.mcnButton{    display:block;   }   .mcnImage,.mcnRetinaImage{    vertical-align:bottom;   }   .mcnTextContent{    word-break:break-word;   }   .mcnTextContent img{    height:auto !important;   }   .mcnDividerBlock{    table-layout:fixed !important;   }   /*   @tab Page   @section Heading 1   @style heading 1   */   h1{    /*@editable*/color:#222222;    /*@editable*/font-family:Helvetica;    /*@editable*/font-size:40px;    /*@editable*/font-style:normal;    /*@editable*/font-weight:bold;    /*@editable*/line-height:150%;    /*@editable*/letter-spacing:normal;    /*@editable*/text-align:center;   }   /*   @tab Page   @section Heading 2   @style heading 2   */   h2{    /*@editable*/color:#222222;    /*@editable*/font-family:Helvetica;    /*@editable*/font-size:34px;    /*@editable*/font-style:normal;    /*@editable*/font-weight:bold;    /*@editable*/line-height:150%;    /*@editable*/letter-spacing:normal;    /*@editable*/text-align:left;   }   /*   @tab Page   @section Heading 3   @style heading 3   */   h3{    /*@editable*/color:#011a27;    /*@editable*/font-family:Helvetica;    /*@editable*/font-size:22px;    /*@editable*/font-style:normal;    /*@editable*/font-weight:bold;    /*@editable*/line-height:150%;    /*@editable*/letter-spacing:normal;    /*@editable*/text-align:left;   }   /*   @tab Page   @section Heading 4   @style heading 4   */   h4{    /*@editable*/color:#999999;    /*@editable*/font-family:Georgia;    /*@editable*/font-size:20px;    /*@editable*/font-style:italic;    /*@editable*/font-weight:normal;    /*@editable*/line-height:125%;    /*@editable*/letter-spacing:normal;    /*@editable*/text-align:left;   }   /*   @tab Header   @section Header Container Style   */   #templateHeader{    /*@editable*/background-color:#F2F3F6;    /*@editable*/background-image:none;    /*@editable*/background-repeat:no-repeat;    /*@editable*/background-position:center;    /*@editable*/background-size:cover;    /*@editable*/border-top:0;    /*@editable*/border-bottom:0;    /*@editable*/padding-top:30px;    /*@editable*/padding-bottom:30px;   }   /*   @tab Header   @section Header Interior Style   */   .headerContainer{    /*@editable*/background-color:transparent;    /*@editable*/background-image:none;    /*@editable*/background-repeat:no-repeat;    /*@editable*/background-position:center;    /*@editable*/background-size:cover;    /*@editable*/border-top:0;    /*@editable*/border-bottom:0;    /*@editable*/padding-top:0;    /*@editable*/padding-bottom:0;   }   /*   @tab Header   @section Header Text   */   .headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{    /*@editable*/color:#808080;    /*@editable*/font-family:Helvetica;    /*@editable*/font-size:16px;    /*@editable*/line-height:150%;    /*@editable*/text-align:left;   }   /*   @tab Header   @section Header Link   */   .headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{    /*@editable*/color:#00ADD8;    /*@editable*/font-weight:normal;    /*@editable*/text-decoration:underline;   }   /*   @tab Body   @section Body Container Style   */   #templateBody{    /*@editable*/background-color:#FFFFFF;    /*@editable*/background-image:none;    /*@editable*/background-repeat:no-repeat;    /*@editable*/background-position:center;    /*@editable*/background-size:cover;    /*@editable*/border-top:0;    /*@editable*/border-bottom:0;    /*@editable*/padding-top:30px;    /*@editable*/padding-bottom:40px;    /*border: 1px solid #1e40ac;*/   }   /*   @tab Body   @section Body Interior Style   */   .bodyContainer{    /*@editable*/background-color:transparent;    /*@editable*/background-image:none;    /*@editable*/background-repeat:no-repeat;    /*@editable*/background-position:center;    /*@editable*/background-size:cover;    /*@editable*/border-top:0;    /*@editable*/border-bottom:0;    /*@editable*/padding-top:0;    /*@editable*/padding-bottom:0;   }   /*   @tab Body   @section Body Text   */   .bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{    /*@editable*/color:#808080;    /*@editable*/font-family:Helvetica;    /*@editable*/font-size:16px;    /*@editable*/line-height:150%;    /*@editable*/text-align:left;   }   /*   @tab Body   @section Body Link   */   .bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{    /*@editable*/color:#00ADD8;    /*@editable*/font-weight:normal;    /*@editable*/text-decoration:underline;   }   /*   @tab Footer   @section Footer Style   */   #templateFooter{    /*@editable*/background-color:#292a2e;    /*@editable*/background-image:none;    /*@editable*/background-repeat:no-repeat;    /*@editable*/background-position:center;    /*@editable*/background-size:cover;    /*@editable*/border-top:0;    /*@editable*/border-bottom:0;    /*@editable*/padding-top:10px;    /*@editable*/padding-bottom:0px;   }   /*   @tab Footer   @section Footer Interior Style   */   .footerContainer{    /*@editable*/background-color:transparent;    /*@editable*/background-image:none;    /*@editable*/background-repeat:no-repeat;    /*@editable*/background-position:center;    /*@editable*/background-size:cover;    /*@editable*/border-top:0;    /*@editable*/border-bottom:0;    /*@editable*/padding-top:0;    /*@editable*/padding-bottom:0;   }   /*   @tab Footer   @section Footer Text   */   .footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{    /*@editable*/color:#FFFFFF;    /*@editable*/font-family:Helvetica;    /*@editable*/font-size:12px;    /*@editable*/line-height:150%;    /*@editable*/text-align:center;   }   /*   @tab Footer   @section Footer Link   */   .footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{    /*@editable*/color:#FFFFFF;    /*@editable*/font-weight:normal;    /*@editable*/text-decoration:underline;   }   @media only screen and (min-width:768px){    .templateContainer{     width:600px !important;    }      } @media only screen and (max-width: 480px){     body,table,td,p,a,li,blockquote{      -webkit-text-size-adjust:none !important;     }       } @media only screen and (max-width: 480px){      body{       width:100% !important;       min-width:100% !important;      }        } @media only screen and (max-width: 480px){       .mcnRetinaImage{        max-width:100% !important;       }         } @media only screen and (max-width: 480px){        .mcnImage{         width:100% !important;        }          } @media only screen and (max-width: 480px){         .mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{          max-width:100% !important;          width:100% !important;         }           } @media only screen and (max-width: 480px){          .mcnBoxedTextContentContainer{           min-width:100% !important;          }            } @media only screen and (max-width: 480px){           .mcnImageGroupContent{            padding:9px !important;           }             } @media only screen and (max-width: 480px){            .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{             padding-top:9px !important;            }              } @media only screen and (max-width: 480px){             .mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{              padding-top:18px !important;             }               } @media only screen and (max-width: 480px){              .mcnImageCardBottomImageContent{               padding-bottom:9px !important;              }                } @media only screen and (max-width: 480px){               .mcnImageGroupBlockInner{                padding-top:0 !important;                padding-bottom:0 !important;               }                 } @media only screen and (max-width: 480px){                .mcnImageGroupBlockOuter{                 padding-top:9px !important;                 padding-bottom:9px !important;                }                  } @media only screen and (max-width: 480px){                 .mcnTextContent,.mcnBoxedTextContentColumn{                  padding-right:18px !important;                  padding-left:18px !important;                 }                   } @media only screen and (max-width: 480px){                  .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{                   padding-right:18px !important;                   padding-bottom:0 !important;                   padding-left:18px !important;                  }                    } @media only screen and (max-width: 480px){                   .mcpreview-image-uploader{                    display:none !important;                    width:100% !important;                   }                     } @media only screen and (max-width: 480px){   /*   @tab Mobile Styles   @section Heading 1   @tip Make the first-level headings larger in size for better readability on small screens.   */   h1{    /*@editable*/font-size:30px !important;    /*@editable*/line-height:125% !important;   }     } @media only screen and (max-width: 480px){   /*   @tab Mobile Styles   @section Heading 2   @tip Make the second-level headings larger in size for better readability on small screens.   */   h2{    /*@editable*/font-size:26px !important;    /*@editable*/line-height:125% !important;   }     } @media only screen and (max-width: 480px){   /*   @tab Mobile Styles   @section Heading 3   @tip Make the third-level headings larger in size for better readability on small screens.   */   h3{    /*@editable*/font-size:20px !important;    /*@editable*/line-height:150% !important;   }     } @media only screen and (max-width: 480px){   /*   @tab Mobile Styles   @section Heading 4   @tip Make the fourth-level headings larger in size for better readability on small screens.   */   h4{    /*@editable*/font-size:18px !important;    /*@editable*/line-height:150% !important;   }     } @media only screen and (max-width: 480px){   /*   @tab Mobile Styles   @section Boxed Text   @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.   */   .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{    /*@editable*/font-size:14px !important;    /*@editable*/line-height:150% !important;   }     } @media only screen and (max-width: 480px){   /*   @tab Mobile Styles   @section Header Text   @tip Make the header text larger in size for better readability on small screens.   */   .headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{    /*@editable*/font-size:16px !important;    /*@editable*/line-height:150% !important;   }     } @media only screen and (max-width: 480px){   /*   @tab Mobile Styles   @section Body Text   @tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.   */   .bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{    /*@editable*/font-size:16px !important;    /*@editable*/line-height:150% !important;   }     } @media only screen and (max-width: 480px){   /*   @tab Mobile Styles   @section Footer Text   @tip Make the footer content text larger in size for better readability on small screens.   */   .footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{    /*@editable*/font-size:14px !important;    /*@editable*/line-height:150% !important;   }    }</style>     <!--*|IF:MC_PREVIEW_TEXT|*-->   <!--[if !gte mso 9]><!----><!-- <span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">*|MC_PREVIEW_TEXT|*</span> --><!--<![endif]-->   <!--*|END:IF|*-->   <center>    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" height="100%" id="bodyTable">     <tbody><tr>      <td align="center" valign="top" id="bodyCell">       <!-- BEGIN TEMPLATE // -->       <table border="0" cellpadding="0" cellspacing="0" width="100%">        <tbody><tr>         <td align="center" valign="top" id="templateHeader" data-template-container="" style="background-color:#011a27; padding: 30px 0px;">           <!--[if (gte mso 9)|(IE)]>           <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">           <tr>           <td align="center" valign="top" width="600" style="width:600px;">           <![endif]-->           <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">            <tbody><tr>             <td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">              <tbody class="mcnImageBlockOuter">               <tr>                <td style="padding:9px" class="mcnImageBlockInner" valign="top">                 <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">                  <tbody><tr>                   <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">                      <img src="[#LogoUrl#]" alt="[#LogoUrl#]" style="padding-bottom: 0;display: inline !important;vertical-align: bottom;color: white !important;" class="mcnImage" width="100" height="5" align="middle">                         </td>                  </tr>                 </tbody></table>                </td>               </tr>              </tbody>             </table></td>            </tr>           </tbody></table>           <!--[if (gte mso 9)|(IE)]>           </td>           </tr>           </table>          <![endif]-->         </td>        </tr>        <tr>         <td align="center" valign="top" id="templateBody" data-template-container="">           <!--[if (gte mso 9)|(IE)]>           <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">           <tr>           <td align="center" valign="top" width="600" style="width:600px;">           <![endif]-->           <table align="center" border="0" cellpadding="0" cellspacing="0" max-width="100%" class="templateContainer" style="padding-bottom: 30px;">            <tbody><tr>             <td valign="top" class="bodyContainer"><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">              <tbody class="mcnTextBlockOuter">               <tr>                <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">                 <!--[if mso]>      <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">      <tr>      <![endif]-->        <!--[if mso]>      <td valign="top" width="600" style="width:600px;">      <![endif]-->      <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">       <tbody><tr>          <td class="mcnTextContent" style="padding-top:0; padding-right:30px; padding-bottom:9px; padding-left:30px;" valign="top">                  <p><span style="text-align: left;font-size:14px;font-weight:400;color:#585858;">Hi </span><span style="text-align: left;font-size:14px;font-weight:400;color:#585858;"><i>[#name#],</i></span></p>                                              <p><span style="color:#585858; font-family:arial,sans-serif; font-size:14px;">Your KOS account has been created.  Your login information is: </span></p>         <p style="margin-bottom:0px;"><span style="color:#585858; font-family:arial,sans-serif; font-size:14px;">User Name: </span><span style="color:#585858; font-family:arial,sans-serif; font-size:14px;"><i>[#email#]</i></span></p>         <p style="margin-top:0px;"><span style="color:#585858; font-family:arial,sans-serif; font-size:14px;">Company Code: </span><span style="color:#585858; font-family:arial,sans-serif; font-size:14px;"><i>[#compnayCode#]</i></span></p>         <p><span style="color:#585858; font-family:arial,sans-serif; font-size:14px;">Please set your password and login for the first time by clicking this link: <br> <a href="[#url#]">Set your Password.</a></span></p>        </td>               </tr>  <tr><td style="padding-top:0; padding-right:30px; padding-bottom:9px; padding-left:30px;" valign="top">    <p style="margin-bottom:0px;"><span style="color:#585858; font-family:arial,sans-serif; font-size:14px">Regards,</span></p>  <p style="margin-bottom:0px;"><span style="color:#585858; font-family:arial,sans-serif; font-size:14px;text-transform:uppercase;"><i>[#signature#]</i></span></p>  </td></tr>        </tbody></table>      <!--[if mso]>      </td>     <![endif]-->        <!--[if mso]>      </tr>      </table>     <![endif]-->    </td>   </tr>  </tbody>      </table>  </td>  </tr>  </tbody></table>           <!--[if (gte mso 9)|(IE)]>           </td>           </tr>           </table>          <![endif]-->         </td>        </tr>        <tr>         <td align="center" valign="top" id="templateFooter" data-template-container="" style="background-color: #011a27;">           <!--[if (gte mso 9)|(IE)]>           <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">           <tr>           <td align="center" valign="top" width="600" style="width:600px;">           <![endif]-->           <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">            <tbody><tr>             <td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">              <tbody class="mcnFollowBlockOuter">               <tr>                <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">                 <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">                  <tbody><tr>                   <td style="padding-left:9px; padding-right:9px;" align="center">                    <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">                     <tbody><tr>                      <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">                       <table cellspacing="0" cellpadding="0" border="0" align="center">                        <tbody><tr>                         <td valign="top" align="center">                           </td>                        </tr>                       </tbody></table>                      </td>                     </tr>                    </tbody></table>                   </td>                  </tr>                 </tbody></table>                  </td>               </tr>              </tbody>             </table>             <table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">              <tbody class="mcnTextBlockOuter">               <tr>                <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">                 <!--[if mso]>      <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">      <tr>      <![endif]-->        <!--[if mso]>      <td valign="top" width="600" style="width:600px;">      <![endif]-->      <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">       <tbody><tr>          <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">           <div style="text-align: center;">          <div class="contact_detail"><a href="https://www.kuboshlaw.com/" style="color: #fff; text-decoration:none;">www.kuboshlaw.com</a></div>          <br>          <div class="contact_detail"><span style="color: #fff;">1619 Lubbock Street Houston, TX 77007</span></div>            <div class="contact_detail" style="color: #fff;"><span><a href="tel:7133227410" style="color: #FFFFFF;text-decoration: none;">(713) 322-7410</a>    |   <a href="mailto:info@kuboshlaw.com" style="color: #FFFFFF;text-decoration: none;"> info@kuboshlaw.com</a></span><br><br>          © Copyright 2018 KuboshLaw. All rights reserved.</div>            <div class="contact_detail">           </div>         </div>          </td>       </tr>      </tbody></table>      <!--[if mso]>      </td>     <![endif]-->        <!--[if mso]>      </tr>      </table>     <![endif]-->    </td>   </tr>  </tbody>  </table></td>  </tr>  </tbody></table>           <!--[if (gte mso 9)|(IE)]>           </td>           </tr>           </table>          <![endif]-->         </td>        </tr>       </tbody></table>       <!-- // END TEMPLATE -->      </td>     </tr>    </tbody></table>   </center>    ', N'Mail', NULL)
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (9, N'SmtpPort', N'847', N'smtp', NULL)
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (10, N'WelcomeMail', N'<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    Hello <b>Mr/Mrs. [#Name#], </b>
    <br>
    <h3>
        Congratulations,
        you have registered successfully in BookingSite(dynamic).
        <br /> Please Click <a href="http://192.168.0.16:7071/#/"> here  </a>
        Thanks,<br />
        BookingSite.
    </h3>
</body>
</html>', N'template', N'WELCOMEMAIL')
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (11, N'forgotpassword', N'



    <meta charset="utf-8">
    <title></title>


    
    
    <h3>

        <br>  Hi. please click <a href="[#tokenLink#]"> here  </a>to create your password.
        Thanks,<br>
        <span style="background-color: rgb(255, 255, 0);"><strong>BookingSite.</strong></span>
    </h3>

', N'template', N'forgotpassword')
GO
INSERT [dbo].[setting] ([SettingId], [parameter], [parameterValue], [type], [templatesubject]) VALUES (12, N'order', N'

<html>
<head>
    <title></title>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

            table th  {
                background-color: #F7F7F7;
                color: #333;
                font-weight: bold;
            }

            table th, table td {
                padding: 5px;
                border-color: #ccc;
            }
    </style>
</head>


<body>

    <table border="1">

        <tr><th colspan="2">Appointment Receipt</th></tr>



        <tr>
            <th>Service</th>
            <td>[#category#]</td>
        </tr>
        <tr>
            <th>Sub Service  </th>
            <td>[#subCategory#]</td>
        </tr>
        <tr>

            <th>Date</th>
            <td> [#date#]</td>
        </tr>
        <tr>
            <th> Time  </th>
            <td>[#time#]</td>
        </tr>
        <!--<tr>

            <th>Apartment</th>
            <td> [#Apartment#]</td>
        </tr>-->
        <tr>
            <th> Address  </th>
            <td>[#Address#]</td>
        </tr>

        <tr>
            <th>OrderId</th>
            <td>[#orderId#]</td>
        </tr>
        <tr>
            <th>TransactionId  </th>
            <td>[#TransactionId#]</td>
        </tr>

        <tr>

            <th>Subtotal</th>
            <td>$ [#price#]</td>
        </tr>
        <tr>
            <th> Tax  </th>
            <td>18%</td>
        </tr>
        <tr>
            <th> Total  </th>
            <td>$ [#GrandTotal#]</td>
        </tr>
        <tr><th colspan="2">Payment Status : COMPLETED</th></tr>
    </table>
</body>
</html>


', N'template', N'Order')
GO
SET IDENTITY_INSERT [dbo].[setting] OFF
GO
SET IDENTITY_INSERT [dbo].[SubCategories] ON 
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (1, 2, N'Service 1', 1, N'assets/img/medium-wall-poster-butterfly-nature-hd-quality-wall-poster-original-imaf9rg5ggvnqsj3.jpeg', 1200, 1, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (3, 9, N'Service 45', 1, N'assets/img/medium-wall-poster-butterfly-nature-hd-quality-wall-poster-original-imaf9rg5ggvnqsj3.jpeg', 1200, 3, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (4, 3, N'Service 1', 1, N'assets/img/1500_Sqft.jpg', 1201, 4, 2)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (5, 3, N'Service 2', 1, N'assets/img/medium-wall-poster-butterfly-nature-hd-quality-wall-poster-original-imaf9rg5ggvnqsj3.jpeg', 1202, 5, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (6, 5, N'Service 1', 1, N'assets/img/medium-wall-poster-butterfly-nature-hd-quality-wall-poster-original-imaf9rg5ggvnqsj3.jpeg', 1203, 6, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (7, 5, N'Service 2', 0, N'assets/img/medium-wall-poster-butterfly-nature-hd-quality-wall-poster-original-imaf9rg5ggvnqsj3.jpeg', 1204, 7, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (9, 7, N'1 BHK', 1, N'assets/img/1_BHK.jpg', 1320, 1, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (10, 7, N'2 BHK', 1, N'assets/img/3_BHK.jpg', 1910, 2, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (11, 7, N'3 BHK', 1, N'assets/img/3_BHK.jpg', 2390, 4, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (12, 7, N'4 BHK', 1, N'assets/img/4_BHK.jpg', 2720, 5, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (13, 8, N'Duplex', 1, N'assets/img/PentHouse.jpg', 1320, 3, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (14, 8, N'Villa', 1, N'assets/img/PentHouse.jpg', 1250, 4, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (15, 8, N'Penthouse', 1, N'assets/img/PentHouse.jpg', 1120, 5, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (16, 8, N'Row Houses', 1, N'assets/img/PentHouse.jpg', 1075, 2, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (17, 9, N'500 Sqft', 1, N'assets/img/1500_Sqft.jpg', 1225, 6, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (18, 9, N'1000 Sqft', 1, N'assets/img/1500_Sqft.jpg', 1430, 2, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (19, 9, N'1500 Sqft', 1, N'assets/img/1500_Sqft.jpg', 1580, 5, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (20, 9, N'2000 Sqft', 1, N'assets/img/1500_Sqft.jpg', 1790, 6, 1)
GO
SET IDENTITY_INSERT [dbo].[SubCategories] OFF
GO
SET IDENTITY_INSERT [dbo].[user_login] ON 
GO
INSERT [dbo].[user_login] ([loginId], [userId], [userName], [email], [password], [roleId]) VALUES (7, 693, N'TEST', N'test@gmail.com', N'test@123', 1)
GO
SET IDENTITY_INSERT [dbo].[user_login] OFF
GO
SET IDENTITY_INSERT [dbo].[userdetails] ON 
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (693, N'hardik', N'hardik', N'Today@123', N'9999999999', N'hardik@gmail.com', N'tset', N'test', N'tset', 1, N'Ol0pzg1eG8NK4oWOSN13WfbFTVQ+69GieVD7QnvG5RH8DvLoZIr2d5FkyqNR90Zd', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (704, N'Kathan', N'ka', N'Kathan@12345', N'1324567890', N'kathan1@gmail.com', N'Nadaid', N'test ', N'', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (707, N'hardik', N' Hardik_Panchal', N'SGFyZGlrQDEyMw==', N'9998205687', N'hardikpanchalit@gmail.com', N'Cerrito 1010, C1010AAV CABA, Argentina', N'test ', N'', 2, N'fqYAvToDW+5eoX0D1WH7X88nldEPN3YlB6h7bdWWKMz4zottjWN9dYgkTvw+LFbXOCbnPpYSk38mZNhpdglJCQ==', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (728, N'kathan', N'kathan', N'S2F0aGFuQDEyMw==', N'1321321321', N'kathan8794@gmail.com', N'Nadiad', N'test ', N'', 2, N'pdBLM1pi20b156Frs8r32ORZQHQlXia7nUBHFOW8GP2FAvJeqeHc6edskNr+wSyk', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (730, N'Vikas', N'vikas16', N'Vikas@123', N'7041456907', N'vikas.vyas@manektech.com', N'acasca', N'test ', N'', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (742, N'Milan', N'milan.shah@manektech', N'Milan123#', N'2332323232', N'milan.shah@manektech.com', N'aditya icon', N'test ', N'', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (871, N'Binal', N'binaldave09', N'Binal@123', N'8485856666', N'binaldave09@gmail.com', N'Alien2=4', N'test ', N'', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (895, N'kathan', N'k', N'S2F0aGFuQDEyMzQ=', N'1234567980', N'kathan87941@gmail.com', N'Ahmedabad', N'test ', N'C:\fakepath\Flag.jpeg', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (897, N'User ', N'Manek Tech', N'TWFuZWtAMTIz', N'8888888888', N'lsd.manektech@gmail.com', N'Mario Bravo 1050, C1175ABT CABA, Argentina', N'test ', N'', 1, N'2Q1aI7G2BWwTpV1wWds0PODCwiWqt1xW0sy/wmYFTmXU+w1jaApXyUS3xdEosYlE', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (906, N'V P', N'k', N'S2F0aGFuQDEyMw==', N'1111111111', N'kathan879412@gmail.com', N'Ahmedabad', N'test ', N'', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (908, N'kathan', N'ka', N'S2F0aGFuQDEyMw==', N'8797452011', N'kathan87946666666666@gmail.com', N'Ahmedabad', N'test ', N'', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (916, N'naitik', N'Naitik', N'TmFpdGlrQDEyMw==', N'9574893198', N'naitikbhatt25@gmail.com', N'ahmedabad', N'test ', N'', 1, N'EMOrbFWHajZhk7azxHB0x9dvrTywvff4WSOdIM6k63LsfYcuxJ7ch2kuHmQzqrkT', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (917, N'string', N'string', N'c3RyaW5n', N'string', N'string', N'string', N'string', N'string', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (918, N'Naitik', N'naitikbhatt', N'bmFpdGlrQDEyMw==', N'9574893198', N'bhattnaitik9555@gmail.com', N'Ahmedabad', N'business', N'string', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (919, N'fdfgf', N'dfgdfgdfg', N'Z2RmZ2RmZw==', N'dfgdfgf', N'gdfgdf', N'dfgdfg', N'test', N'', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (920, N'naitik', N'naitik', N'bmFpdGlrYmhhdHQ=', N'95748923198', N'17mcen01@nirmauni.ac.in', N'ahmedabad', N'test', N'', 1, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (921, N'Admin', N'Admin', N'QWRtaW5AMTIz', N'9999999997', N'admin@gmail.com', N'112 Ocean Ave, Amityville, NY 11701, USA', N'test', N'', 2, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (926, N'naitik', N'jhjhb', N'jmbhjhb', N'jhbjhb', N'bjh', N'bjbjb', N'hbjb', N'jhb', 2, NULL, 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId], [token], [IsActive]) VALUES (927, N'jaymin', N'Dean@123', N'MURpZ2l0QA==', N'9574893198', N'dean@manektech.com', N'ahmedabad', N'test', N'', 1, N'Tj0cKbI+BBLcfJPFHxKpMuCxAApty9VL+O5UEg4StqytolK7+ojTTgDu+SP88YIk', 1)
GO
SET IDENTITY_INSERT [dbo].[userdetails] OFF
GO
ALTER TABLE [dbo].[SubCategories] ADD  CONSTRAINT [DF_SubCategories_approxTime]  DEFAULT ((0)) FOR [approxTime]
GO
ALTER TABLE [dbo].[SubCategories] ADD  CONSTRAINT [DF_SubCategories_policyid]  DEFAULT ((0)) FOR [policyid]
GO
ALTER TABLE [dbo].[AddUser]  WITH CHECK ADD  CONSTRAINT [FK_AddUser_userdetails] FOREIGN KEY([userId])
REFERENCES [dbo].[userdetails] ([userId])
GO
ALTER TABLE [dbo].[AddUser] CHECK CONSTRAINT [FK_AddUser_userdetails]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD FOREIGN KEY([masterCategoryId])
REFERENCES [dbo].[CategoryMaster] ([masterCategoryId])
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD FOREIGN KEY([masterCategoryId])
REFERENCES [dbo].[CategoryMaster] ([masterCategoryId])
GO
ALTER TABLE [dbo].[CategoryMaster]  WITH CHECK ADD FOREIGN KEY([masterCategoryId])
REFERENCES [dbo].[CategoryMaster] ([masterCategoryId])
GO
ALTER TABLE [dbo].[PaymentDetails]  WITH CHECK ADD  CONSTRAINT [FK_PaymentDetails_userdetails] FOREIGN KEY([userId])
REFERENCES [dbo].[userdetails] ([userId])
GO
ALTER TABLE [dbo].[PaymentDetails] CHECK CONSTRAINT [FK_PaymentDetails_userdetails]
GO
ALTER TABLE [dbo].[SubCategories]  WITH CHECK ADD FOREIGN KEY([categoryId])
REFERENCES [dbo].[Categories] ([categoryId])
GO
ALTER TABLE [dbo].[user_login]  WITH CHECK ADD  CONSTRAINT [FK_user_login_user_login] FOREIGN KEY([userId])
REFERENCES [dbo].[userdetails] ([userId])
GO
ALTER TABLE [dbo].[user_login] CHECK CONSTRAINT [FK_user_login_user_login]
GO
ALTER TABLE [dbo].[userdetails]  WITH CHECK ADD  CONSTRAINT [FK__userdetai__roleI__74AE54BC] FOREIGN KEY([roleId])
REFERENCES [dbo].[Role] ([roleId])
GO
ALTER TABLE [dbo].[userdetails] CHECK CONSTRAINT [FK__userdetai__roleI__74AE54BC]
GO
/****** Object:  StoredProcedure [dbo].[getAllImages]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getAllImages]
@id INT
AS
BEGIN
		SET NOCOUNT ON;
		Select SubCategoriesId,subcategoryName,status,image,price,approxTime,policyId,categoryId from SubCategories where categoryId = @id
END
--exe [dbo].[getAllImages] @id='7'
GO
/****** Object:  StoredProcedure [dbo].[getMasterData]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		vishal Guptalala	
-- Create date: 6/3/2019
-- Description:	get data from CategoryMaster data
-- =============================================
CREATE PROCEDURE [dbo].[getMasterData]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select masterCategoryId,categoryName,status from CategoryMaster where status = 1

END
GO
/****** Object:  StoredProcedure [dbo].[getMasterDataa]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<vishal Guptalala>
-- Create date: <06-03-2019>
-- Description:	<join of category data>
-- =============================================
CREATE PROCEDURE [dbo].[getMasterDataa] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select CategoryMaster.categoryName,CategoryMaster.status,

	Categories.masterCategoryId,Categories.subcategoryName

	From CategoryMaster Inner Join 

	Categories On 

	CategoryMaster.masterCategoryId = Categories.masterCategoryId;


END
--exec [getMasterDataa]
GO
/****** Object:  StoredProcedure [dbo].[getSubCategory]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		vishal Guptalala
-- Create date: 6/3/2019
-- Description:	get data from Categories table.
-- =============================================
CREATE PROCEDURE [dbo].[getSubCategory]
@id INT

AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select categoryId,subcategoryName,status from Categories where masterCategoryId = @id  
	--select SubCategories.categoryId,SubCategories.SubCategoriesId,SubCategories.price,SubCategories.subCategoryName,Categories.categoryId,Categories.masterCategoryId,Categories.subcategoryName
	--from SubCategories inner join Categories on SubCategories.categoryId = Categories.categoryId where masterCategoryId = @id
END
--exec [getSubCategory] @id='2'
GO
/****** Object:  StoredProcedure [dbo].[newRegister]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[newRegister]
			(
			
			@userName nvarchar(50),
			@password nvarchar(50),
			@email nvarchar(50),
			@isActive bit
			)

			AS
			BEGIN
			INSERT INTO registration 
					(
					[userName],
					[password],
					[email],					
					[isActive]
					)
			VALUES 
					(
					@userName,
					@password,
					@email,
					1
					)
			END
GO
/****** Object:  StoredProcedure [dbo].[Registration]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:     <Vishal Gupta>
-- Create date: <15 feb 2019> 
-- Description:   <For add Data from Registration>
-- ============================================= 
--exec [dbo].[Registration] 'kasdhdsvghhish', '66658sdv8855', 'keshsdvbghdxish@gmail1111.com31','london',Null,Null, 1,'rangddsvxbghila','12345', 2;
CREATE PROCEDURE [dbo].[Registration] @name          VARCHAR(50), 
                                     @contactNumber VARCHAR(18), 
                                     @email  NVARCHAR(50), 
                                     @address       VARCHAR(50), 
                                     @image         VARCHAR(max) = NULL, 
                                     @occupation    VARCHAR(50) = NULL, 
                                     @roleId        INT, 
                                     @userName      VARCHAR(50), 
                                     @password      VARCHAR(50),
									 @outputParam	INT = 0 OUTPUT
AS 

  BEGIN 
      --DECLARE @returnValue INT; 

      SET nocount ON; 

      BEGIN 
          BEGIN TRANSACTION [addUserData] 

          BEGIN try 
              IF NOT EXISTS(SELECT * 
                            FROM   userdetails 
                            WHERE  email = @email) 
                BEGIN 
                    INSERT INTO userdetails 
                                (NAME, 
                                 contactnumber, 
                                 email, 
                                 [address], 
                                 roleid) 
                    VALUES     (@name, 
                                @contactNumber, 
                                @email, 
                                @address, 
                                @roleId); 

                    INSERT INTO [user_login] 
                                (username, 
                                 email, 
                                 [password], 
                                 roleid) 
                    VALUES      (@userName, 
                                 @email, 
                                 @password, 
                                 @roleId); 

                    SET @outputParam = Scope_identity(); 
                END 
              ELSE 
                BEGIN 
                    SET @outputParam = 0; 
                END 

			 COMMIT TRANSACTION [addUserData]
          END try 

          BEGIN catch 
              ROLLBACK TRANSACTION [addUserData] 
          END catch 
      END 
      SELECT @outputParam AS outputParam; 
  END 
GO
/****** Object:  StoredProcedure [dbo].[spTimeSchedule]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spTimeSchedule] 
		@dayNumber int
As
BEGIN
	select * from ScheduleTime where dayNumber = @dayNumber
END
GO
/****** Object:  StoredProcedure [dbo].[userLogin]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vishal Guptalala>
-- Create date: <27-2-2019>
-- Description:	<For login check email>
-- =============================================

--exec [dbo].[userLogin] 'vikas@gmail.com';
CREATE PROCEDURE [dbo].[userLogin]
 
  @email nvarchar(50),
  @password varchar(50)
 
  

 
AS
BEGIN	
    SELECT userName,userId, email ,roleId  FROM userdetails 
	WHERE email = @email AND [password] = @password AND IsActive = 1;
	 
	--SET NOCOUNT ON;

    -- Insert statements for procedure here

END

GO
/****** Object:  StoredProcedure [dbo].[UserRegister]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UserRegister]
			(
			@Name nvarchar(50),
			@userName nvarchar(50),
			@password nvarchar(50),
			@contactNumber nvarchar(50),
			@email nvarchar(50),
			@address varchar(50),
			@occupation varchar(50),
			@image varchar(MAX),
			@roleId int,
			@IsActive bit
			)

			AS
			BEGIN
			INSERT INTO userdetails 
					(
					[Name],
					[userName],
					[password],
					[contactNumber],
					[email],
					[address],
					[occupation],
					[image],
					[roleId],
					[IsActive]
					)
			VALUES 
					(
					@Name,
					@userName,
					@password,
					@contactNumber,
					@email,
					@address,
					@occupation,
					@image,
					@roleId,
					1
					)
			END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddUser]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AddUser] 
	(
	@userId int,
	@Address VARCHAR(50),
	@ZipCode INT,
	@Apartment VARCHAR(50),
	@serviceTime nvarchar(50),
	@keyInfo nvarchar(50),
	@about_Key nvarchar(50),
	@special_Notes nvarchar(200)
	
	
	)
	AS 
	BEGIN
	
	INSERT INTO  AddUser
		(
			[userId],
			[Address],
			[ZipCode],
			[Apartment],
			[serviceTime],
			[keyInfo],
			[about_Key],
			[special_Notes]
		) 
	Values
		(
			@userId ,
			@Address,
			@ZipCode,
			@Apartment,
			@serviceTime,
			@keyInfo,
			@about_Key,
			@special_Notes

	    )
		
	END
	--exec usp_AddUser @userid='704',@Address='Ahmedabad',@ZipCode='388004',@Apartment='iconic',@serviceTime ='onetime',@keyInfo='doorman',@about_Key ='hidden',@special_Notes ='deep clean' 
			
GO
/****** Object:  StoredProcedure [dbo].[usp_AdminControl]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AdminControl]
       @Action VARCHAR(100),
       @masterCategoryId INT = 0,
       @categoryName VARCHAR(100) = '',
       @status bit = 0
	   --@Search NVARCHAR(50) = NULL,
	  
AS
BEGIN
      --SELECT
      IF @Action = 'SELECT'
      BEGIN
            SELECT masterCategoryId, categoryName, status
            FROM CategoryMaster		    
	END
	  --GET
	  else IF @Action = 'GET'
      BEGIN
            SELECT masterCategoryId, categoryName, status FROM CategoryMaster where masterCategoryId = @masterCategoryId
      END
 
      --INSERT
      else IF @Action = 'INSERT'
      BEGIN
            INSERT INTO CategoryMaster(categoryName, status)
            VALUES (@categoryName, @status)
      END
 
      --UPDATE
     else IF @Action = 'UPDATE'
      BEGIN
            UPDATE CategoryMaster
            SET status = @status, categoryName = @categoryName
            WHERE masterCategoryId = @masterCategoryId
      END
 
      --DELETE
     else IF @Action = 'SOFT_DELETE'
      BEGIN
		    DELETE FROM CategoryMaster 
            WHERE masterCategoryId = @masterCategoryId
      END
	  ----SOFT_DELETE
	  --else IF @Action = 'SOFT_DELETE'
   --   BEGIN
	 

   --         UPDATE CategoryMaster
   --         SET status = 0
   --         WHERE masterCategoryId = @masterCategoryId
   --   END
END

--exec usp_AdminControl @Action='select'
--exec usp_AdminControl @Action='UPDATE',@masterCategoryId='180',@categoryName='Event Planer',@status='0'
--exec usp_AdminControl @Action='INSERT',@categoryName='Bunglows',@status='1'
--exec usp_AdminControl @Action='SOFT_DELETE',
--exec usp_AdminControl @Action='GET',@masterCategoryId='4'
GO
/****** Object:  StoredProcedure [dbo].[usp_Category_Admin]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Category_Admin]
       @Action VARCHAR(100),
       @categoryId INT = 0,
	   @masterCategoryId INT = 0,
       @subcategoryName VARCHAR(100) = '',
       @status bit = 0,
	   @SearchValue NVARCHAR(50) = NULL,
	   @PageNumber INT = 1,
       @PageSize   INT = 100
AS
BEGIN
      --SELECT
      IF @Action = 'SELECT'
      BEGIN
            SELECT Categories.categoryId, Categories.subcategoryName,CategoryMaster.categoryName, Categories.status,Categories.masterCategoryId
            FROM Categories INNER JOIN CategoryMaster ON  CategoryMaster.masterCategoryId = Categories.masterCategoryId

			 ORDER BY categoryId
				 OFFSET @PageSize * (@PageNumber - 1) ROWS
				 FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE);
      END
	  else IF @Action = 'GET'
      BEGIN
            SELECT categoryId, subcategoryName, status FROM Categories where categoryId = @categoryId
      END
 
      --INSERT
      else IF @Action = 'INSERT'
      BEGIN
            INSERT INTO Categories(subcategoryName, [status],masterCategoryId)
            VALUES (@subcategoryName, 1 ,@masterCategoryId)
      END
	   else IF @Action = 'UPDATE'
      BEGIN
            UPDATE Categories
            SET status = @status, subcategoryName = @subcategoryName,masterCategoryId=@masterCategoryId
            WHERE categoryId = @categoryId
      END
 
      --DELETE
     else IF @Action = 'DELETE'
      BEGIN
		    DELETE FROM Categories 
            WHERE categoryId = @categoryId
      END
END

--exec usp_Category_Admin @Action='select'
--exec usp_Category_Admin @Action='UPDATE',@masterCategoryId='7',@categoryName='string',@status='0'
--exec usp_Category_Admin @Action='INSERT',@categoryName='Bunglows',@status='1'
--exec usp_Category_Admin @Action='SOFT_DELETE',
--exec usp_Category_Admin @Action='GET',@categoryId='5'
GO
/****** Object:  StoredProcedure [dbo].[usp_CategoryExist]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_CategoryExist]

@categoryName VARCHAR(100),
@masterCategoryId int=0
as
begin
	IF(@masterCategoryId!=0) -- EDIT TIME
	BEGIN
		select * from CategoryMaster where  categoryName = @categoryName AND masterCategoryId != @masterCategoryId
	END
	ELSE -- ADD TIME
	BEGIN
		select * from CategoryMaster where  categoryName = @categoryName 	
	END
end
--exec usp_CategoryExist @categoryName = 'Mason'
GO
/****** Object:  StoredProcedure [dbo].[usp_CategoryExistEdit]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_CategoryExistEdit]
@subcategoryName VARCHAR(100),
@categoryId int=0
as
begin
	IF(@categoryId!=0) -- EDIT TIME
	BEGIN
		select * from Categories where  subcategoryName = @subcategoryName AND categoryId != @categoryId
	END
	ELSE -- ADD TIME
	BEGIN
		select * from Categories where  subcategoryName = @subcategoryName 	
	END
end
--exec usp_CategoryExist @categoryName = 'Mason'
GO
/****** Object:  StoredProcedure [dbo].[usp_changePassword]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_changePassword]
@userId int,
@password nvarchar(50)

as
begin
update userdetails set [password]=@password
where userId = @userId 
end
GO
/****** Object:  StoredProcedure [dbo].[usp_Checkbox]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Checkbox]
      @Action VARCHAR(100),
      @masterCategoryId INT = null,
	  @categoryId INT = null,
	  @SubCategoriesId INT = null,
            @status bit = null,
			 @Type nvarchar(50)='',
			 @IsActive bit=null,
			 @userId  INT = null
as
BEGIN
 --Master Category select
 IF @Action = 'CheckBox1'
BEGIN
 UPDATE CategoryMaster
            SET status = @status, Type = @Type
            WHERE masterCategoryId = @masterCategoryId
END
 -- Category select
	  else IF @Action = 'CheckBox2'
      BEGIN
           UPDATE Categories
            SET status = @status 
            WHERE categoryId = @categoryId
      END
	  --Sub Category  select
	  else IF @Action = 'CheckBox3'
      BEGIN
           UPDATE SubCategories
            SET status = @status 
            WHERE SubCategoriesId = @SubCategoriesId
      END
	  --User Active  select
	  else IF @Action = 'UserCheckBoxActive'
      BEGIN
           UPDATE userdetails
            SET IsActive = @IsActive 
            WHERE userId = @userId
      END
	  -- For MasterCategory Selected List 
	  else IF @Action = 'Select'
      BEGIN
			SELECT categoryName ,masterCategoryId  FROM CategoryMaster where status = 1
      END
	   -- For Category Selected List  
	  else IF @Action = 'SubCatSelect'
      BEGIN
			SELECT subcategoryName ,categoryId  FROM Categories where status = 1
      END

END
--exec [usp_Checkbox] @Action = 'CheckBox2'
GO
/****** Object:  StoredProcedure [dbo].[usp_CreatePassword]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[usp_CreatePassword]
@email nvarchar(50),
@password nvarchar(50)
as
begin
--select * from userdetails where email = @email
update userdetails set password = @password
where email=@email
end
--exec usp_getEmail  @email='hardikpanchalit@gmail.com'
GO
/****** Object:  StoredProcedure [dbo].[usp_Downloadpdf]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_Downloadpdf] 

	@orderID nvarchar(50)
as
begin 
select * from PaymentReceipt where orderID = @orderID 
end
--exec usp_Downloadpdf @orderID = '916'
GO
/****** Object:  StoredProcedure [dbo].[usp_EditProfile]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[usp_EditProfile]
@userId int,
@userName nvarchar(50),
@contactNumber nvarchar(50),
@email nvarchar(50),
@address nvarchar(50)

as
begin
update userdetails set [userName]=@userName,[contactNumber]=@contactNumber,[email]=@email,[address]=@address 
	
where userId = @userId 
end
GO
/****** Object:  StoredProcedure [dbo].[usp_email]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_email] 

	@userId int
as
begin 
select * from userdetails where userId = @userId 
end
--exec usp_email @userId = '704'
GO
/****** Object:  StoredProcedure [dbo].[usp_Employee]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_Employee]

@UserName nvarchar(50),
@Password nvarchar(50),
@Email nvarchar(50),
@ContactNo nvarchar(50)


as
begin
insert into Emloyee (UserName,Password,Email,ContactNo)
VALUES(@UserName,@Password,@Email,@ContactNo) 
end

---exec [usp_Employee] @Name='abc',@Email='abc@gmail.com',@Password='Abc@123'
GO
/****** Object:  StoredProcedure [dbo].[usp_forgotEmail]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_forgotEmail]
@email nvarchar(50)
as
begin
--select * from userdetails where email = @email
SELECT * FROM userdetails WHERE email = @email
end
--exec usp_getEmail  @email='hardikpanchalit@gmail.com'
GO
/****** Object:  StoredProcedure [dbo].[usp_getCardName]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_getCardName]
 @SubcategoriesId VARCHAR(MAX)
as
declare @sql_query varchar(max)
SET @sql_query=' select * from SubCategories where SubcategoriesId IN ('+ @SubcategoriesId+')'
exec (@sql_query)
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCategoryId]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_GetCategoryId]

@Action VARCHAR(100),
 @masterCategoryId INT = 0,
 @categoryId INT = 0
as


BEGIN
 
	IF @Action = 'SELECT_M_ID'
	BEGIN
			select * from Categories where masterCategoryId = @masterCategoryId
	END

	else IF @Action = 'SELECT_C_ID'
    BEGIN
           SELECT  * from SubCategories where categoryId = @categoryId
     END
END
-- exec [usp_GetCategoryId] @Action ='SELECT_M_ID' ,@masterCategoryId ='31'
-- exec [usp_GetCategoryId] @Action ='SELECT_C_ID' ,@categoryId ='19'
GO
/****** Object:  StoredProcedure [dbo].[usp_getEmail]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[usp_getEmail]
@email nvarchar(50)
as
begin
select * from userdetails where email = @email
end
GO
/****** Object:  StoredProcedure [dbo].[usp_order]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_order] 

	@userId int
as
begin 
select * from PaymentReceipt where userId = @userId 
end
--exec usp_email @userId = '704'
GO
/****** Object:  StoredProcedure [dbo].[usp_Pagination]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Pagination]
       
       @masterCategoryId INT = 0,
	    @categoryId INT = 0,
	    @Action VARCHAR(100),
       @categoryName VARCHAR(100) = '',
	    @subcategoryName VARCHAR(100) = '',
       @status bit = 0,
	   --@Search NVARCHAR(50) = NULL,
	   @pageIndex int = 1,
       @pageSize   INT =  0
AS
BEGIN
     
      IF @Action = 'Master'
      BEGIN
            SELECT masterCategoryId, categoryName, status
            FROM CategoryMaster
			--where  categoryName like '%'+@search+'%'

			 ORDER BY masterCategoryId
				 OFFSET @pageSize * (@pageIndex + 1) ROWS
				 FETCH NEXT @pageSize ROWS ONLY OPTION (RECOMPILE);
      END
	   else IF @Action = 'Category'
      BEGIN
            SELECT Categories.categoryId, Categories.subcategoryName,CategoryMaster.categoryName, Categories.status,Categories.masterCategoryId
            FROM Categories INNER JOIN CategoryMaster ON  CategoryMaster.masterCategoryId = Categories.masterCategoryId
			--where  categoryName like '%'+@search+'%'

			 ORDER BY categoryId
				 OFFSET @pageSize * (@pageIndex + 1) ROWS
				 FETCH NEXT @pageSize ROWS ONLY OPTION (RECOMPILE);
      END

	    else IF @Action = 'SubCategories'
      BEGIN
            SELECT SubCategories.SubCategoriesId,SubCategories.categoryId, SubCategories.subcategoryName,Categories.subcategoryName as Category, SubCategories.status,SubCategories.approxTime,SubCategories.image,SubCategories.price

            FROM SubCategories INNER JOIN Categories ON  SubCategories.categoryId = Categories.categoryId

--where  categoryName like '%'+@search+'%'

			 ORDER BY SubCategoriesId
				 OFFSET @pageSize * (@pageIndex + 1) ROWS
				 FETCH NEXT @pageSize ROWS ONLY OPTION (RECOMPILE);
      END
END
	  
 
    
 
    
   


--exec usp_AdminControl @Action='select'
--exec usp_AdminControl @Action='UPDATE',@masterCategoryId='180',@categoryName='Event Planer',@status='0'
--exec usp_AdminControl @Action='INSERT',@categoryName='Bunglows',@status='1'
--exec usp_AdminControl @Action='SOFT_DELETE',
--exec usp_AdminControl @Action='GET',@masterCategoryId='4'
GO
/****** Object:  StoredProcedure [dbo].[usp_Payment]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_Payment]
(
	@userId int,
	@Credit_Card_No nvarchar(50),
	@Expire_Date datetime,
	@CVV int,
	@CardHolder_Name nvarchar(50),
	@Email nvarchar(50),
	@ContactNo int

)
AS
BEGIN
INSERT INTO PaymentDetails
(
	
	[userId],
	[Credit_Card_No],
	[Expire_Date],
	[CVV],
	[CardHolder_Name],
	[Email],
	[ContactNo]
)

VALUES
(
	
	@userId,
	@Credit_Card_No,
	@Expire_Date,
	@CVV,
	@CardHolder_Name,
	@Email,
	@ContactNo
)

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Receipt]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_Receipt]
(

	@userId int,
	@category nvarchar(50),
	@subCategory	nvarchar(50),	
	@Typeserivce	nvarchar(50),	
	@date	nvarchar(50),	
	@keyInfo	nvarchar(50),	
	@Apartment	nvarchar(50),	
	@time	nvarchar(50),	
	@Address	nvarchar(50),	
	@price	nvarchar(50),	
	@GrandTotal	nvarchar(50),
	@orderID nvarchar(50),
	@TransactionID nvarchar(50)
	
)
AS
BEGIN
INSERT INTO PaymentReceipt
(

	[userId],
	[category],
	[subCategory],
	[Typeserivce],
	[date],
	[keyInfo],
	[Apartment],	
	[time],	
	[Address],	
	[price],		
	[GrandTotal],
	[orderID],
	[TransactionID]
		

)

VALUES
(
	
	
	@userId,
	@category,
	@subCategory,	
	@Typeserivce,	
	@date,	
	@keyInfo,	
	@Apartment,	
	@time,	
	@Address,	
	@price,	
	@GrandTotal,
	@orderID ,
	@TransactionID
		
)

END

--exec usp_Receipt @userid='704',@category='Ahmedabad',@subCategory='388004',@Typeserivce='iconic',@date ='onetime',@keyInfo='doorman',@Apartment ='hidden',@time ='deep clean',@Address='abc',@price ='12',@GrandTotal='500',@orderID='3ML347256C783732C' ,	@TransactionID='RE74FMTFHYPSS' ,@Status='Completd'	 
GO
/****** Object:  StoredProcedure [dbo].[usp_ResetPassword]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[usp_ResetPassword]
@email nvarchar(50),
@token nvarchar(500)
as
begin
update userdetails set token=@token
where email = @email 
end
GO
/****** Object:  StoredProcedure [dbo].[usp_setting]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_setting]
		@Action VARCHAR(100)=NULL,
		@SettingId int=null,
	   	@parameter nvarchar(100)=null,
		@parameterValue nvarchar(MAX)=null,
		@type nvarchar(20)=null,
		@templatesubject nvarchar(50)=null
AS
	   


BEGIN
      --SELECT
      IF @Action = 'SELECTSETTING'
      BEGIN

	  
	select  SettingId, parameter, parameterValue
	 from setting where type=@type

            	    
	END
	 --SELECT template5
      IF @Action = 'SELECTTEMPLATE'
      BEGIN

	  
	select  SettingId, parameter,type, parameterValue , templatesubject 
	 from setting where type=@type

            	    
	END
	 --SELECT template5
      IF @Action = 'SELECTSENDINGTEMPLATE'
      BEGIN

	  
	select  SettingId, parameter,type, parameterValue , templatesubject 
	 from setting where parameter=@parameter

            	    
	END
	
	 --SELECT template5
      IF @Action = 'SELECTRECEIPTTEMPLATE'
      BEGIN

	  
	select  SettingId, parameter,type, parameterValue , templatesubject 
	 from setting where parameter=@parameter

            	    
	END
	
	 --SELECT template5
      IF @Action = 'SELECTFORGOTPASSWORDTEMPLATE'
      BEGIN

	  
	select  SettingId, parameter,type, parameterValue , templatesubject 
	 from setting where parameter=@parameter

            	    
	END
	 --SELECT template5
      IF @Action = 'SELECTEDITTEMPLATE'
      BEGIN

	  
	select  SettingId, parameter,type, parameterValue , templatesubject 
	 from setting where type='template' AND SettingId=@SettingId

            	    
	END
	--update
      IF @Action = 'UPDATESETTING'
      BEGIN

	  print 'update'

	  UPDATE setting  SET  
		    parameterValue=@parameterValue  where parameter=@parameter 

            	    
	END
	--update
      IF @Action = 'UPDATETEMPLATE'
      BEGIN

	  print 'update'

	  UPDATE setting  SET  
		    parameterValue = @parameterValue, templatesubject=@templatesubject  where SettingId=@SettingId  

            	    
	END
END


--exec usp_setting @Action = 'SELECTFORGOTPASSWORDTEMPLATE', @parameter = 'forgotpassword' 
--exec usp_setting @Action = 'SELECTTEMPLATE' ,@type = 'template' 
--exec usp_setting @Action = 'SELECTTEMPLATE' ,@type = 'template' ,@SettingId='11'
--exec usp_setting @Action = 'UPDATESETTING',  @parameterValue='CHANGEd',@parameter='host'
--exec usp_setting @Action = 'UPDATETEMPLATE',  @parameterValue='HELLO brother',@parameter='WELCOMEMAil', @templatesubject='done' , @type='template'
--exec usp_Category_Admin @Action='UPDATE',@masterCategoryId='7',@categoryName='string',@status='0'













GO
/****** Object:  StoredProcedure [dbo].[usp_SubCategories]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SubCategories]
	   @SubCategoriesId int=0,
       @Action VARCHAR(100),
       @categoryId INT = 0,
       @subCategoryName VARCHAR(100) = '',
	   @price INT = 0,
	   @image varchar(MAX)=0,
       @status bit = 0
	   
AS
BEGIN
      --SELECT
      IF @Action = 'SELECT'
      BEGIN

	  SELECT SubCategories.SubCategoriesId,SubCategories.categoryId, SubCategories.subcategoryName,Categories.subcategoryName as Category, SubCategories.status,SubCategories.approxTime,SubCategories.image,SubCategories.price

            FROM SubCategories INNER JOIN Categories ON  SubCategories.categoryId = Categories.categoryId


            --SELECT * FROM SubCategories		    
	END


	--INSERT
      else IF @Action = 'INSERT'
      BEGIN
            INSERT INTO SubCategories(subcategoryName, [status],categoryId ,price,[image])
            VALUES (@subcategoryName, 1 ,@categoryId,@price,@image)

			
      END

	 
	   else IF @Action = 'UPDATE'
      BEGIN
            UPDATE SubCategories
            SET status = @status, price=@price, image = @image,  subCategoryName = @subCategoryName,categoryId=@categoryId
            WHERE SubCategoriesId = @SubCategoriesId
      END
        
		 --DELETE
     else IF @Action = 'DELETE'
      BEGIN
		    DELETE FROM SubCategories 
            WHERE SubCategoriesId = @SubCategoriesId
      END
	   else IF @Action = 'ImageUpload'
      BEGIN
		    update SubCategories set image = @image
            WHERE SubCategoriesId = @SubCategoriesId
      END
END

--exec usp_SubCategories @Action='SELECT'
--exec usp_SubCategories @Action='UPDATE',@SubCategoriesId='22',@subCategoryName='Event Planer',@status='1',@categoryId='13'
--exec usp_SubCategories @Action='INSERT',@subCategoryName='cooook'
--exec usp_SubCategories @Action='SOFT_DELETE',@SubCategoriesId='22'
--exec usp_SubCategories @Action='GET',@masterCategoryId='4'




			
GO
/****** Object:  StoredProcedure [dbo].[usp_SubCategoryExist]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[usp_SubCategoryExist]
@subcategoryName VARCHAR(100),
@SubCategoriesId int=0
as
begin
	IF(@SubCategoriesId!=0) -- EDIT TIME
	BEGIN
		select * from SubCategories where  subcategoryName = @subcategoryName AND SubCategoriesId != @SubCategoriesId
	END
	ELSE -- ADD TIME
	BEGIN
		select * from SubCategories where  subcategoryName = @subcategoryName 	
	END
end
--exec usp_CategoryExist @categoryName = 'Mason'
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateSetting]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================



CREATE PROCEDURE [dbo].[usp_UpdateSetting]
	@SettingId int,
	@parameter nvarchar(200),
	@parameterValue nvarchar(MAX)
	

AS
BEGIN

		update setting  
		set parameter=@parameter,parameterValue= @parameterValue where settingId=@settingId

END

GO
/****** Object:  StoredProcedure [dbo].[usp_userExist]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_userExist]

@email nvarchar(50)
as
begin
select * from userdetails where  email = @email 
end
--exec usp_userExist @email = 'kathan8794@gmail.com'
GO
/****** Object:  StoredProcedure [dbo].[usp_UserList]    Script Date: 03-01-2020 12:34:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UserList]
		@Action VARCHAR(100),
		@userId int=null,
	   	@Name nvarchar(50)=null,
		@userName nvarchar(50)=null,
		@password nvarchar(50)=null,
		@contactNumber nvarchar(50)=null,
		@email nvarchar(50)=null,
		@address varchar(50)=null,
		@occupation varchar(50)=null,
		@image varchar(MAX)=null,
		@roleId int=null,
		@IsActive bit=null
	   
AS
BEGIN
      --SELECT
      IF @Action = 'SELECT'
      BEGIN

	  SELECT   userdetails.userId ,userdetails.name ,userdetails.userName ,userdetails.password ,userdetails.contactNumber,userdetails.email, userdetails.address , userdetails.occupation, userdetails.image, userdetails.roleId ,  userdetails.token, userdetails.IsActive ,Role.name as RoleName    
	  
	  
	   FROM userdetails INNER JOIN Role  ON  userdetails.roleId = Role.roleId


            	    
	END
	--SELECT
      IF @Action = 'SELECTSINGLE'
      BEGIN

	  SELECT   userdetails.userId ,userdetails.name ,userdetails.userName ,userdetails.password ,userdetails.contactNumber,userdetails.email, userdetails.address , userdetails.occupation, userdetails.image, userdetails.roleId ,  userdetails.token, userdetails.IsActive ,Role.name as RoleName    
	  
	  
	   FROM userdetails INNER JOIN Role  ON  userdetails.roleId = Role.roleId WHERE	userdetails.userId=@userId


            	    
	END
END

--exec usp_UserList @Action='SELECTSINGLE' ,@userId='704'
GO
USE [master]
GO
ALTER DATABASE [myclean] SET  READ_WRITE 
GO
