USE [master]
GO
/****** Object:  Database [myclean]    Script Date: 8/2/2019 7:14:46 PM ******/
CREATE DATABASE [myclean]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'myclean', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\myclean.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'myclean_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\myclean_log.ldf' , SIZE = 1536KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [myclean] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [myclean].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [myclean] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [myclean] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [myclean] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [myclean] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [myclean] SET ARITHABORT OFF 
GO
ALTER DATABASE [myclean] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [myclean] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [myclean] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [myclean] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [myclean] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [myclean] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [myclean] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [myclean] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [myclean] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [myclean] SET  ENABLE_BROKER 
GO
ALTER DATABASE [myclean] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [myclean] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [myclean] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [myclean] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [myclean] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [myclean] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [myclean] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [myclean] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [myclean] SET  MULTI_USER 
GO
ALTER DATABASE [myclean] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [myclean] SET DB_CHAINING OFF 
GO
ALTER DATABASE [myclean] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [myclean] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [myclean] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [myclean] SET QUERY_STORE = OFF
GO
USE [myclean]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 8/2/2019 7:14:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[addressId] [int] IDENTITY(1,1) NOT NULL,
	[customerId] [int] NOT NULL,
	[address] [varchar](50) NULL,
	[zipCode] [numeric](18, 0) NULL,
	[Apartment] [nvarchar](100) NULL,
 CONSTRAINT [PK__Address__26A111AD14A4E4DD] PRIMARY KEY CLUSTERED 
(
	[addressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AddUser]    Script Date: 8/2/2019 7:14:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddUser](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[Address] [varchar](50) NULL,
	[ZipCode] [int] NULL,
	[Apartment] [varchar](50) NULL,
	[userId] [int] NULL,
	[serviceTime] [nvarchar](50) NULL,
	[keyInfo] [nvarchar](50) NULL,
	[about_Key] [nvarchar](50) NULL,
	[special_Notes] [nvarchar](200) NULL,
 CONSTRAINT [PK_AddUser] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BookingDetails]    Script Date: 8/2/2019 7:14:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookingDetails](
	[bookingId] [int] IDENTITY(1,1) NOT NULL,
	[totalAmount] [numeric](18, 0) NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[userId] [int] NULL,
	[addressId] [int] NULL,
	[paymentId] [int] NULL,
	[status] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[bookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 8/2/2019 7:14:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[categoryId] [int] IDENTITY(1,1) NOT NULL,
	[subcategoryName] [varchar](100) NULL,
	[status] [bit] NULL,
	[masterCategoryId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[categoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 8/2/2019 7:14:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[masterCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[categoryName] [varchar](50) NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[masterCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[login]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[login](
	[loginId] [int] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[password] [varchar](50) NULL,
	[roleId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[loginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[payment]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payment](
	[paymentId] [int] IDENTITY(1,1) NOT NULL,
	[paymentType] [varchar](50) NULL,
	[status] [varchar](50) NULL,
	[datetime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[paymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaymentDetails]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentDetails](
	[PaymentId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NULL,
	[Credit_Card_No] [nvarchar](50) NULL,
	[Expire_Date] [datetime] NULL,
	[CVV] [int] NULL,
	[CardHolder_Name] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[ContactNo] [int] NULL,
 CONSTRAINT [PK_PaymentDetails] PRIMARY KEY CLUSTERED 
(
	[PaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaymentReceipt]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentReceipt](
	[ReceiptId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[category] [nvarchar](50) NULL,
	[subCategory] [nvarchar](50) NULL,
	[Typeserivce] [nvarchar](50) NULL,
	[date] [nvarchar](50) NULL,
	[keyInfo] [nvarchar](50) NULL,
	[Apartment] [nvarchar](50) NULL,
	[time] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[price] [nvarchar](50) NULL,
	[GrandTotal] [nvarchar](50) NULL,
	[orderID] [nvarchar](50) NULL,
	[TransactionID] [nvarchar](50) NULL,
 CONSTRAINT [PK_PaymentReceipt] PRIMARY KEY CLUSTERED 
(
	[ReceiptId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[policy]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[policy](
	[policyId] [int] IDENTITY(1,1) NOT NULL,
	[policyName] [varchar](50) NULL,
	[vistingCharge] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[policyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[roleId] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ScheduleTime]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleTime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dayName] [nvarchar](50) NULL,
	[dayNumber] [int] NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[interval] [float] NULL,
 CONSTRAINT [PK_ScheduleTime] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategories]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategories](
	[SubCategoriesId] [int] IDENTITY(1,1) NOT NULL,
	[categoryId] [int] NULL,
	[subCategoryName] [varchar](100) NULL,
	[status] [bit] NULL,
	[image] [varchar](max) NULL,
	[price] [int] NULL,
	[approxTime] [int] NULL,
	[policyid] [int] NULL,
 CONSTRAINT [PK_SubCategories] PRIMARY KEY CLUSTERED 
(
	[SubCategoriesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_login]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_login](
	[loginId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[userName] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[password] [varchar](50) NULL,
	[roleId] [int] NULL,
 CONSTRAINT [PK_user_login] PRIMARY KEY CLUSTERED 
(
	[loginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[userdetails]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userdetails](
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[userName] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NULL,
	[contactNumber] [nvarchar](50) NULL,
	[email] [nvarchar](50) NOT NULL,
	[address] [varchar](50) NULL,
	[occupation] [varchar](50) NULL,
	[image] [varchar](max) NULL,
	[roleId] [int] NOT NULL,
 CONSTRAINT [PK__userdeta__CB9A1CFF3E2327BF] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AddUser] ON 
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (1, N'1216 5th Ave, New York, NY 10029, USA', 10029, N'abc', 707, N'One Time', N'Hidden Key', N'Hidden Key', N'clean')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (2, N'1234 Market St, Philadelphia, PA 19107, USA', 19107, N'ccx', 707, N'One Time', N'Hidden Key', N'Hidden Key', N'abc')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (3, N'1216 5th Ave, New York, NY 10029, USA', 10029, N'abc', 707, N'One Time', N'Hidden Key', N'Hidden Key', N'clean')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (4, N'string', 0, N'string', 707, N'string', N'string', N'string', N'string')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (5, N'1216 5th Ave, New York, NY 10029, USA', 10029, N'new York', 707, N'One Time', N'Hidden Key', N'garden', N'deep clean')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (6, N'1216 5th Ave, New York, NY 10029, USA', 10029, N'abcd', 707, N'One Time', N'Hidden Key', N'home', N'cny')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (7, N'1211 6th Ave, New York, NY 10036, USA', 10036, N'new york', 707, N'One Time', N'Hidden Key', N'in home', N'instruction')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (8, N'120 Broadway, New York, NY 10271, USA', 10271, N'new york', 707, N'One Time', N'Hidden Key', N'232132', N'23153')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (9, N'2130 Broadway, New York, NY 10023, USA', 10023, N'fdsfd', 707, N'One Time', N'Hidden Key', N'dfd', N'dfdf')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (10, N'1216 5th Ave, New York, NY 10029, USA', 10029, N'12', 707, N'One Time', N'Hidden Key', N'jhjhgj', N'hgjg')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (11, N'dfdsfdsf', 0, N'dfdfdf', 707, N'One Time', N'Hidden Key', N'dfdsfds', N'fdfsfdsfds')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (12, N'New Jersey', 0, N'fghdfgh', 728, N'One Time', N'Hidden Key', N'fgh', N'fghdfgh')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (13, N'mt', 382451, N'test', 728, N'One Time', N'Someone is Home', NULL, N'comp')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (14, N'mt', 382451, N'test', 728, N'One Time', N'Someone is Home', NULL, N'comp')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (15, N'ascasvc', 154514, N'ascsac', 728, N'One Time', N'Someone is Home', NULL, N'How Do we get in?')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (16, N'Ahmedabad', 123456, N'Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'ggg')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (17, N'timber point', 321321, N'mt', 728, N'Weekly', N'Doorman', NULL, N'at door')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (18, N'Ahmedabad', 1231, N'Ahmedabad', 728, N'One Time', N'Someone is Home', NULL, N'fdgfdg')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (19, N'adfdfd', 222323, N'101 test', 730, N'One Time', N'Someone is Home', NULL, N'undefined')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (20, N'2222222222', 222222, N'222222222', 728, N'One Time', N'Someone is Home', NULL, N'22222222')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (21, N'4th Floor, Timber Point, Beside Kotak Mahindra Ban', 380051, N'Prahalad Nagar Road', 730, N'One Time', N'Someone is Home', NULL, N'undefined')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (22, N'Ahmedabad', 123456, N'Timber Point', 728, N'One Time', N'Someone is Home', NULL, N'123')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (23, N'ggdf', 123145, N'fgfg', 728, N'One Time', N'Someone is Home', NULL, N'fgdfg')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (24, N'sdsd', 123132, N'dsasdsa', 707, N'One Time', N'Someone is Home', NULL, N'sadsads')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (25, N'4th Floor,Above Timber Point,100 Feet Ring Road,Op', 380015, N'Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'nothing')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (26, N'4th Floor,Above Timber Point,100 Feet Ring Road,Op', 380015, N'Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'nothing')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (27, N'4th Floor,Above Timber Point,100 Feet Ring Road,Op', 380015, N'Timber Point', 707, N'Weekly', N'Doorman', NULL, N'nothing')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (28, N'4th Floor,Above Timber Point,100 Feet Ring Road,Op', 380015, N'Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'Nothing')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (29, N'Timber Point , Manek Tech ,Prahladnagar , Ahmedaba', 323232, N'Ahmedabad', 707, N'One Time', N'Someone is Home', NULL, N'fdsfd')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (30, N'Ahmedabad', 123456, N'Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'123')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (31, N'Ahmedabad', 123456, N'Timber Point', 734, N'One Time', N'Someone is Home', NULL, N'123')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (32, N'525255525', 525252, N'525252', 734, N'Weekly', N'Doorman', NULL, N'undefined')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (33, N'525255525', 123132, N'fgf', 734, N'One Time', N'Someone is Home', NULL, N'132')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (34, N'kjbkjbkb', 121212, N'fkjbdfbdafj', 734, N'One Time', N'Someone is Home', NULL, N'545456')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (35, N'khjkhj', 565465, N'jkjkhjkjh', 707, N'One Time', N'Someone is Home', NULL, N'undefined')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (36, N'12121212', 121212, N'11212', 734, N'One Time', N'Someone is Home', NULL, N'undefined')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (37, N'dsahdsahkjhjksa', 456456, N'lhkjdfsdfshkjhkj', 734, N'One Time', N'Doorman', NULL, N'hgjgjgfh')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (38, N'Timber Point', 123456, N'Manek Tech ', 707, N'One Time', N'Someone is Home', NULL, N'fdsfd')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (39, N'dfgdgdgd', 135232, N'fgdgdfgg', 734, N'One Time', N'Someone is Home', NULL, N'iouuyiy')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (40, N'Moller Street', 260001, N'fghjfghfgh', 734, N'Weekly', N'Doorman', NULL, N'bnmbnm')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (41, N'Moller Street', 260001, N'jhgfhgf', 734, N'One Time', N'Someone is Home', NULL, N'bnmbnm')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (42, N'Manek Tech', 323232, N'Timber Point', 734, N'One Time', N'Someone is Home', NULL, N'undefined')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (43, N'Manek Tech', 323232, N'Timber Point', 734, N'One Time', N'Someone is Home', NULL, N'undefined')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (44, N'gh', 132321, N'sgdfhgh', 750, N'One Time', N'Someone is Home', NULL, N'gh')
GO
INSERT [dbo].[AddUser] ([CustomerId], [Address], [ZipCode], [Apartment], [userId], [serviceTime], [keyInfo], [about_Key], [special_Notes]) VALUES (45, N'Manek Tech', 323232, N'Timber Point', 707, N'One Time', N'Someone is Home', NULL, N'31533')
GO
SET IDENTITY_INSERT [dbo].[AddUser] OFF
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (2, N'Switches', 0, 1)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (3, N'Fans', 1, 1)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (4, N'Blocks & Leakages', 1, 2)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (5, N'Bathroom Fittings', 1, 2)
GO
INSERT [dbo].[Categories] ([categoryId], [subcategoryName], [status], [masterCategoryId]) VALUES (6, N'Home Cleaning', 0, 3)
GO
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[CategoryMaster] ON 
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status]) VALUES (1, N'Electician', 1)
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status]) VALUES (2, N'Plumber', 1)
GO
INSERT [dbo].[CategoryMaster] ([masterCategoryId], [categoryName], [status]) VALUES (3, N'Cleaner', 0)
GO
SET IDENTITY_INSERT [dbo].[CategoryMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[payment] ON 
GO
INSERT [dbo].[payment] ([paymentId], [paymentType], [status], [datetime]) VALUES (2, N'online', N'Success', CAST(N'1900-01-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[payment] ([paymentId], [paymentType], [status], [datetime]) VALUES (3, N'online', N'Success', CAST(N'1900-01-12T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[payment] OFF
GO
SET IDENTITY_INSERT [dbo].[PaymentDetails] ON 
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (8, 704, N'32132121', CAST(N'2019-07-10T00:00:00.000' AS DateTime), 32132, N'Kathan', N'test@gmail.com', 3131510)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (9, 707, N'1235555555555555', CAST(N'2019-07-09T18:30:00.000' AS DateTime), 123, N'hardik', N'hardik@gmail.com', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (10, 707, N'1235555555555555', CAST(N'2019-07-11T18:30:00.000' AS DateTime), 123, N'hardik panchal', N'hardik1@gmail.com', 1234566789)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (11, 707, N'2131113331513151', CAST(N'2019-07-17T18:30:00.000' AS DateTime), 123, N'hardik', N'hardik@gmail.com', 1211313131)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (12, 707, N'1234567891234567', CAST(N'2019-07-17T18:30:00.000' AS DateTime), 456, N'hardik', N'hardik@gmail.com', 1234567891)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (13, 704, N'1312312312312312', CAST(N'2019-07-08T18:30:00.000' AS DateTime), 123, N'johny bhai', N'john@gmail.com', 1231231231)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (24, 707, N'2222222222222222', CAST(N'2019-07-18T18:30:00.000' AS DateTime), 111, N'1111111111111', N'11111111111111', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (25, 704, N'3232323232323232', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 321, N'kathan shah', N'kathan@gmail.com', 2132132132)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (27, 707, N'string', CAST(N'2019-07-23T10:43:48.627' AS DateTime), 0, N'string', N'string', 0)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (28, 707, N'0515153158131361', CAST(N'2019-07-23T18:30:00.000' AS DateTime), 454, N'213213123123123123', N'13123123213123123123', 1231231232)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (29, 707, N'0515153158131361', CAST(N'2019-07-23T18:30:00.000' AS DateTime), 454, N'213213123123123123', N'13123123213123123123', 1231231232)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (30, 707, N'1321153151111531', CAST(N'2019-07-10T18:30:00.000' AS DateTime), 113, N'211131121111321313211', N'111212132131', 1132131321)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (31, 707, N'1321153151111531', CAST(N'2019-07-10T18:30:00.000' AS DateTime), 113, N'211131121111321313211', N'111212132131', 1132131321)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (32, 707, N'1321153151111531', CAST(N'2019-07-10T18:30:00.000' AS DateTime), 113, N'211131121111321313211', N'111212132131', 1132131321)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (33, 707, N'2132321312312312', CAST(N'2019-07-17T18:30:00.000' AS DateTime), 312, N'12312312312', N'123123123', 1312312323)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (34, 707, N'string', CAST(N'2019-07-24T04:44:44.277' AS DateTime), 0, N'string', N'string', 0)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (35, 707, N'2323231231231231', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 131, N'3123123123', N'123123123', 1312313123)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (36, 707, N'3263131231231231', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 111, N'312312321312', N'3123123', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (37, 707, N'3263131231231231', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 111, N'312312321312', N'3123123', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (38, 707, N'3263131231231231', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 111, N'312312321312', N'3123123', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (39, 707, N'2121212121212121', CAST(N'2019-07-16T18:30:00.000' AS DateTime), 121, N'1212121', N'12121', 1212121212)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (40, 707, N'1111111111111111', CAST(N'2019-07-24T18:30:00.000' AS DateTime), 111, N'1111111111111111', N'1111111111111', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (43, 707, N'1111111111111111', CAST(N'2019-07-16T18:30:00.000' AS DateTime), 111, N'11111111', N'1111111', 1111111111)
GO
INSERT [dbo].[PaymentDetails] ([PaymentId], [userId], [Credit_Card_No], [Expire_Date], [CVV], [CardHolder_Name], [Email], [ContactNo]) VALUES (44, 707, N'string', CAST(N'2019-07-23T10:43:48.627' AS DateTime), 123, N'string', N'string', 1234567892)
GO
SET IDENTITY_INSERT [dbo].[PaymentDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[PaymentReceipt] ON 
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (1, 704, N'string', N'string', N'string', N'Jul 23 2019 11:34AM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (2, 707, N'Electician', N'[{"id":6,"price":1203,"subCategoryName":"Bathroom"', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'5454', N'08:30 AM', N'Av. Corrientes 4500, C1195AAQ CABA, Argentina', N'1200', N'1416', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (3, 704, N'string', N'string', N'string', N'Jul 23 2019 11:44AM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (4, 704, N'string', N'string', N'string', N'Jul 23 2019 11:49AM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (5, 704, N'Electician', N'[{"id":2,"price":1000,"subCategoryName":"Others"},', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'jhgkjgh', N'11:00 AM', N'155 6th Ave, New York, NY 10013, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (6, 704, N'Electician', N'[{"id":2,"price":1000,"subCategoryName":"Others"},', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'jhgkjgh', N'11:00 AM', N'155 6th Ave, New York, NY 10013, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (11, 707, N'string', N'string', N'string', N'Jul 23 2019 12:52PM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (12, 707, N'Electrician', N'Other', N'One Time', N'Jul 23 2019  1:15PM', N'Hidden', N'Timber Point', N'13:15:05.384Z', N'Manek Tech ,Timber Point ,Ahmedabad', N'2550', N'3000', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (13, 707, N'string', N'string', N'string', N'Jul 23 2019  1:39PM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (14, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (15, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (16, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (17, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (18, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (19, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (20, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (21, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (22, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (23, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (24, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (25, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (26, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (27, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (28, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (29, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (30, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (31, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (32, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (33, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (34, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (35, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (36, 704, N'string', N'string', N'string', N'Jul 24 2019  6:49AM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (37, 707, N'string', N'string', N'string', N'Jul 24 2019  6:58AM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (38, 704, N'string', N'string', N'string', N'Jul 24 2019  7:04AM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (39, 707, N'string', N'string', N'string', N'Jul 24 2019  7:08AM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (40, 704, N'electrician', N'others', N'one week', N'Jul 24 2019  7:14AM', N'in the car parking', N'Timber point', N'7:30', N'Ahmedabad', N'1400', N'1502', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (41, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (42, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (43, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (44, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (45, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (46, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (47, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (48, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (49, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (50, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (51, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (52, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (53, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (54, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (55, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (56, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (57, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'1212', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (58, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'street', N'08:30 AM', N'1234 Market St, Philadelphia, PA 19107, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (59, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'street', N'08:30 AM', N'1234 Market St, Philadelphia, PA 19107, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (60, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'street', N'08:30 AM', N'1234 Market St, Philadelphia, PA 19107, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (61, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'street', N'08:30 AM', N'1234 Market St, Philadelphia, PA 19107, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (62, 704, N'string', N'string', N'string', N'Jul 24 2019  8:08AM', N'string', N'string', N'string', N'string', N'string', N'string', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (63, 704, N'electrician', N'Others', N'string', N'Jul 24 2019 12:00AM', N'on table', N'timber point', N'7:30', N'Ahmedabad', N'1400', N'1502', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (64, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'street', N'08:30 AM', N'1234 Market St, Philadelphia, PA 19107, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (65, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'street', N'08:30 AM', N'1234 Market St, Philadelphia, PA 19107, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (66, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'street', N'08:30 AM', N'1234 Market St, Philadelphia, PA 19107, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (67, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'street', N'08:30 AM', N'1234 Market St, Philadelphia, PA 19107, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (68, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'456456', N'08:30 AM', N'1441 Broadway, New York, NY 10018, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (69, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 27 2019 12:00AM', N'Someone is Home', N'vbnvbn', N'08:30 AM', N'C1043ABN, Av. Corrientes 1400, C1043ABN CABA, Arge', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (70, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 27 2019 12:00AM', N'Someone is Home', N'vbnvbn', N'08:30 AM', N'C1043ABN, Av. Corrientes 1400, C1043ABN CABA, Arge', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (71, 707, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'jikhk', N'02:30 PM', N'1441 Broadway, New York, NY 10018, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (72, 728, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'dfsdf', N'08:30 AM', N'120 Broadway, New York, NY 10271, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (73, 728, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'dsfdsf', N'01:30 PM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (74, 728, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 27 2019 12:00AM', N'Someone is Home', N'asdads', N'08:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (75, 728, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'dsd', N'08:30 AM', N'120 Broadway, New York, NY 10271, USA', N'2200', N'2596', NULL, NULL)
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (76, 704, N'Ahmedabad', N'388004', N'iconic', N'onetime', N'doorman', N'hidden', N'deep clean', N'abc', N'12', N'500', N'3ML347256C783732C', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (77, 707, N'Plumber', N'["Bathroom","Bedroom","1 BHK"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'New York', N'04:00 PM', N'1216 5th Ave, New York, NY 10029, USA', N'3612', N'4262.16', N'8RU247844N082512U', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (78, 704, N'Ahmedabad', N'388004', N'iconic', N'onetime', N'doorman', N'hidden', N'deep clean', N'abc', N'12', N'500', N'3ML347256C783732C', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (79, 707, N'Plumber', N'["Bathroom","Bedroom","1 BHK"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'New York', N'02:30 PM', N'120 Broadway, New York, NY 10271, USA', N'3612', N'4262.16', N'06976572BY605690K', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (80, 707, N'Plumber', N'["Bathroom","Bedroom","1 BHK"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'New York', N'02:30 PM', N'120 Broadway, New York, NY 10271, USA', N'3612', N'4262.16', N'1P038329TB0112323', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (81, 703, N'Plumber', N'["Bathroom","Bedroom","1 BHK"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'New York', N'01:30 PM', N'1211 6th Ave, New York, NY 10036, USA', N'3612', N'4262.16', N'29P78416S5642521F', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (82, 707, N'Electician', N'["Main control Board","Others","New Electric Ports', N'One Time', N'Jul 24 2019 12:00AM', N'Someone is Home', N'New York', N'12:30 PM', N'1216 5th Ave, New York, NY 10029, USA', N'3400', N'4012', N'82S26965LX6812430', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (83, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 24 2019 12:00AM', N'Someone is Home', N'dsd', N'10:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', N'42473203038993817', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (84, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 24 2019 12:00AM', N'Someone is Home', N'dsd', N'10:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', N'2NW376907D019733F', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (85, 707, N'Electician', N'["Main control Board","Others"]', N'One Time', N'Jul 24 2019 12:00AM', N'Someone is Home', N'dsd', N'10:30 AM', N'1216 5th Ave, New York, NY 10029, USA', N'2200', N'2596', N'0A9070320G129911U', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (86, 728, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 27 2019 12:00AM', N'Someone is Home', N'dfsfsdf', N'07:00 PM', N'Ahmedabad', N'2200', N'2596', N'6PR25683KE778983S', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (87, 704, N'Ahmedabad', N'388004', N'iconic', N'onetime', N'doorman', N'hidden', N'deep clean', N'abc', N'12', N'500', N'3ML347256C783732C', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (88, 728, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Hidden Key', N'fghdfgh', N'04:00 PM', N'New Jersey', N'2200', N'2596', N'3U378466WU047212S', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (89, 728, N'Electician', N'["Others","New Electric Ports"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'test', N'11:00 AM', N'mt', N'2200', N'2596', N'4PR326980M395052A', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (90, 728, N'Electician', N'["Main control Board","Others","New Electric Ports', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'ascsac', N'01:30 PM', N'ascasvc', N'3400', N'4012', N'72E71665831377918', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (91, 707, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'Timber Point', N'11:00 AM', N'Ahmedabad', N'2200', N'2596', N'2TN9639614399501G', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (92, 728, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Jul 26 2019 12:00AM', N'Doorman', N'dfdsf', N'05:30 PM', N'dsfdsf', N'2200', N'2596', N'96V06245P1571952H', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (93, 728, N'Electician', N'["Service 2","Service 3"]', N'Weekly', N'Jul 25 2019 12:00AM', N'Doorman', N'mt', N'01:30 PM', N'timber point', N'2200', N'2596', N'97667516NP229951W', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (94, 728, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'Ahmedabad', N'11:00 AM', N'Ahmedabad', N'2200', N'2596', N'0BU6336537232683R', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (95, 728, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'222222222', N'05:30 PM', N'2222222222', N'2200', N'2596', N'4VA477693W476900W', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (96, 730, N'Electician', N'["Service 1"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'101 test', N'05:30 PM', N'adfdfd', N'1200', N'1416', N'7YP046345C265084V', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (97, 728, N'Electician', N'["Service 1"]', N'One Time', N'Jul 25 2019 12:00AM', N'Someone is Home', N'Timber Point', N'01:30 PM', N'Ahmedabad', N'1200', N'1416', N'15570850P9847421V', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (98, 728, N'Electician', N'["Service 3"]', N'One Time', N'Jul 30 2019 12:00AM', N'Someone is Home', N'fgfg', N'05:30 PM', N'ggdf', N'1200', N'1416', N'6L6569044L0012030', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (99, 707, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Jul 27 2019 12:00AM', N'Someone is Home', N'jkjhkjhk', N'07:00 PM', N'sdsdhjkjk', N'2200', N'2596', N'75S20382829318723', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (100, 707, N'Electician', N'["Service 1","Service 2"]', N'One Time', N'Jul 27 2019 12:00AM', N'Someone is Home', N'Ahmedabad', N'03:30 PM', N'Timber Point , Manek Tech ,Prahladnagar , Ahmedaba', N'2200', N'2596', N'40L44260KW6701600', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (101, 707, N'Electician', N'["Service 2"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'Timber Point', N'11:30 AM', N'Ahmedabad', N'1000', N'1180', N'0Y537642K4370912C', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (102, 734, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'Timber Point', N'02:30 PM', N'Ahmedabad', N'2200', N'2596', N'6MF55125LY2452124', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (103, 734, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Jul 26 2019 12:00AM', N'Someone is Home', N'fgf', N'11:30 AM', N'525255525', N'2200', N'2596', N'1E719743GM8974939', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (104, 734, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Jul 26 2019 12:00AM', N'Doorman', N'lhkjdfsdfshkjhkj', N'08:30 AM', N'dsahdsahkjhjksa', N'2200', N'2596', N'35A75659HT442225A', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (105, 734, N'Electician', N'["Service 1","Service 2"]', N'One Time', N'Jul 31 2019 12:00AM', N'Hidden Key', N'fghfgh', N'02:30 PM', N'fgh', N'2200', N'2596', N'7KT39761AA747134B', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (106, 734, N'Electician', N'["Service 3"]', N'One Time', N'Jul 29 2019 12:00AM', N'Someone is Home', N'undefined', N'12:30 PM', N'Manek Tech', N'1200', N'1416', N'4K468762GM040980V', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (107, 707, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Jul 31 2019 12:00AM', N'Someone is Home', N'Timber Point', N'02:30 PM', N'Manek Tech', N'2200', N'2596', N'9R2412933B7998309', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (108, 750, N'Electician', N'["Service 1","Service 2","Service 3"]', N'One Time', N'Aug  8 2019 12:00AM', N'Someone is Home', N'shyam', N'01:30 PM', N'Shikhar', N'3400', N'4012', N'4GN38427UN963443K', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (109, 707, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Aug 23 2019 12:00AM', N'Someone is Home', N'Shyam', N'05:30 PM', N'https://www.c-sharpcorner.com/article/create-regis', N'2200', N'2596', N'2RV83843B0725813L', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (110, 707, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Aug 22 2019 12:00AM', N'Someone is Home', N'fdsf', N'04:00 PM', N'https://www.c-sharpcorner.com/article/create-regis', N'2200', N'2596', N'2N210972WK3874110', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (111, 707, N'string', N'string', N'string', N'Aug  1 2019 11:02AM', N'string', N'string', N'string', N'https://www.c-sharpcorner.com/article/create-regis', N'string', N'string', N'string', N'string')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (112, 707, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Aug 28 2019 12:00AM', N'Someone is Home', N'Shyam', N'04:30 PM', N'https://www.c-sharpcorner.com/article/create-regis', N'2200', N'2596', N'5H7785982K208943K', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (113, 750, N'Electician', N'["Service 2","Service 3"]', N'One Time', N'Aug 22 2019 12:00AM', N'Someone is Home', N'Rain ', N'04:00 PM', N'Nadiyad', N'2200', N'2596', N'5PJ01007U6271273R', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (114, 707, N'Electician', N'["Service 1","Service 2","Service 3"]', N'One Time', N'Aug 22 2019 12:00AM', N'Someone is Home', N'Shyamal', N'04:00 PM', N'Shivranjani', N'3400', N'4012', N'2MN918949L0805947', N'RE74FMTFHYPSS')
GO
INSERT [dbo].[PaymentReceipt] ([ReceiptId], [userId], [category], [subCategory], [Typeserivce], [date], [keyInfo], [Apartment], [time], [Address], [price], [GrandTotal], [orderID], [TransactionID]) VALUES (115, 871, N'Electician', N'["Service 1","Service 2"]', N'One Time', N'Aug  7 2019 12:00AM', N'Someone is Home', N'303', N'10:30 AM', N'Praheladnagar', N'2200', N'2596', N'5R4354399D273333T', N'RE74FMTFHYPSS')
GO
SET IDENTITY_INSERT [dbo].[PaymentReceipt] OFF
GO
SET IDENTITY_INSERT [dbo].[policy] ON 
GO
INSERT [dbo].[policy] ([policyId], [policyName], [vistingCharge]) VALUES (1, N'standared', CAST(96 AS Numeric(18, 0)))
GO
INSERT [dbo].[policy] ([policyId], [policyName], [vistingCharge]) VALUES (2, N'standaredplus', CAST(99 AS Numeric(18, 0)))
GO
SET IDENTITY_INSERT [dbo].[policy] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 
GO
INSERT [dbo].[Role] ([roleId], [name]) VALUES (1, N'employee')
GO
INSERT [dbo].[Role] ([roleId], [name]) VALUES (2, N'customer')
GO
INSERT [dbo].[Role] ([roleId], [name]) VALUES (3, N'admin')
GO
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[ScheduleTime] ON 
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (1, N'Sun', 0, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 0.5)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (2, N'Mon', 1, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 1)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (3, N'Tue', 2, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 1.5)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (4, N'Wed', 3, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 2)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (5, N'Thu', 4, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 2.5)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (6, N'Fri', 5, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 3)
GO
INSERT [dbo].[ScheduleTime] ([id], [dayName], [dayNumber], [startTime], [endTime], [interval]) VALUES (7, N'Sat', 6, CAST(N'2019-06-21T08:30:00.000' AS DateTime), CAST(N'2019-06-21T20:30:00.000' AS DateTime), 3.5)
GO
SET IDENTITY_INSERT [dbo].[ScheduleTime] OFF
GO
SET IDENTITY_INSERT [dbo].[SubCategories] ON 
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (1, 2, N'Service 1', 1, N'category/xyz.png', 1200, 1, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (2, 2, N'Service 2', 1, N'category/xyz.png', 1000, 2, 2)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (3, 2, N'Service 3', 1, N'category/xyz.png', 1200, 3, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (4, 3, N'Service 1', 1, N'category/xyz.png', 1201, 4, 2)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (5, 3, N'Service 2', 1, N'category/xyz.png', 1202, 5, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (6, 5, N'Service 1', 0, N'category/xyz.png', 1203, 6, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (7, 5, N'Service 2', 0, N'category/xyz.png', 1204, 7, 1)
GO
INSERT [dbo].[SubCategories] ([SubCategoriesId], [categoryId], [subCategoryName], [status], [image], [price], [approxTime], [policyid]) VALUES (8, 5, N'Service 3', 0, N'category/xyz.png', 1205, 8, 1)
GO
SET IDENTITY_INSERT [dbo].[SubCategories] OFF
GO
SET IDENTITY_INSERT [dbo].[user_login] ON 
GO
INSERT [dbo].[user_login] ([loginId], [userId], [userName], [email], [password], [roleId]) VALUES (7, 693, N'TEST', N'test@gmail.com', N'test@123', 1)
GO
SET IDENTITY_INSERT [dbo].[user_login] OFF
GO
SET IDENTITY_INSERT [dbo].[userdetails] ON 
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (693, NULL, N'hardik', NULL, N'9999999999', N'hardik@gmail.com', N'tset', N'test', N'tset', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (704, N'Kathan', N'Kathan', N'Kathan@12345', N'1234567890', N'kathan8794@gmail.com', N'', N'test ', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (707, N'hardik', N'Hardik Panchal', N'Hardik@123', N'9999999999', N'hardikpanchalit@gmail.com', N'Manek Tech , Timber Point ,Ahemdabad', N'test ', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (728, N'kathan', N'kathan', N'321321312', N'1321321321', N'kathan8794@gmail.com', N'Nadiad', N'test ', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (730, N'Vikas', N'vikas16', N'Vikas@123', N'7041456907', N'vikas.vyas@manektech.com', N'acasca', N'test ', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (734, N'kathan', N'kathan', N'Kathan@123', N'1321321321', N'kathan8794@gmail.com', N'string', N'string', N'string', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (742, N'Milan', N'milan.shah@manektech', N'Milan123#', N'2332323232', N'milan.shah@manektech.com', N'aditya icon', N'test ', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (750, N'sdfgdfg', N'Kathan_Shah', N'Kathan@123', N'9456852536', N'kathan.shah@gmail.com', N'Nadiyad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (869, N'V P', N'kathan8794', N'Kathan@123', N'2222222222', N'kathan879@gmail.com', N'sdfdsfsdf', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (870, N'hardik', N'hp', N'Hardik@123', N'2132131321', N'hardik1@gmail.com', N'y', N'test ', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (871, N'Binal', N'binaldave09', N'Binal@123', N'8485856666', N'binaldave09@gmail.com', N'Alien2=4', N'test ', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (872, N'V P', N'kathan8794@gmail.com', N'Kathan@123', N'3333333333', N'k@kathan.com', N'kmf;ldkfa;f', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (873, N'V P', N'kathan8794@gmail.com', N'Kathan@123', N'3333333333', N'kathan87974@gmail.com', N'kmf;ldkfa;f', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (874, N'kathan', N'kathan', N'Kathan@123', N'2222222222', N'john.doe@gmail.com', N'dfg', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (875, N'sddsf', N'kathan8794', N'Kathan@123', N'8797452011', N'kathand8794@gmail.com', N'Ahmedabad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (876, N'kathan', N'kathan8794', N'Kathan@123', N'2222222222', N'kathan87s94@gmail.com', N'Ahmedabad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (877, N'V P', N'kathan879', N'Kathan@123', N'8797452011', N'kathan87k94@gmail.com', N'Ahmedabad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (878, N'kathan', N'kathan8', N'Kathan@123', N'8797452011', N'kathan8794s@gmail.com', N'Ahmedabad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (879, N'V P', N'dsadsadsa', N'Kathan@123', N'2322222222', N'kathan87sdaad94@gmail.com', N'dsasdad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (880, N'V P', N'kathan8794', N'Kathan@123', N'8797452011', N'dfghdfgh@gd.com', N'Ahmedabad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (881, N'zfgdfg', N'kathan8794@gmail.com', N'Kathan@123', N'3223423423', N'dsfsadf23a@dfzg.com', N'fdgdfg', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (882, N'dfgf fdgf', N'kathan8 ', N'Kathan@123', N'2423234234', N'kathan8794@gmssdfail.com', N'dfssdf', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (883, N'V P', N'kathan8794', N'Kathan@123', N'8797452011', N'kathankjhk8794@gmail.com', N'Ahmedabad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (884, N'V P', N'kathan8794@gmail.com', N'Kathan@123', N'8797452011', N'vidhi762@gmail.com', N'Ahmedabad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (885, N'V P', N'kathan8794@gmail.com', N'Kathan@123', N'8797452011', N'kathan879fdfg4@gmail.com', N'Ahmedabad', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (886, N'V P', N'kathan8794@gmail.com', N'Kathan@123', N'2222222222', N'kathan879dfgdfg4@gmail.com', N'dfg', N'', N'', 1)
GO
INSERT [dbo].[userdetails] ([userId], [Name], [userName], [password], [contactNumber], [email], [address], [occupation], [image], [roleId]) VALUES (887, N'V P', N'kathan8794@gmail.com', N'Kathan@123', N'8797452011', N'kathan8794@gmailas.com', N'Ahmedabad', N'', N'', 1)
GO
SET IDENTITY_INSERT [dbo].[userdetails] OFF
GO
ALTER TABLE [dbo].[AddUser]  WITH CHECK ADD  CONSTRAINT [FK_AddUser_userdetails] FOREIGN KEY([userId])
REFERENCES [dbo].[userdetails] ([userId])
GO
ALTER TABLE [dbo].[AddUser] CHECK CONSTRAINT [FK_AddUser_userdetails]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD FOREIGN KEY([masterCategoryId])
REFERENCES [dbo].[CategoryMaster] ([masterCategoryId])
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD FOREIGN KEY([masterCategoryId])
REFERENCES [dbo].[CategoryMaster] ([masterCategoryId])
GO
ALTER TABLE [dbo].[CategoryMaster]  WITH CHECK ADD FOREIGN KEY([masterCategoryId])
REFERENCES [dbo].[CategoryMaster] ([masterCategoryId])
GO
ALTER TABLE [dbo].[login]  WITH CHECK ADD FOREIGN KEY([roleId])
REFERENCES [dbo].[Role] ([roleId])
GO
ALTER TABLE [dbo].[PaymentDetails]  WITH CHECK ADD  CONSTRAINT [FK_PaymentDetails_userdetails] FOREIGN KEY([userId])
REFERENCES [dbo].[userdetails] ([userId])
GO
ALTER TABLE [dbo].[PaymentDetails] CHECK CONSTRAINT [FK_PaymentDetails_userdetails]
GO
ALTER TABLE [dbo].[SubCategories]  WITH CHECK ADD FOREIGN KEY([categoryId])
REFERENCES [dbo].[Categories] ([categoryId])
GO
ALTER TABLE [dbo].[user_login]  WITH CHECK ADD  CONSTRAINT [FK_user_login_user_login] FOREIGN KEY([userId])
REFERENCES [dbo].[userdetails] ([userId])
GO
ALTER TABLE [dbo].[user_login] CHECK CONSTRAINT [FK_user_login_user_login]
GO
ALTER TABLE [dbo].[userdetails]  WITH CHECK ADD  CONSTRAINT [FK__userdetai__roleI__74AE54BC] FOREIGN KEY([roleId])
REFERENCES [dbo].[Role] ([roleId])
GO
ALTER TABLE [dbo].[userdetails] CHECK CONSTRAINT [FK__userdetai__roleI__74AE54BC]
GO
/****** Object:  StoredProcedure [dbo].[addNewUser]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--exec [dbo].[addNewUser] 1, 'krunali', 9045888988, 'krunali@gmail.com','vira',Null,Null, 1,1,'Ramesh','ramesh@gmail.com','12345';

-- =============================================
-- Author:		Nayan Pariya
-- Create date: 1/10/2019
-- Description:	get data from SubCategories table
-- =============================================
--exec [dbo].[addNewUser] 'kaushal', '9545888988', 'kaushal@gmail1111.com','virasat',Null,Null, 1,'Ramesha','12345';
CREATE PROCEDURE [dbo].[addNewUser]

@name varchar(50),
@contactNumber varchar(18),
@emailAddress nvarchar(50),
@address varchar(50),
@image varchar(max) = null,
@occupation varchar(50) = null,
@roleId int,
@userName varchar(50),
@password varchar(50)

AS
BEGIN

SELECT [emailAddress] FROM [myclean].[dbo].[userdetails] WHERE [emailAddress] like @emailAddress
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF NOT EXISTS (SELECT emailAddress FROM userdetails WHERE emailAddress = @emailAddress)
	Begin 
	   INSERT INTO userdetails (name,contactNumber,emailAddress,[address],roleId)VALUES(@name,@contactNumber,@emailAddress,@address,@roleId);

	   INSERT INTO [login] (userName,email,[password],roleId)VALUES (@userName,@emailAddress,@password,@roleId);
	END
END



GO
/****** Object:  StoredProcedure [dbo].[checkExistingUser]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vishal Guptalala>
-- Create date: <22-04-2019>
-- Description:	<chcek xeisting emailaddress>
-- =============================================
--exec [dbo].[checkExistingUser] 'vishal@gmail.com';
CREATE PROCEDURE [dbo].[checkExistingUser]
@email varchar(50),
@outputParam	INT = 0 OUTPUT


	
AS
BEGIN
	
	SET NOCOUNT ON;


	IF EXISTS(SELECT email as emailAddress
                        FROM login
                        WHERE email = @email)
						 SET @outputParam = 1
       ELSE
      BEGIN
            SET @outputParam = 0
      END
      SELECT @outputParam AS outputParam; 
     
END
GO
/****** Object:  StoredProcedure [dbo].[getAllImages]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nayan Pariya
-- Create date: 1/10/2019
-- Description:	get data from SubCategories table
-- =============================================
CREATE PROCEDURE [dbo].[getAllImages]
@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	Select SubCategoriesId,subcategoryName,status,[image],price,approxTime,policyId,categoryId from SubCategories where categoryId = @id
END
GO
/****** Object:  StoredProcedure [dbo].[getMasterData]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		vishal Guptalala	
-- Create date: 6/3/2019
-- Description:	get data from CategoryMaster data
-- =============================================
CREATE PROCEDURE [dbo].[getMasterData]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select masterCategoryId,categoryName,status from CategoryMaster where status = 1

END
GO
/****** Object:  StoredProcedure [dbo].[getMasterDataa]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<vishal Guptalala>
-- Create date: <06-03-2019>
-- Description:	<join of category data>
-- =============================================
CREATE PROCEDURE [dbo].[getMasterDataa] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select CategoryMaster.categoryName,CategoryMaster.status,

	Categories.masterCategoryId,Categories.subcategoryName

	From CategoryMaster Inner Join 

	Categories On 

	CategoryMaster.masterCategoryId = Categories.masterCategoryId;


END
GO
/****** Object:  StoredProcedure [dbo].[getSubCategory]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		vishal Guptalala
-- Create date: 6/3/2019
-- Description:	get data from Categories table.
-- =============================================
CREATE PROCEDURE [dbo].[getSubCategory]
@id INT
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select categoryId,subcategoryName,status from Categories where masterCategoryId = @id 


END
GO
/****** Object:  StoredProcedure [dbo].[loginAddData]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vishal Guptalala>
-- Create date: <27-2-2019>
-- Description:	<For login check email>
-- =============================================

--exec [dbo].[loginAddData] 'yogesh@gmail.com';
CREATE PROCEDURE [dbo].[loginAddData]
 
  @email nvarchar(50)
 
AS
BEGIN	
    SELECT email as email,[password]  FROM login WHERE email = @email;
	 


	SET NOCOUNT ON;

    -- Insert statements for procedure here

END
GO
/****** Object:  StoredProcedure [dbo].[Registration]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:     <Vishal Gupta>
-- Create date: <15 feb 2019> 
-- Description:   <For add Data from Registration>
-- ============================================= 
--exec [dbo].[Registration] 'kasdhdsvghhish', '66658sdv8855', 'keshsdvbghdxish@gmail1111.com31','london',Null,Null, 1,'rangddsvxbghila','12345', 2;
CREATE PROCEDURE [dbo].[Registration] @name          VARCHAR(50), 
                                     @contactNumber VARCHAR(18), 
                                     @email  NVARCHAR(50), 
                                     @address       VARCHAR(50), 
                                     @image         VARCHAR(max) = NULL, 
                                     @occupation    VARCHAR(50) = NULL, 
                                     @roleId        INT, 
                                     @userName      VARCHAR(50), 
                                     @password      VARCHAR(50),
									 @outputParam	INT = 0 OUTPUT
AS 

  BEGIN 
      --DECLARE @returnValue INT; 

      SET nocount ON; 

      BEGIN 
          BEGIN TRANSACTION [addUserData] 

          BEGIN try 
              IF NOT EXISTS(SELECT * 
                            FROM   userdetails 
                            WHERE  email = @email) 
                BEGIN 
                    INSERT INTO userdetails 
                                (NAME, 
                                 contactnumber, 
                                 email, 
                                 [address], 
                                 roleid) 
                    VALUES     (@name, 
                                @contactNumber, 
                                @email, 
                                @address, 
                                @roleId); 

                    INSERT INTO [user_login] 
                                (username, 
                                 email, 
                                 [password], 
                                 roleid) 
                    VALUES      (@userName, 
                                 @email, 
                                 @password, 
                                 @roleId); 

                    SET @outputParam = Scope_identity(); 
                END 
              ELSE 
                BEGIN 
                    SET @outputParam = 0; 
                END 

			 COMMIT TRANSACTION [addUserData]
          END try 

          BEGIN catch 
              ROLLBACK TRANSACTION [addUserData] 
          END catch 
      END 
      SELECT @outputParam AS outputParam; 
  END 
GO
/****** Object:  StoredProcedure [dbo].[spTimeSchedule]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spTimeSchedule] 
		@dayNumber int
As
BEGIN
	select * from ScheduleTime where dayNumber = @dayNumber
END
GO
/****** Object:  StoredProcedure [dbo].[userLogin]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vishal Guptalala>
-- Create date: <27-2-2019>
-- Description:	<For login check email>
-- =============================================

--exec [dbo].[userLogin] 'vikas@gmail.com';
CREATE PROCEDURE [dbo].[userLogin]
 
  @email nvarchar(50),
  @password varchar(50)
  

 
AS
BEGIN	
    SELECT userName,userId, email ,roleId  FROM userdetails 
	WHERE email = @email AND [password] = @password;
	 
	--SET NOCOUNT ON;

    -- Insert statements for procedure here

END
GO
/****** Object:  StoredProcedure [dbo].[UserRegister]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UserRegister]
			(
			@Name nvarchar(50),
			@userName nvarchar(50),
			@password nvarchar(50),
			@contactNumber nvarchar(50),
			@email nvarchar(50),
			@address varchar(50),
			@occupation varchar(50),
			@image varchar(MAX),
			@roleId int
			)

			AS
			BEGIN
			INSERT INTO userdetails 
					(
					[Name],
					[userName],
					[password],
					[contactNumber],
					[email],
					[address],
					[occupation],
					[image],
					[roleId]
					)
			VALUES 
					(
					@Name,
					@userName,
					@password,
					@contactNumber,
					@email,
					@address,
					@occupation,
					@image,
					@roleId
					)
			END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddUser]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AddUser] 
	(
	@userId int,
	@Address VARCHAR(50),
	@ZipCode INT,
	@Apartment VARCHAR(50),
	@serviceTime nvarchar(50),
	@keyInfo nvarchar(50),
	@about_Key nvarchar(50),
	@special_Notes nvarchar(200)
	
	
	)
	AS 
	BEGIN
	
	INSERT INTO  AddUser
		(
			[userId],
			[Address],
			[ZipCode],
			[Apartment],
			[serviceTime],
			[keyInfo],
			[about_Key],
			[special_Notes]
		) 
	Values
		(
			@userId ,
			@Address,
			@ZipCode,
			@Apartment,
			@serviceTime,
			@keyInfo,
			@about_Key,
			@special_Notes

	    )
		
	END
	--exec usp_AddUser @userid='704',@Address='Ahmedabad',@ZipCode='388004',@Apartment='iconic',@serviceTime ='onetime',@keyInfo='doorman',@about_Key ='hidden',@special_Notes ='deep clean' 
			
GO
/****** Object:  StoredProcedure [dbo].[usp_changePassword]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_changePassword]
@userId int,
@password nvarchar(50)

as
begin
update userdetails set [password]=@password
where userId = @userId 
end
GO
/****** Object:  StoredProcedure [dbo].[usp_EditProfile]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[usp_EditProfile]
@userId int,
@userName nvarchar(50),
@contactNumber nvarchar(50),
@email nvarchar(50),
@address nvarchar(50)

as
begin
update userdetails set [userName]=@userName,[contactNumber]=@contactNumber,[email]=@email,[address]=@address 
	
where userId = @userId 
end
GO
/****** Object:  StoredProcedure [dbo].[usp_email]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_email] 

	@userId int
as
begin 
select * from userdetails where userId = @userId 
end
--exec usp_email @email = 'kathan8794@gmail.com'
GO
/****** Object:  StoredProcedure [dbo].[usp_getCardName]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_getCardName]
 @SubcategoriesId VARCHAR(MAX)
as
declare @sql_query varchar(max)
SET @sql_query=' select * from SubCategories where SubcategoriesId IN ('+ @SubcategoriesId+')'
exec (@sql_query)
--exec usp_getCardName @SubcategoriesId='2,7'
GO
/****** Object:  StoredProcedure [dbo].[usp_Payment]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_Payment]
(
	@userId int,
	@Credit_Card_No nvarchar(50),
	@Expire_Date datetime,
	@CVV int,
	@CardHolder_Name nvarchar(50),
	@Email nvarchar(50),
	@ContactNo int

)
AS
BEGIN
INSERT INTO PaymentDetails
(
	
	[userId],
	[Credit_Card_No],
	[Expire_Date],
	[CVV],
	[CardHolder_Name],
	[Email],
	[ContactNo]
)

VALUES
(
	
	@userId,
	@Credit_Card_No,
	@Expire_Date,
	@CVV,
	@CardHolder_Name,
	@Email,
	@ContactNo
)

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Receipt]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_Receipt]
(

	@userId int,
	@category nvarchar(50),
	@subCategory	nvarchar(50),	
	@Typeserivce	nvarchar(50),	
	@date	nvarchar(50),	
	@keyInfo	nvarchar(50),	
	@Apartment	nvarchar(50),	
	@time	nvarchar(50),	
	@Address	nvarchar(50),	
	@price	nvarchar(50),	
	@GrandTotal	nvarchar(50),
	@orderID nvarchar(50),
	@TransactionID nvarchar(50)
	
)
AS
BEGIN
INSERT INTO PaymentReceipt
(

	[userId],
	[category],
	[subCategory],
	[Typeserivce],
	[date],
	[keyInfo],
	[Apartment],	
	[time],	
	[Address],	
	[price],		
	[GrandTotal],
	[orderID],
	[TransactionID]
		

)

VALUES
(
	
	
	@userId,
	@category,
	@subCategory,	
	@Typeserivce,	
	@date,	
	@keyInfo,	
	@Apartment,	
	@time,	
	@Address,	
	@price,	
	@GrandTotal,
	@orderID ,
	@TransactionID
		
)

END

--exec usp_Receipt @userid='704',@category='Ahmedabad',@subCategory='388004',@Typeserivce='iconic',@date ='onetime',@keyInfo='doorman',@Apartment ='hidden',@time ='deep clean',@Address='abc',@price ='12',@GrandTotal='500',@orderID='3ML347256C783732C' ,	@TransactionID='RE74FMTFHYPSS' ,@Status='Completd'	 
GO
/****** Object:  StoredProcedure [dbo].[usp_userExist]    Script Date: 8/2/2019 7:14:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_userExist]

@email nvarchar(50)
as
begin
select * from userdetails where  email = @email 
end
--exec usp_userExist @email = 'kathan8794@gmail.com'
GO
USE [master]
GO
ALTER DATABASE [myclean] SET  READ_WRITE 
GO
