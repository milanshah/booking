﻿using eService.BAL.Repository;
using eService.DataContext.Models;
using System.Collections.Generic;


namespace eService.DAL.Manager
{
    public class DashboardManager
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly DashboardRepository repo = new DashboardRepository();


        public List<CategoryMasterDataContext> CategoryMasterData()
        {
            List<CategoryMasterDataContext> returnvalue = repo.getCategoryMaster();

            return returnvalue;

        }

        public List<CategoriesDataContext> CategoriesData(int id)
        {
            return repo.CategoriesData(id);
        }


        public List<SubCategoryDataContext> SubCategoryData(int id)
        {
            List<SubCategoryDataContext> returnvalue = repo.SubCategoryData(id);
            return returnvalue;
        }
        // For Diaplay Master CategoryLIst
        public List<CategoryMasterDataContext> CategoryDetail(string Action)
        {
            List<CategoryMasterDataContext> returnvalue = repo.CategoryDetail(Action);
            return returnvalue;
        }

        //for Pagination
        public List<CategoryMasterDataContext> Pagination(int pageIndex, int pageSize)
        {
            List<CategoryMasterDataContext> returnvalue = repo.Pagination(pageIndex, pageSize);
            return returnvalue;
        }
        //Edit Category
        public bool MasterCategoryEdit(CategoryMasterDataContext Details)
        {
            return repo.MasterCategoryEdit(Details);
        }

        ////get category data
        //public CategoryMasterDataContext MasterCategoryEdit(int masterCategoryId)
        //{

        //    return repo.MasterCategoryEdit(masterCategoryId);
        //}

        //Master Category Delete
        public string MasterCategoryDelete(int masterCategoryId)
        {
            return repo.MasterCategoryDelete(masterCategoryId);
        }

        //AddMasterCategory

        public bool AddMasterCategory(CategoryMasterDataContext Details)
        {
            return repo.AddMasterCategory(Details);
        }
        //checkbox
        public bool AdminControl(AdminUpdateStatus adminUpdateStatus)
        {
            return repo.AdminControl(adminUpdateStatus);
        }
       
        //Useractivecheckbox
        public bool ActiveUserCheckbox(UserListContext userListContext)
        {
            return repo.ActiveUserCheckbox(userListContext);
        }
        //

        // 
        //public bool CategoryMaster(AdminUpdateStatus adminUpdateStatus)
        //{
        //    return repo.AdminControl(adminUpdateStatus);
        //}
        //
        //public bool SubCategories(AdminUpdateStatus adminUpdateStatus)
        //{
        //    return repo.AdminControl(adminUpdateStatus);
        //}
        //MasterCategoryexist
        public bool MasterCategoryExist(string categoryName, int id)
        {

            return repo.MasterCategoryExist(categoryName, id);
        }

        // Category Add Update Delete

        //for category List
        public List<CategoryMasterDataContext> CategoryList()
        {
            List<CategoryMasterDataContext> returnvalue = repo.CategoryList();
            return returnvalue;
        }

        // For Category Edit
       
        public bool CategoryEdit(CategoryMasterDataContext Details)
        {
            return repo.CategoryEdit(Details);
        }

        //CategoryAdd

        public bool CategoryAdd(CategoryMasterDataContext Details)
        {
            return repo.CategoryAdd(Details);
        }

        //Categorydelete
        public string CategoryDelete(int categoryId)
        {
            return repo.CategoryDelete(categoryId);
        }

        //Categoryexist
        public bool CategoryExist(string subcategoryName, int id)
        {

            return repo.CategoryExist(subcategoryName, id);
        }

        //Category Checkbox
        public bool CategoryCheckbox(AdminUpdateStatus adminUpdateStatus)
        {
            return repo.CategoryCheckbox(adminUpdateStatus);
        }

        //for Pagination
        public List<CategoryMasterDataContext> CategoryPagination(int pageIndex, int pageSize)
        {
            List<CategoryMasterDataContext> returnvalue = repo.CategoryPagination(pageIndex, pageSize);
            return returnvalue;
        }

        //for CheckboxList
        public List<CategoryMasterDataContext> CheckboxList()
        {
            List<CategoryMasterDataContext> returnvalue = repo.CheckboxList();
            return returnvalue;
        }


        //For SubCategory Add Update Delete
        // For SubCategoryList
        public List<SubCategoryDataContext> SubCategoryList()
        {
            List<SubCategoryDataContext> returnvalue = repo.SubCategoryList();
            return returnvalue;
        }
        // For SubCategory Edit

        public bool SubCategoryEdit(SubCategoryDataContext Details)
        {
            return repo.SubCategoryEdit(Details);
        }

        //for SubCategory CheckboxList
        public List<SubCategoryDataContext> SubCheckboxList()
        {
            List<SubCategoryDataContext> returnvalue = repo.SubCheckboxList();
            return returnvalue;
        }
        //SubCategorydelete
        public string SubCategoryDelete(int SubCategoriesId)
        {
            return repo.SubCategoryDelete(SubCategoriesId);
        }
        //SubCategoryAdd

        public bool SubCategoryAdd(SubCategoryDataContext Details)
        {
            return repo.SubCategoryAdd(Details);
        }
        //SubCategoryexist
        public bool SubCategoryExist(string subcategoryName, int id)
        {

            return repo.SubCategoryExist(subcategoryName, id);
        }

        //SubCategory Checkbox
        public bool SubCategoryCheckbox(AdminUpdateStatus adminUpdateStatus)
        {
            return repo.SubCategoryCheckbox(adminUpdateStatus);
        }

        //for SubCategory Pagination
        public List<SubCategoryDataContext> SubCategoryPagination(int pageIndex, int pageSize)
        {
            List<SubCategoryDataContext> returnvalue = repo.SubCategoryPagination(pageIndex, pageSize);
            return returnvalue;
        }
        public List<UserListContext> getuserlist()
        {
            var returnvalue = repo.getuserlist();

            return returnvalue;

        }
        public List<UserListContext> getsingleuserlist(int userId)
        {
            var returnvalue = repo.getsingleuserlist(userId);

            return returnvalue;

        }
             public bool Updatesetting(SettingContext settingmodel)
        {
            var returnvalue = repo.Updatesetting(settingmodel);

            return returnvalue;

        }
        public bool updatetemplate(SettingContext settingmodel)
        {
            var returnvalue = repo.updatetemplate(settingmodel);

            return returnvalue;

        }
        public List<SettingContext> getsetting(string type)
        {
            var returnvalue = repo.getsetting(type);

            return returnvalue;

        }
        public List<SettingContext> gettemplate(string type)
        {
            var returnvalue = repo.gettemplate(type);

            return returnvalue;

        }
        public List<SettingContext> getedittemplate(int SettingId)
        {
            var returnvalue = repo.getedittemplate(SettingId);

            return returnvalue;

        }
        
    }
}
