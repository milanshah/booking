﻿using eService.BAL.Repository;
using eService.DataContext.Models;
using System.Collections.Generic;

namespace eService.DAL.Manager
{
    public class UserManager
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public readonly UserRepository repo = new UserRepository();

        //Login
        public RegistrationDataContext AuthenticateUser(string email, string password)
        {
            return repo.AuthenticateUser(email, password);
        }
        //Registration
        public bool RegisterUser(RegistrationDataContext users)
        {
            return repo.RegisterUser(users);
        }

        public List<TimeSchedule> TimeSchedules(int dayNumber)
        {
            UserRepository userRepository = new UserRepository();
            List<TimeSchedule> returnvalue = userRepository.TimeSchedules(dayNumber);
            return returnvalue;

        }
        public bool AddUser(UserDetails UserDetails)
        {
            return repo.AddUser(UserDetails);
        }

        //payment
        public bool Payment(PaymentDataContext paymentData)
        {

            return repo.Payment(paymentData);
        }
        //category
        public List<CategoryDetails> CategoryDetail(string CategoryValues)
        {

            return repo.CategoryDetail(CategoryValues);
        }
        //payment
        public bool Payment(PaymentReceipt paymentData)
        {

            return repo.Receipt(paymentData);
        }

        //get user data
        public UserProfile profile(int userId)
        {

            return repo.profile(userId);
        }
        //get user order
        public List<Userorder> userorder(int userId)
        {

            return repo.userorder(userId);
        }
        
        //get user order
        public List<Userorder> Usercancelorder(int userId)
        {

            return repo.Usercancelorder(userId);
        }
        //get user download order in pdf
        public Userorder Downloadorderinpdf(string orderId)
        {
             
            return repo.Downloadorderinpdf(orderId);
        }

        //get user User Order Delete in pdf
        public Userorder UserOrderDelete(string orderId)
        {

            return repo.UserOrderDelete(orderId);
        }
        //get user download order in pdf
        public string Download(string orderId, string filepath)
        {

            return repo.Download(orderId, filepath);
        }

        //post user data
        public bool editProfile(UserProfile editProfile)
        {
            return repo.EditProfile(editProfile);
        }
        //change password
        public bool ChngPassword(ChangePassword Password)
        {
            return repo.ChngPassword(Password);
        }
        public bool IsuserExist(string email)
        {

            return repo.IsuserExist(email);
        }

        //for forgot password
        public bool ForgotPassword(ForgotPassword forgot)
        {

            return repo.ForgotPassword(forgot);
        }
        //create password
        //change password
        public bool CreatePassword(Create_Password Password)
        {
            return repo.CreatePassword(Password);
        }
    }
}
