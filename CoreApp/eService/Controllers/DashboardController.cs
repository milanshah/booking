﻿using eService.DAL.Manager;
using eService.DataContext.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Net.Http.Headers;
using System.Web.Http.Cors;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace eService
{
    [Route("api/Dashboard")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DashboardController : ControllerBase
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly DashboardManager manager = new DashboardManager();

        private readonly ILogger _logger;

        public DashboardController(ILogger<DashboardController> logger, IHostingEnvironment hostingEnvironment)
        {
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
        }
      
        public IActionResult Index()
        {
            _logger.LogInformation("Log message in the Index() method");
            return Ok();
           
        }

        public IActionResult About()
        {
            _logger.LogInformation("Log message in the About() method");

            return Ok();
        }

        [HttpGet]
        [Route("CategoriesData")]
        public IActionResult CategoriesData(int id)
        {
            List<CategoriesDataContext> returndata = manager.CategoriesData(id);

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }

        [HttpGet]
        [Route("CategoryMasterData")]
        public IActionResult CategoryMasterData()
        {
            List<CategoryMasterDataContext> returndata = manager.CategoryMasterData();

            if (returndata == null)
            {
                return NotFound();
            }

            LogError("acascascascas"); 
            return Ok(returndata);

        }

        [HttpGet]
        [Route("SubCategoryData")]
        public IActionResult SubCategoryData(int id)
        {
            List<SubCategoryDataContext> returndata = manager.SubCategoryData(id);
            try
            {
                if (returndata == null)
                {
                    return NotFound();
                }

               

            }

            catch(Exception ex)
            {
                _logger.LogInformation("Log message in the Index() method");
            }
            return Ok(returndata);
        }

        //For Display Master Category Data

        [HttpGet]
        [Route("MasterCategoryList")]
        public IActionResult CategoryDetail(string Action)
        {
            List<CategoryMasterDataContext> returndata = manager.CategoryDetail(Action);

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }


        //For Pagination
        [HttpGet]
        [Route("Pagination")]
        public IActionResult Pagination(int pageIndex, int pageSize)
        {
            List<CategoryMasterDataContext> returndata = manager.Pagination(pageIndex, pageSize);

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }

        [HttpPost]
        [Route("MasterCategoryEdit")]

        public IActionResult MasterCategoryEdit(CategoryMasterDataContext Details)
        {
            DashboardManager DashboardManager = new DashboardManager();
            if (DashboardManager.MasterCategoryExist(Details.categoryName, Details.masterCategoryId))
            {
                return Ok(new { message = "Category Already exist" });
            }
            if (!string.IsNullOrWhiteSpace(Details.ToString()))
            {

                if (DashboardManager.MasterCategoryEdit(Details))
                {
                    return Ok(new { status = 200, message = "Category Update Successfully" });
                }
                else
                {
                    return Ok(new { status = 200, message = " fail" });
                }
            }
            return Ok(new { message = " fail 1" });
        }

        //get Catgory data for edit 

        //[HttpGet]
        //[Route("GetCategory")]
        //public IActionResult MasterCategoryEdit(int masterCategoryId)
        //{
        //    CategoryMasterDataContext List = manager.MasterCategoryEdit(masterCategoryId);
        //    if (masterCategoryId > 0)
        //    {
        //        return Ok(List);
        //    }
        //    else
        //    {
        //        return BadRequest(new { status = 401 });
        //    }

        //}


        [HttpGet]
        [Route("MasterCategoryDelete")]
        public IActionResult MasterCategoryDelete(int masterCategoryId)
        {
            DashboardManager DashboardManager = new DashboardManager();

            if (DashboardManager.MasterCategoryDelete(masterCategoryId) == "Category Already Use")
            {
                return Ok(new { message = "Category Already Use" });
            }
            else
            {
                return Ok(new { message = "Category Delete Successfully " });
            }


        }
        //Add Master Category
        [HttpPost]
        [Route("AddMasterCategory")]

        public IActionResult AddMasterCategory(CategoryMasterDataContext Details)
        {
            DashboardManager DashboardManager = new DashboardManager();
            if (DashboardManager.MasterCategoryExist(Details.categoryName, 0))
            {
                return Ok(new { message = "Category Already exist" });
            }
            if (!string.IsNullOrWhiteSpace(Details.ToString()))
            {

                if (DashboardManager.AddMasterCategory(Details))
                {
                    return Ok(new { status = 200, message = "Category Add Successfully" });
                }
                else
                {
                    return Ok(new { status = 200, message = " fail" });
                }

            }
            return Ok(new { message = " fail 1" });
        }

        //For MasterCategoryCheckbox
        [HttpPost]
        [Route("MasterCategoryCheckbox")]
        public IActionResult AdminControl(AdminUpdateStatus adminUpdateStatus)
        {
            DashboardManager DashboardManager = new DashboardManager();

            if (DashboardManager.AdminControl(adminUpdateStatus))
            {
                return Ok(new { status = 200, message = "Category Add Successfully" });
            }
            else
            {
                return Ok(new { status = 200, message = "Category Remove Successfully" });
            }
        }
        //For MasterCategoryCheckbox
        [HttpPost]
        [Route("ActiveUserCheckbox")]

        public IActionResult ActiveUserCheckbox(UserListContext userListContext)
        {
            DashboardManager DashboardManager = new DashboardManager();

            if (DashboardManager.ActiveUserCheckbox(userListContext))
            {
                return Ok(new { status = 200, message = "User Activated Successfully" });
            }
            else
            {
                return Ok(new { status = 200, message = "Category InActive Successfully" });
            }
        }
        //for MasterCategoryExist
        [HttpGet]
        [Route("MasterCategoryExist")]
        public IActionResult MasterCategoryExist(string categoryName, int id)
        {

            DashboardManager DashboardManager = new DashboardManager();
            if (DashboardManager.MasterCategoryExist(categoryName, id))
            {
                return Ok(new { message = "Category Already exist" });
            }
            return NotFound();

        }

        // Category Add Update Delete

        //for category list 
        [HttpGet]
        [Route("CategoryList")]
        public IActionResult CategoryList()
        {
            List<CategoryMasterDataContext> returndata = manager.CategoryList();

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }

        // For Category Edit
        [HttpPost]
        [Route("CategoryEdit")]

        public IActionResult CategoryEdit(CategoryMasterDataContext Details)
        {
            DashboardManager DashboardManager = new DashboardManager();
            if (DashboardManager.CategoryExist(Details.subcategoryName, Details.categoryId))
            {
                return Ok(new { message = "Category Already exist" });
            }
            if (!string.IsNullOrWhiteSpace(Details.ToString()))
            {

                if (DashboardManager.CategoryEdit(Details))
                {
                    return Ok(new { status = 200, message = "Category Update Successfully" });
                }
                else
                {
                    return Ok(new { status = 200, message = " fail" });
                }
            }
            return Ok(new { message = " fail 1" });
        }

        //Add Category
        [HttpPost]
        [Route("CategoryAdd")]

        public IActionResult CategoryAdd(CategoryMasterDataContext Details)
        {
            DashboardManager DashboardManager = new DashboardManager();
            if (DashboardManager.CategoryExist(Details.subcategoryName, 0))
            {
                return Ok(new { message = "Category Already exist" });
            }
            if (!string.IsNullOrWhiteSpace(Details.ToString()))
            {
                if (DashboardManager.CategoryAdd(Details))
                {
                    return Ok(new { status = 200, message = "Category Add Successfully" });
                }
                else
                {
                    return Ok(new { status = 200, message = " fail" });
                }
            }
            return Ok(new { message = " fail 1" });
        }

        [HttpGet]
        [Route("CategoryDelete")]
        public IActionResult CategoryDelete(int categoryId)
        {
            DashboardManager DashboardManager = new DashboardManager();

            if (DashboardManager.CategoryDelete(categoryId) == "Category Already Use")
            {
                return Ok(new { message = "Category Already Use" });
            }
            else
            {
                return Ok(new { message = "Category Delete Successfully " });
            }
        }

        //for CategoryExist
        [HttpGet]
        [Route("CategoryExist")]
        public IActionResult CategoryExist(string subcategoryName, int id)
        {

            DashboardManager DashboardManager = new DashboardManager();
            if (DashboardManager.CategoryExist(subcategoryName, id))
            {
                return Ok(new { message = "Category Already exist" });
            }
            return NotFound();
        }
        //For CategoryCheckbox
        [HttpPost]
        [Route("CategoryCheckbox")]

        public IActionResult CategoryCheckbox(AdminUpdateStatus adminUpdateStatus)
        {
            DashboardManager DashboardManager = new DashboardManager();

            if (DashboardManager.CategoryCheckbox(adminUpdateStatus))
            {
                return Ok(new { status = 200, message = "Category Add Successfully" });
            }
            else
            {
                return Ok(new { status = 200, message = "Category Remove Successfully" });
            }
        }

        //For Category Pagination
        [HttpGet]
        [Route("CategoryPagination")]
        public IActionResult CategoryPagination(int pageIndex, int pageSize)
        {
            List<CategoryMasterDataContext> returndata = manager.CategoryPagination(pageIndex, pageSize);

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }
        //selected Checkboxlist
        [HttpGet]
        [Route("CheckboxList")]
        public IActionResult CheckboxList()
        {
            List<CategoryMasterDataContext> returndata = manager.CheckboxList();

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }

        // For Subcategory Add Update Delete
        [HttpGet]
        [Route("SubCategoryList")]
        public IActionResult SubCategoryList()
        {
            List<SubCategoryDataContext> returndata = manager.SubCategoryList();

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }
        [HttpPost]
        [Route("SubCategoryEdit")]

        public IActionResult SubCategoryEdit(SubCategoryDataContext Details)
        {
            DashboardManager DashboardManager = new DashboardManager();
            if (DashboardManager.SubCategoryExist(Details.subcategoryName, Details.SubCategoriesId))
            {
                return Ok(new { message = "Category Already exist" });
            }
            if (!string.IsNullOrWhiteSpace(Details.ToString()))
            {

                if (DashboardManager.SubCategoryEdit(Details))
                {
                    return Ok(new { status = 200, message = "Category Update Successfully" });
                }
                else
                {
                    return Ok(new { status = 200, message = " fail" });
                }
            }
            return Ok(new { message = " fail 1" });
        }

        //selected SubCheckboxList
        [HttpGet]
        [Route("SubCheckboxList")]
        public IActionResult SubCheckboxList()
        {
            List<SubCategoryDataContext> returndata = manager.SubCheckboxList();

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }
        [HttpGet]
        [Route("SubCategoryDelete")]
        public IActionResult SubCategoryDelete(int SubCategoriesId)
        {
            DashboardManager DashboardManager = new DashboardManager();

            if (DashboardManager.SubCategoryDelete(SubCategoriesId) == "Subcategory Delete Successfully")
            {
                return Ok(new { message = "Subcategory Delete Successfully" });
            }
            else
            {
                return Ok(new { message = "Fail" });
            }
        }

        //Subcategory Add
        [HttpPost]
        [Route("SubCategoryAdd")]

        public IActionResult SubCategoryAdd(SubCategoryDataContext Details)
        {
            try
            {
                DashboardManager DashboardManager = new DashboardManager();
                if (DashboardManager.SubCategoryExist(Details.subcategoryName, 0))
                {
                    return Ok(new { message = "Category Already exist" });
                }
                if (!string.IsNullOrWhiteSpace(Details.ToString()))
                {
                    if (DashboardManager.SubCategoryAdd(Details))
                    {
                        return Ok(new { status = 200, message = "Category Add Successfully" });
                    }
                    else
                    {
                        return Ok(new { status = 200, message = " fail" });
                    }
                }
                return Ok(new { message = " fail 1" });
            }
            catch (System.Exception ex)
            {
                LogError(ex.ToString());
                return Ok("Sub category is not add " + ex.Message);
            }
        }

        //for CategoryExist
        [HttpGet]
        [Route("SubCategoryExist")]
        public IActionResult SubCategoryExist(string subcategoryName, int id)
        {

            DashboardManager DashboardManager = new DashboardManager();
            if (DashboardManager.SubCategoryExist(subcategoryName, id))
            {
                return Ok(new { message = "Category Already exist" });
            }
            return NotFound();
        }

        //For SubCategoryCheckbox
        [HttpPost]
        [Route("SubCategoryCheckbox")]

        public IActionResult SubCategoryCheckbox(AdminUpdateStatus adminUpdateStatus)
        {
            DashboardManager DashboardManager = new DashboardManager();

            if (DashboardManager.SubCategoryCheckbox(adminUpdateStatus))
            {
                return Ok(new { status = 200, message = "Category Add Successfully" });
            }
            else
            {
                return Ok(new { status = 200, message = "Category Remove Successfully" });
            }
        }
        //For Category Pagination
        [HttpGet]
        [Route("SubCategoryPagination")]
        public IActionResult SubCategoryPagination(int pageIndex, int pageSize)
        {
            List<SubCategoryDataContext> returndata = manager.SubCategoryPagination(pageIndex, pageSize);

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }



        private readonly IHostingEnvironment _hostingEnvironment;

        
        //Upload Image

        [HttpPost]
        [Route("ImageUpload")]
        public ActionResult UploadFile()
        {
            try
            {
                Microsoft.AspNetCore.Http.IFormCollection x = Request.Form;
                Microsoft.AspNetCore.Http.IFormFile file = Request.Form.Files[0];
                //var id = Request.Form["SubCategoriesId"];
                Microsoft.Extensions.Primitives.StringValues image = Request.Form["image"];

                //string webRootPath = _hostingEnvironment.WebRootPath;
                string webRoot = ConfigurationManager.AppSettings["UploadPath"].ToString();
                LogError(webRoot);
                if (!Directory.Exists(webRoot))
                {
                    Directory.CreateDirectory(webRoot);
                }
                if (file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    LogError(fileName);
                    string fullPath = Path.Combine(webRoot, fileName);
                    LogError(fullPath);
                    using (FileStream stream = new FileStream(fullPath, FileMode.Create))
                    {
                        SqlParameter[] objParameter = new SqlParameter[1];
                        objParameter[0] = new SqlParameter("@image", fileName);
                        file.CopyTo(stream);
                    }
                }
                return Ok("Upload Successful.");
            }
            catch (System.Exception ex)
            {
                LogError(ex.ToString());
                return Ok("Upload Failed: " + ex.Message);
            }
        }

        private void LogError(string ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            //string path = @"D:\DemoProject\Booking site\CoreApp\Log\ErrorLog.txt"; //Server.MapPath("~/ErrorLog/ErrorLog.txt");
            //using (StreamWriter writer = new StreamWriter(path, true))
            //{
            //    writer.WriteLine(message);
            //    writer.Close();
            //}
        }
        [HttpGet]
        [Route("getuserlist")]
        public IActionResult getuserlist()
        {
            List<UserListContext> returndata = manager.getuserlist();

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }
        [HttpGet]
        [Route("getsingleuserlist")]
        public IActionResult getsingleuserlist(int userId)
        {
            List<UserListContext> returndata = manager.getsingleuserlist(userId);

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }
        [HttpGet]
        [Route("getsetting")]
        public IActionResult getSMTPSetting(string Type)
        {
            string type = "smtp";
            List<SettingContext> returndata = manager.getsetting(type);

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }
        [HttpGet]
        [Route("gettemplate")]
        public IActionResult gettemplate()
        {
            string type = "template";
            List<SettingContext> returndata = manager.gettemplate(type);

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }
        [HttpGet]
        [Route("getedittemplate")]
        public IActionResult getedittemplate(int SettingId)
        {
            string type = "template";
            List<SettingContext> returndata = manager.getedittemplate(SettingId);

            if (returndata == null)
            {
                return NotFound();
            }

            return Ok(returndata);

        }
        [HttpPost]
        [Route("Updatesetting")]
        public IActionResult UpdateSMTPSetting(SettingContext settingmodel)
        {
            //string type = "smtp";
           // bool returndata = manager.Updatesetting(settingmodel);
            DashboardManager DashboardManager = new DashboardManager();

            if (DashboardManager.Updatesetting(settingmodel))
            {
                return Ok(new { status = 200, message = "Category Add Successfully" });
            }
            else
            {
                return Ok(new { status = 200, message = "Category Remove Successfully" });
            }
            //if (returndata == null)
            //{
            //    return NotFound();
            //}

            //return Ok(returndata);

        }
        [HttpPost]
        [Route("updatetemplate")]
        public IActionResult updatetemplate(SettingContext settingmodel)
        {
            //string type = "smtp";
            // bool returndata = manager.Updatesetting(settingmodel);
            DashboardManager DashboardManager = new DashboardManager();

            if (DashboardManager.updatetemplate(settingmodel))
            {
                return Ok(new { status = 200, message = "Category Add Successfully" });
            }
            else
            {
                return Ok(new { status = 200, message = "Category Remove Successfully" });
            }
            //if (returndata == null)
            //{
            //    return NotFound();
            //}

            //return Ok(returndata);

        }
    }

}
