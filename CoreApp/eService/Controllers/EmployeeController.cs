﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eService.DataContext.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        EmployeeDataAccessLayer obj = new EmployeeDataAccessLayer();
        [HttpGet]
        [Route("Employee")]
        public IEnumerable<Employee> Index()
        {
            return obj.GetAllEmployee();
        }

        [HttpPost]
        [Route("api/Employee/Create")]
        public IActionResult Create(Employee employee)
        {

            bool isInserted = obj.AddEmployee(employee);

            return Ok(isInserted);
        }

        [HttpPut]
        [Route("api/Employee/Edit")]
        public IActionResult Edit([FromBody]Employee employee)
        {
            obj.UpdateEmployee(employee);
            return RedirectToAction("Index");
        }

        [HttpDelete]
        [Route("api/Employee/Delete/{id}")]
        public IActionResult Delete(int id)
        {
            obj.DeleteEmployee(id);
            return RedirectToAction("Index");
        }
    }
}