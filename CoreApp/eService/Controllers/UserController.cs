﻿using eService.DAL.Manager;
using eService.DataContext.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace eService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ILogger _logger;
        private readonly IHostingEnvironment _hostingEnvironment;
        public UserController(ILogger<UserController> logger, IHostingEnvironment hostingEnvironment)
        {
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
        }
        public readonly UserManager manager = new UserManager();
        [HttpPost]
        [Route("userLogin")]
        public IActionResult AuthenticateUser(user_login login)
        {
            RegistrationDataContext users = new RegistrationDataContext();
            if (!string.IsNullOrWhiteSpace(login.email) && !string.IsNullOrWhiteSpace(login.password))
            {
                users = manager.AuthenticateUser(login.email, login.password);
                IActionResult response = Unauthorized();

                if (users != null && users.userId > 0)
                {
                    return Ok(new { users, status = 200 });
                }
                else
                {
                    return new UnauthorizedResult();
                }
            }
            else
            {
                return new UnauthorizedResult();
            }
        }

        [HttpPost]
        [Route("RegisterUser")]
        public IActionResult RegisterUser(RegistrationDataContext users)
        {
            //RegistrationDataContext registration = new RegistrationDataContext();
            UserManager userManager = new UserManager();
            if (userManager.IsuserExist(users.email))
            {
                return Ok(new { message = "Email Already exist" });
            }

            if (!string.IsNullOrWhiteSpace(users.ToString()))
            { 

                // IActionResult response = Unauthorized();

                if (manager.RegisterUser(users))
                {
                    return Ok(new { users, status = 200 });
                }
                else
                {
                    return new UnauthorizedResult();
                }
            }
            else
            {
                return new UnauthorizedResult();
            }
        }

        [HttpGet]
        [Route("DaysTime")]
        public List<TimeSchedule> TimeSchedules(int dayNumber)
        {
            UserManager userManager = new UserManager();
            List<TimeSchedule> returnvalue = userManager.TimeSchedules(dayNumber);
            return returnvalue;

        }
        [HttpPost]
        [Route("UserDetails")]
        public IActionResult AddUser(UserDetails userDetails)
        {
            try
            {
                UserManager userManager = new UserManager();
                if (userManager.AddUser(userDetails))
                {
                    return Ok(new { status = 200 });

                }
                else
                {
                    return Ok(new { status = 200 });
                }
            }
            catch(Exception ex)
            {
                _logger.LogInformation("Log message in the Index() method");
                
            }
            
            return Ok();

        }
        [HttpPost]
        [Route("Payment")]

        public IActionResult Payment(PaymentDataContext paymentData)
        {
            UserManager userManager = new UserManager();
            if (userManager.Payment(paymentData))
            {
                return Ok(new { status = 200 });

            }
            else
            {
                return Ok(new { status = 200 });
            }
            //return false;

        }

        [HttpGet]
        [Route("CategoryName")]
        public IActionResult Categoryname(string CategoryValues)
        {
            //CategoryDetails categoryDetails = new CategoryDetails();
            List<CategoryDetails> categoryList = manager.CategoryDetail(CategoryValues);
            if (categoryList.Count > 0)
            {
                return Ok(categoryList);
            }
            else
            {
                return BadRequest(new { status = 401 });
            }

        }



        [HttpGet]
        [Route("Profile")]
        public IActionResult Profile(int userId)
        {
            UserProfile List = manager.profile(userId);
            if (userId > 0)
            {
                return Ok(List);
            }
            else
            {
                return BadRequest(new { status = 401 });
            }

        }
        //get user order
        [HttpGet]
        [Route("Userorder")]
        public IActionResult Userorder(int userId)
        {
            List<Userorder> List = manager.userorder(userId);
            if (userId > 0)
            {
                return Ok(List);
            }
            else
            {
                return BadRequest(new { status = 401 });
            }

        }
        //get user cancel order
        [HttpGet]
        [Route("Usercancelorder")]
        public IActionResult Usercancelorder(int userId)
        {
            List<Userorder> List = manager.Usercancelorder(userId);
            if (userId > 0)
            {
                return Ok(List);
            }
            else
            {
                return BadRequest(new { status = 401 });
            }

        }
        
        [HttpGet]
        [Route("downloadorderinpdf")]
        public IActionResult downloadorderinpdf(string orderId)
        {
            Userorder List = manager.Downloadorderinpdf(orderId);
            if (orderId.Length > 0)
            {
                return Ok(List);
            }
            else
            {
                return BadRequest(new { status = 401 });
            }
        }


        [HttpGet]
        [Route("UserOrderDelete")]
        public IActionResult UserOrderDelete(string orderId)
        {
            Userorder List = manager.UserOrderDelete(orderId);
            if (orderId.Length > 0)
            {
                return Ok(new { status = 200 });
            }
            else
            {
                return BadRequest(new { status = 401 });
            }

        }
        
        [HttpGet]
        [Route("download")]
        public IActionResult download(string orderId)
        {
            string filepath = string.Empty;
            // manager -- > file path .. 
            string pdfFilePath = @"C:\inetpub\wwwroot\BookingAPI\PDF";
            filepath = pdfFilePath + @"\Invoice.pdf";


            string List = manager.Download(orderId, filepath);
            if (orderId.Length > 0)
            {
                var stream = System.IO.File.OpenRead(filepath);
            
                byte[] fileBytes = System.IO.File.ReadAllBytes(filepath);
                return new FileStreamResult(stream, "application/octet-stream");
            }
            else
            {
                return BadRequest(new { status = 401 });
            }

        }

        //edit
        [HttpPost]
        [Route("EditProfile")]

        public IActionResult EditProfile(UserProfile Profile)
        {
            UserManager userManager = new UserManager();
            if (userManager.editProfile(Profile))
            {
                return Ok(new { status = 200 });
            }
            else
            {
                return Ok(new { status = 200, message = " fail" });
            }
        }

        [HttpPost]
        [Route("PaymentReceipt")]

        public IActionResult Receipt(PaymentReceipt paymentData)
        {
            UserManager userManager = new UserManager();
            if (userManager.Payment(paymentData))
            {
                return Ok(new { status = 200 });

            }
            else
            {
                return Ok(new { status = 200 });
            }
            //return false;

        }

        
        [HttpPost]
        [Route("ChangePassword")]

        public IActionResult ChngPassword(ChangePassword Password)
        {
            UserManager userManager = new UserManager();
            if (userManager.ChngPassword(Password))
            {
                return Ok(new { status = 200 });
            }
            else
            {
                return Ok(new { status = 200, message = " fail" });
            }
        }

        //user Exist

        [HttpGet]
        [Route("UserExist")]
        public IActionResult IsuserExist(string email)
        {
            //CategoryDetails categoryDetails = new CategoryDetails();
            UserManager userManager = new UserManager();
            if (userManager.IsuserExist(email))
            {
                return Ok(new { message = "Email Already exist" });
            }
            return NotFound();

        }

        //forgot Password
        [HttpPost]
        [Route("ForgotPassword")]
        public IActionResult ForgotPassword(ForgotPassword forgot)
        {
            UserManager userManager = new UserManager();
            if (userManager.ForgotPassword(forgot))
            {
                return Ok(new { Message = "Link Send Successfully " });
            }
            else
            {
                return BadRequest(new { Message = "User Not Exist" });
            }

        }
        ////create Password
        [HttpPost]
        [Route("Create-Password")]
        public IActionResult CreatePassword(Create_Password create)
        {
            UserManager userManager = new UserManager();
            if (userManager.CreatePassword(create))
            {
                return Ok(new { status = 200 });
            }
            else
            {
                return Ok(new { status = 200, message = " fail" });
            }
        }

    }
}
