﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eService.DataContext.Models;



using System.Data.SqlClient;

namespace eService.Common.Helper
{
   public static class CategoriesTranslator
    {
        public static CategoriesDataContext TranslateAsCategories(this SqlDataReader reader, bool isList = false)

        {

            if (!isList)

            {

                if (!reader.HasRows)

                    return null;

                reader.Read();

            }

            var item = new CategoriesDataContext();

            if (reader.IsColumnExists("categoryId"))

                item.categoryId = SqlHelper.GetNullableInt32(reader, "categoryId");

            if (reader.IsColumnExists("subcategoryName"))

                item.subcategoryName = SqlHelper.GetNullableString(reader, "subcategoryName");

            if (reader.IsColumnExists("status"))

                item.status = SqlHelper.GetNullableString(reader, "status");

            if (reader.IsColumnExists("masterCategoryId"))

                item.masterCategoryId = SqlHelper.GetNullableInt32(reader, "masterCategoryId");

            return item;

        }



        public static List<CategoriesDataContext> TranslateAsCategoriesList(this SqlDataReader reader)

        {

            var list = new List<CategoriesDataContext>();

            while (reader.Read())

            {

                list.Add(TranslateAsCategories(reader, true));

            }

            return list;

        }


    }
}
