﻿using eService.DataContext.Models;

using System.Collections.Generic;

using System.Data.SqlClient;


namespace eService.Common.Helper
{
    public static class UserTranslator

    {

        public static RegistrationDataContext TranslateAsUser(this SqlDataReader reader, bool isList = false)

        {

            if (!isList)

            {

                if (!reader.HasRows)

                    return null;

                reader.Read();

            }

            var item = new RegistrationDataContext();

            if (reader.IsColumnExists("email"))

                item.email = SqlHelper.GetNullableString(reader, "email");



            if (reader.IsColumnExists("password"))

                item.password = SqlHelper.GetNullableString(reader, "password");

            if (reader.IsColumnExists("userName"))

                item.userName = SqlHelper.GetNullableString(reader, "userName");

            return item;

        }



        public static List<RegistrationDataContext> TranslateAsUsersList(this SqlDataReader reader)

        {

            var list = new List<RegistrationDataContext>();

            while (reader.Read())

            {
   
                list.Add(TranslateAsUser(reader, true));

            }

            return list;

        }

        
        
        
        
    }
}
