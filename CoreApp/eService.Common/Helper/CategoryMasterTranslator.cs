﻿
using System.Collections.Generic;
using System.Data.SqlClient;

using eService.DataContext.Models;

namespace eService.Common.Helper
{
   public static class CategoryMasterTranslator
    {

        public static CategoryMasterDataContext TranslateAsCategoryMaster(this SqlDataReader reader, bool isList = false)
        {

            if (!isList)

            {

                if (!reader.HasRows)

                    return null;

                reader.Read();

            }
            var item = new CategoryMasterDataContext();

            if (reader.IsColumnExists("masterCategoryId"))

                item.masterCategoryId = SqlHelper.GetNullableInt32(reader, "masterCategoryId");




            if (reader.IsColumnExists("categoryName"))

                item.categoryName = SqlHelper.GetNullableString(reader, "categoryName");

            if (reader.IsColumnExists("status"))

                item.status =  SqlHelper.GetBoolean(reader, "status");

            return item;

        }



        public static List<CategoryMasterDataContext> TranslateAsCategoryMasterList(this SqlDataReader reader)

        {

            var list = new List<CategoryMasterDataContext>();

            while (reader.Read())

            {

                list.Add(TranslateAsCategoryMaster(reader, true));

            }

            return list;

        }


    }
}