﻿using eService.DataContext.Models;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace eService.Common.Helper
{
   public static class SubCategoryTranslator
    {
        public static SubCategoryDataContext TranslateAsSubCategory(this SqlDataReader reader, bool isList = false)

        {

            if (!isList)

            {

                if (!reader.HasRows)

                    return null;

                reader.Read();

            }

            var item = new SubCategoryDataContext();

            if (reader.IsColumnExists("SubCategoriesId"))

                item.SubCategoriesId = SqlHelper.GetNullableInt32(reader, "SubCategoriesId");

            if (reader.IsColumnExists("subcategoryName"))

                item.subcategoryName = SqlHelper.GetNullableString(reader, "subcategoryName");

            if (reader.IsColumnExists("price"))

                item.price = SqlHelper.GetNullableInt32(reader, "price");


            if (reader.IsColumnExists("categoryId"))

                item.categoryId = SqlHelper.GetNullableInt32(reader, "categoryId");

           

            return item;

        }



        public static List<SubCategoryDataContext> TranslateAsSubCategoriesList(this SqlDataReader reader)

        {

            var list = new List<SubCategoryDataContext>();

            while (reader.Read())

            {

                list.Add(TranslateAsSubCategory(reader, true));

            }

            return list;

        }
    }
}
