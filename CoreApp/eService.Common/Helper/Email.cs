﻿using eService.DataContext.Models;
using System;
using System.Net.Mail;

namespace eService.Common.Helper
{
    public class Email
    {
        public static void SendMail(string body, string subject, string toAddress)
        {


            EmailSettings emailSettings = new EmailSettings();
            try
            {
                //Base class for sending email  
                MailMessage mail = new MailMessage();


                SmtpClient SmtpServer = new SmtpClient("Smtp.gmail.com");

                mail.From = new MailAddress("icds123.manektech@gmail.com");
                mail.To.Add(toAddress);
                mail.IsBodyHtml = true;
                mail.Subject = "Booking Website - Registration";
                mail.Body = body;
                SmtpServer.Port = Convert.ToInt16(587);
                SmtpServer.Credentials = new System.Net.NetworkCredential("icds123.manektech", "icds@123");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch (Exception)
            {

            }
        }
        public static void Receipt(string body, string subject, string toAddress)
        {


            EmailSettings emailSettings = new EmailSettings();
            try
            {
                //Base class for sending email  
                MailMessage mail = new MailMessage();


                SmtpClient SmtpServer = new SmtpClient("Smtp.gmail.com");

                mail.From = new MailAddress("icds123.manektech@gmail.com");
                mail.To.Add(toAddress);
                mail.IsBodyHtml = true;
                mail.Subject = "Appointment Receipt";
                mail.Body = body;
                SmtpServer.Port = Convert.ToInt16(587);
                SmtpServer.Credentials = new System.Net.NetworkCredential("icds123.manektech", "icds@123");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);

            }
            catch (Exception)
            {
                // Logger.ErrorLog(ex.ToString());
            }
        }
        public static void ForgotPassword(string body, string subject, string toAddress)

        {


            EmailSettings emailSettings = new EmailSettings();
            try
            {
                //Base class for sending email  
                MailMessage mail = new MailMessage();


                SmtpClient SmtpServer = new SmtpClient("Smtp.gmail.com");

                mail.From = new MailAddress("icds123.manektech@gmail.com");
                mail.To.Add(toAddress);
                mail.IsBodyHtml = true;
                mail.Subject = "ForgotPassword";
                mail.Body = body;
                SmtpServer.Port = Convert.ToInt16(587);
                SmtpServer.Credentials = new System.Net.NetworkCredential("icds123.manektech", "icds@123");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);

            }
            catch (Exception)
            {
                // Logger.ErrorLog(ex.ToString());
            }
        }


    }
}
