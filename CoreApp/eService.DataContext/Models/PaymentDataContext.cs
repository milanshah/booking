﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
   public class PaymentDataContext
    {
        public int PaymentId { get; set; }

        public int userId { get; set; }

        public string Credit_Card_No { get; set; }

        //public DateTime Expire_Date { get; set; }

        public int CVV { get; set; }

        public string CardHolder_Name { get; set; }

        public string Email { get; set; }

        public int ContactNo { get; set; }
        

    }
}
