﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
   public class PaymentReceipt
    {
        public int userId { get; set; }
        public int ReceiptId { get; set; }
        public string category { get; set; }
        public string subCategory { get; set; }
        public string Typeserivce { get; set; }
        public DateTime date { get; set; }
        public string keyInfo { get; set; }
        public string Apartment { get; set; }
        public string time { get; set; }
        public string Address { get; set; }
        public string price { get; set; }
        public string GrandTotal { get; set; }
        public string orderID { get; set; }
        public string TransactionID  { get; set; }
        public string Status { get; set; }

    }
}
