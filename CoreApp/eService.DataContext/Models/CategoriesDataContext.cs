﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
   public class CategoriesDataContext
    {
        public int categoryId { get; set; }
        public string subcategoryName { get; set; }
        public string status { get; set; }
        public int masterCategoryId { get; set; }
    }
}
