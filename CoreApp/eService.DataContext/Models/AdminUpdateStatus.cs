﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
   public class AdminUpdateStatus
    {
        public int masterCategoryId { get; set; }
        public int SubCategoriesId { get; set; }
        public int categoryId { get; set; }
        public bool Status { get; set; }
        public string Type { get; set; }
       

    }
}
