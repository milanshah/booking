﻿using System;

namespace eService.DataContext.Models
{
    public class TimeSchedule
    {
        public int id { get; set; }

        public string dayName { get; set; }

        public int dayNumber { get; set; }

        public String time { get; set; }

        public double interval { get; set; }

    }
}



