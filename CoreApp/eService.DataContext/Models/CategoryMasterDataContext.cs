﻿namespace eService.DataContext.Models
{
    public class CategoryMasterDataContext
    {

        public int categoryId { get; set; }
        public string subcategoryName { get; set; }
        public int masterCategoryId { get; set; }
        public string categoryName { get; set; }
        public bool status { get; set; }
        public string Action { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
       

    }
}
