﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
    public class RegistrationDataContext
    {
        public long loginId { get; set; }
        public long userId { get; set; }
        public string name { get; set; }
        public string contactNumber { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string image { get; set; }
        public string occupation { get; set; }
        public int roleId { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public Boolean IsActive { get; set; }

    }
}
