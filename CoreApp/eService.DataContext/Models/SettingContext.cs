﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
  public  class SettingContext
    {
        public int SettingId { get; set; }
        public string parameter { get; set; }
        public string parameterValue { get; set; }
        public string type { get; set; }
        public string Action { get; set; }
        public string templatesubject { get; set; }
    }
}
