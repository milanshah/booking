﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
   public class ChangePassword
    {
        public string NewPassword { get; set; }

        public int userId { get; set; }
    }
}
