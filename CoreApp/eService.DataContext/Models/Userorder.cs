﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
    public class Userorder
    {
        public int userId { get; set; }
        public string category { get; set; }
        public string subcategoryName { get; set; }
        public string Typeserivce { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string Address { get; set; }
        public string price { get; set; }
        public string GrandTotal { get; set; }
        public string OrderId { get; set; }
        

    }
}
