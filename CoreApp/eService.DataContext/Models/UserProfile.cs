﻿namespace eService.DataContext.Models
{
    public class UserProfile
    {

        public int userId { get; set; }
        public string contactNumber { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string userName { get; set; }

    }
}
