﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
  public  class UserDetails
    {

        public int CustomerId { get; set; }

        public string Address  { get; set; }

        public int ZipCode { get; set; }
        
        public string Apartment { get; set; }

        public int userId { get; set; }

        public string serviceTime { get; set;}

        public string keyInfo { get; set; }

        public string about_Key { get; set; }

        public string special_Notes { get; set; }



    }
}
