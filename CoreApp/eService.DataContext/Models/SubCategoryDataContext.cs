﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
   public class SubCategoryDataContext
    {
        public int SubCategoriesId { get; set; }
        public string subcategoryName { get; set; }
        public Boolean status { get; set; }
        public string Category { get; set; }
        public int price { get; set; }
        public string image { get; set; }
        public int categoryId { get; set; }
    }
}
