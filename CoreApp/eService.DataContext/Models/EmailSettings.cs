﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
   public class EmailSettings
    {
        public string ServerName { get; set; }

        public string Port { get; set; }

        public string UserName { get; set; }

        public string From { get; set; }

        public string Password { get; set; }

        public bool EnableSsl { get; set; }

        public string EmailTemplate { get; set; }

        public bool IsBodyHtml { get; set; }

    }
}
