﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eService.DataContext.Models
{
   public class CategoryDetails
    {
        public int SubCategoriesId { get; set; }

        public int categoryId { get; set; }

        public string subCategoryName { get; set; }

        public string image { get; set; }

        public int price { get; set; }

    }
}
