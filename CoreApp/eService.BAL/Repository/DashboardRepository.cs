﻿using eService.Common.Helper;
using eService.DataContext.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace eService.BAL.Repository
{
    public class DashboardRepository
    {
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["myclean"].ToString();
        // private readonly bool _disposed;

        public List<CategoryMasterDataContext> getCategoryMaster()
        {

            return SqlHelper.ExtecuteProcedureReturnData<List<CategoryMasterDataContext>>(
               "getMasterData", r => r.TranslateAsCategoryMasterList(), null);
        }

        public List<CategoriesDataContext> CategoriesData(int id)
        {

            SqlParameter[] param = {
                new SqlParameter ("@id", id)
            };

            return SqlHelper.ExtecuteProcedureReturnData<List<CategoriesDataContext>>(
               "getSubCategory", r => r.TranslateAsCategoriesList(), param);
        }

        public List<SubCategoryDataContext> SubCategoryData(int id)
        {
            List<SubCategoryDataContext> subCategoryDataContextsList = new List<SubCategoryDataContext>();
            DataTable dtSubCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@id", id);
            SqlHelper.Fill(dtSubCategory, "getAllImages", objParameter);

            if (dtSubCategory != null && dtSubCategory.Rows.Count > 0)
            {
                for (int index = 0; index < dtSubCategory.Rows.Count; index++)
                {
                    SubCategoryDataContext subCategoryDataContext = new SubCategoryDataContext
                    {
                        categoryId = Convert.ToInt32(dtSubCategory.Rows[index]["categoryId"]),
                        image = Convert.ToString(dtSubCategory.Rows[index]["image"]),
                        price = Convert.ToInt32(dtSubCategory.Rows[index]["price"]),
                        status = Convert.ToBoolean(dtSubCategory.Rows[index]["status"]),
                        //status = Convert.ToString(dtSubCategory.Rows[index]["status"]),
                        SubCategoriesId = Convert.ToInt32(dtSubCategory.Rows[index]["SubCategoriesId"]),
                        subcategoryName = Convert.ToString(dtSubCategory.Rows[index]["subcategoryName"])
                    };

                    subCategoryDataContextsList.Add(subCategoryDataContext);


                }


            }
            return subCategoryDataContextsList;
        }

        //for Display Masater Category Data
        public List<CategoryMasterDataContext> CategoryDetail(string Action)
        {
            List<CategoryMasterDataContext> categoryList = new List<CategoryMasterDataContext>();
            //CategoryDetails objCategory = new CategoryDetails();
            DataTable dtCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@Action", "SELECT");
            SqlHelper.Fill(dtCategory, "[usp_AdminControl]", objParameter);
            if (dtCategory != null && dtCategory.Rows.Count > 0)
            {
                foreach (DataRow row in dtCategory.Rows)
                {
                    CategoryMasterDataContext objCategory = new CategoryMasterDataContext();
                    {
                        objCategory.masterCategoryId = Convert.ToInt32(row["masterCategoryId"]);
                        objCategory.categoryName = Convert.ToString(row["categoryName"]);
                        objCategory.status = Convert.ToBoolean(row["status"]);

                    };

                    categoryList.Add(objCategory);
                }

            }
            return categoryList;
        }

        //for pagination
        public List<CategoryMasterDataContext> Pagination(int pageSize, int pageIndex)
        {
            List<CategoryMasterDataContext> categoryList = new List<CategoryMasterDataContext>();
            //CategoryDetails objCategory = new CategoryDetails();
            DataTable dtCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[3];
            objParameter[0] = new SqlParameter("@Action", "Master");
            objParameter[1] = new SqlParameter("@pageIndex", pageIndex);
            objParameter[2] = new SqlParameter("@pageSize", pageSize);
            SqlHelper.Fill(dtCategory, "[usp_Pagination]", objParameter);
            if (dtCategory != null && dtCategory.Rows.Count > 0)
            {
                foreach (DataRow row in dtCategory.Rows)
                {
                    CategoryMasterDataContext objCategory = new CategoryMasterDataContext();
                    {
                        objCategory.masterCategoryId = Convert.ToInt32(row["masterCategoryId"]);
                        objCategory.categoryName = Convert.ToString(row["categoryName"]);
                        objCategory.status = Convert.ToBoolean(row["status"]);

                    };

                    categoryList.Add(objCategory);
                }

            }
            return categoryList;
        }

        //Master Category edit
        public bool MasterCategoryEdit(CategoryMasterDataContext Details)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[4];
            objParameter[0] = new SqlParameter("@masterCategoryId", Details.masterCategoryId);
            objParameter[1] = new SqlParameter("@categoryName", Details.categoryName);
            // objParameter[2] = new SqlParameter("@status", Details.status);
            if (!string.IsNullOrEmpty(Details.status.ToString()))
            { objParameter[2] = new SqlParameter("@status", Details.status); }
            else { objParameter[2] = new SqlParameter("@status", DBNull.Value); }
            objParameter[3] = new SqlParameter("@Action", "UPDATE");

            int count = SqlHelper.ExecuteNonQuery("usp_AdminControl", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        ////Get Master Category
        //public CategoryMasterDataContext MasterCategoryEdit(int masterCategoryId)
        //{
        //    //  List<UserProfile> List = new List<UserProfile>();
        //    CategoryMasterDataContext objUser = new CategoryMasterDataContext();
        //    DataTable dtDetail = new DataTable();
        //    SqlParameter[] objParameter = new SqlParameter[2];
        //    objParameter[0] = new SqlParameter("@masterCategoryId", masterCategoryId);
        //    objParameter[1] = new SqlParameter("@Action", "GET");
        //    SqlHelper.Fill(dtDetail, "usp_AdminControl", objParameter);
        //    if (dtDetail != null && dtDetail.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in dtDetail.Rows)
        //        {

        //            objUser.masterCategoryId = Convert.ToInt32(row["masterCategoryId"]);
        //            objUser.categoryName = Convert.ToString(row["categoryName"]);
        //            objUser.status = Convert.ToBoolean(row["status"]);
        //        }
        //    }
        //    return objUser;
        //}

        //Master Category Add 

        public bool AddMasterCategory(CategoryMasterDataContext Details)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[4];
            objParameter[0] = new SqlParameter("@masterCategoryId", Details.masterCategoryId);
            objParameter[1] = new SqlParameter("@categoryName", Details.categoryName);
            // objParameter[2] = new SqlParameter("@status", Details.status);
            if (!string.IsNullOrEmpty(Details.status.ToString()))
            { objParameter[2] = new SqlParameter("@status", Details.status); }
            else { objParameter[2] = new SqlParameter("@status", DBNull.Value); }
            objParameter[3] = new SqlParameter("@Action", "INSERT");

            int count = SqlHelper.ExecuteNonQuery("usp_AdminControl", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Master Category AdminControl
        public bool AdminControl(AdminUpdateStatus adminUpdateStatus)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[4];
            objParameter[0] = new SqlParameter("@Action", "CheckBox1");
            objParameter[1] = new SqlParameter("@masterCategoryId", adminUpdateStatus.masterCategoryId);
            objParameter[2] = new SqlParameter("@Status", adminUpdateStatus.Status);
            objParameter[3] = new SqlParameter("@Type", "CategoryMaster");
            int count = SqlHelper.ExecuteNonQuery("usp_Checkbox", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Master Category Delete 
        public string MasterCategoryDelete(int masterCategoryId)
        {

            DataTable dtSubCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@Action", "SELECT_M_ID");
            objParameter[1] = new SqlParameter("@masterCategoryId", masterCategoryId);
            SqlHelper.Fill(dtSubCategory, "usp_GetCategoryId", objParameter);
            if (dtSubCategory != null && dtSubCategory.Rows.Count > 0)
            {
                return "Category Already Use";
            }
            else
            {

                SqlParameter[] obj = new SqlParameter[2];
                obj[0] = new SqlParameter("@masterCategoryId", masterCategoryId);
                obj[1] = new SqlParameter("@Action", "SOFT_DELETE");
                int count = SqlHelper.ExecuteNonQuery("usp_AdminControl", obj);
                return "OK";
            }


        }
        // Master Category Already Exist
        public bool MasterCategoryExist(string categoryName, int id)
        {
            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@categoryName", categoryName);
            objParameter[1] = new SqlParameter("@masterCategoryId", id);
            SqlHelper.Fill(dtDetail, "usp_CategoryExist", objParameter);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //For Category Add Update Delete//

        //For Display Category List 
        public List<CategoryMasterDataContext> CategoryList()
        {
            List<CategoryMasterDataContext> categoryList = new List<CategoryMasterDataContext>();
            DataTable dtCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@Action", "Select");
            SqlHelper.Fill(dtCategory, "[usp_Category_Admin]", objParameter);
            if (dtCategory != null && dtCategory.Rows.Count > 0)
            {
                foreach (DataRow row in dtCategory.Rows)
                {
                    CategoryMasterDataContext objCategory = new CategoryMasterDataContext();
                    {
                        objCategory.categoryId = Convert.ToInt32(row["categoryId"]);
                        objCategory.masterCategoryId = Convert.ToInt32(row["masterCategoryId"]);
                        objCategory.categoryName = Convert.ToString(row["categoryName"]);
                        objCategory.subcategoryName = Convert.ToString(row["subcategoryName"]);
                        objCategory.status = Convert.ToBoolean(row["status"]);

                    };

                    categoryList.Add(objCategory);
                }

            }
            return categoryList;
        }

        //Category Edit
        public bool CategoryEdit(CategoryMasterDataContext Details)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[5];
            objParameter[0] = new SqlParameter("@masterCategoryId", Details.masterCategoryId);
            objParameter[1] = new SqlParameter("@categoryId", Details.categoryId);
            objParameter[2] = new SqlParameter("@subcategoryName", Details.subcategoryName);
            if (!string.IsNullOrEmpty(Details.status.ToString()))
            { objParameter[3] = new SqlParameter("@status", Details.status); }
            else { objParameter[3] = new SqlParameter("@status", DBNull.Value); }
            objParameter[4] = new SqlParameter("@Action", "UPDATE");

            int count = SqlHelper.ExecuteNonQuery("usp_Category_Admin", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Category Add 

        public bool CategoryAdd(CategoryMasterDataContext Details)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[5];
            // objParameter[0] = new SqlParameter("@masterCategoryId", Details.masterCategoryId);
            if (!string.IsNullOrEmpty(Details.masterCategoryId.ToString()))
            { objParameter[0] = new SqlParameter("@masterCategoryId", Details.masterCategoryId); }
            else { objParameter[0] = new SqlParameter("@masterCategoryId", DBNull.Value); }
            objParameter[1] = new SqlParameter("@categoryId", Details.categoryId);
            objParameter[2] = new SqlParameter("@subcategoryName", Details.subcategoryName);
            if (!string.IsNullOrEmpty(Details.status.ToString()))
            { objParameter[3] = new SqlParameter("@status", Details.status); }
            else { objParameter[3] = new SqlParameter("@status", DBNull.Value); }
            objParameter[4] = new SqlParameter("@Action", "INSERT");

            int count = SqlHelper.ExecuteNonQuery("usp_Category_Admin", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Category Delete 
        public string CategoryDelete(int categoryId)
        {

            DataTable dtSubCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@Action", "SELECT_C_ID");
            objParameter[1] = new SqlParameter("@categoryId", categoryId);
            SqlHelper.Fill(dtSubCategory, "usp_GetCategoryId", objParameter);
            if (dtSubCategory != null && dtSubCategory.Rows.Count > 0)
            {
                return "Category Already Use";
            }
            else
            {
                //int count = SqlHelper.ExecuteNonQuery("usp_GetCategoryId", objParameter);
                SqlParameter[] obj = new SqlParameter[2];
                obj[0] = new SqlParameter("@categoryId", categoryId);
                obj[1] = new SqlParameter("@Action", "DELETE");
                int count = SqlHelper.ExecuteNonQuery("usp_Category_Admin", obj);
                return "OK";
            }
        }

        // Category Already Exist
        public bool CategoryExist(string subcategoryName, int id)
        {
            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@subcategoryName", subcategoryName);
            objParameter[1] = new SqlParameter("@categoryId", id);
            SqlHelper.Fill(dtDetail, "[usp_CategoryExistEdit]", objParameter);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Category  Checkbox
        public bool CategoryCheckbox(AdminUpdateStatus adminUpdateStatus)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[4];
            objParameter[0] = new SqlParameter("@Action", "CheckBox2");
            objParameter[1] = new SqlParameter("@Type", "Categories");
            objParameter[2] = new SqlParameter("@categoryId", adminUpdateStatus.categoryId);
            objParameter[3] = new SqlParameter("@Status", adminUpdateStatus.Status);
            int count = SqlHelper.ExecuteNonQuery("usp_Checkbox", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //for pagination
        public List<CategoryMasterDataContext> CategoryPagination(int pageSize, int pageIndex)
        {
            List<CategoryMasterDataContext> categoryList = new List<CategoryMasterDataContext>();
            //CategoryDetails objCategory = new CategoryDetails();
            DataTable dtCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[3];
            objParameter[0] = new SqlParameter("@Action", "Category");
            objParameter[1] = new SqlParameter("@pageIndex", pageIndex);
            objParameter[2] = new SqlParameter("@pageSize", pageSize);
            SqlHelper.Fill(dtCategory, "[usp_Pagination]", objParameter);
            if (dtCategory != null && dtCategory.Rows.Count > 0)
            {
                foreach (DataRow row in dtCategory.Rows)
                {
                    CategoryMasterDataContext objCategory = new CategoryMasterDataContext();
                    {
                        objCategory.categoryId = Convert.ToInt32(row["categoryId"]);
                        objCategory.masterCategoryId = Convert.ToInt32(row["masterCategoryId"]);
                        objCategory.categoryName = Convert.ToString(row["categoryName"]);
                        objCategory.subcategoryName = Convert.ToString(row["subcategoryName"]);
                        objCategory.status = Convert.ToBoolean(row["status"]);

                    };

                    categoryList.Add(objCategory);
                }

            }
            return categoryList;
        }

        //For Selected Checkbox List//
        public List<CategoryMasterDataContext> CheckboxList()
        {
            List<CategoryMasterDataContext> categoryList = new List<CategoryMasterDataContext>();
            DataTable dtCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@Action", "Select");
            SqlHelper.Fill(dtCategory, "[usp_Checkbox]", objParameter);
            if (dtCategory != null && dtCategory.Rows.Count > 0)
            {
                foreach (DataRow row in dtCategory.Rows)
                {
                    CategoryMasterDataContext objCategory = new CategoryMasterDataContext();
                    {
                        objCategory.masterCategoryId = Convert.ToInt32(row["masterCategoryId"]);
                        objCategory.categoryName = Convert.ToString(row["categoryName"]);
                    };

                    categoryList.Add(objCategory);
                }

            }
            return categoryList;
        }

        //for SubCategory Add Update Delete
        //for Display SubCategory Data
        public List<SubCategoryDataContext> SubCategoryList()
        {
            List<SubCategoryDataContext> SubCategoryList = new List<SubCategoryDataContext>();
            DataTable dtCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@Action", "SELECT");
            SqlHelper.Fill(dtCategory, "[usp_SubCategories]", objParameter);
            if (dtCategory != null && dtCategory.Rows.Count > 0)
            {
                foreach (DataRow row in dtCategory.Rows)
                {
                    SubCategoryDataContext objCategory = new SubCategoryDataContext();
                    {
                        objCategory.categoryId = Convert.ToInt32(row["categoryId"]);
                        objCategory.SubCategoriesId = Convert.ToInt32(row["SubCategoriesId"]);
                        objCategory.status = Convert.ToBoolean(row["status"]);
                        objCategory.price = Convert.ToInt32(row["price"]);
                        objCategory.image = Convert.ToString(row["image"]);
                        objCategory.subcategoryName = Convert.ToString(row["subcategoryName"]);
                        objCategory.Category = Convert.ToString(row["Category"]);

                    };

                    SubCategoryList.Add(objCategory);
                }

            }
            return SubCategoryList;
        }

        //SubCategory Edit
        public bool SubCategoryEdit(SubCategoryDataContext Details)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[7];
            objParameter[0] = new SqlParameter("@SubCategoriesId", Details.SubCategoriesId);
            objParameter[1] = new SqlParameter("@categoryId", Details.categoryId);
            objParameter[2] = new SqlParameter("@subcategoryName", Details.subcategoryName);
            if (!string.IsNullOrEmpty(Details.status.ToString()))
            { objParameter[3] = new SqlParameter("@status", Details.status); }
            else { objParameter[3] = new SqlParameter("@status", DBNull.Value); }
            objParameter[4] = new SqlParameter("@price", Details.price);
            if (!string.IsNullOrEmpty(Details.image.ToString()))
            { objParameter[5] = new SqlParameter("@image", Details.image); }
            else { objParameter[5] = new SqlParameter("@image", DBNull.Value); }
            objParameter[6] = new SqlParameter("@Action", "UPDATE");

            int count = SqlHelper.ExecuteNonQuery("usp_SubCategories", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //For Selected Checkbox List//
        public List<SubCategoryDataContext> SubCheckboxList()
        {
            List<SubCategoryDataContext> categoryList = new List<SubCategoryDataContext>();
            DataTable dtCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@Action", "SubCatSelect");
            SqlHelper.Fill(dtCategory, "[usp_Checkbox]", objParameter);
            if (dtCategory != null && dtCategory.Rows.Count > 0)
            {
                foreach (DataRow row in dtCategory.Rows)
                {
                    SubCategoryDataContext objCategory = new SubCategoryDataContext();
                    {
                        objCategory.categoryId = Convert.ToInt32(row["categoryId"]);
                        objCategory.subcategoryName = Convert.ToString(row["subcategoryName"]);
                    };

                    categoryList.Add(objCategory);
                }

            }
            return categoryList;
        }

        // SubCategory Delete 
        public string SubCategoryDelete(int SubCategoriesId)
        {

            DataTable dtSubCategory = new DataTable();
            SqlParameter[] obj = new SqlParameter[2];
            obj[0] = new SqlParameter("@SubCategoriesId", SubCategoriesId);
            obj[1] = new SqlParameter("@Action", "DELETE");
            int count = SqlHelper.ExecuteNonQuery("usp_SubCategories", obj);
            if (count > 0)

            {
                return "Subcategory Delete Successfully";
            }
            else
            {

                return "Fail";
            }
        }

        //Sub Category Add 
        public bool SubCategoryAdd(SubCategoryDataContext Details)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[7];
            objParameter[0] = new SqlParameter("@SubCategoriesId", Details.SubCategoriesId);
            objParameter[1] = new SqlParameter("@categoryId", Details.categoryId);
            objParameter[2] = new SqlParameter("@subcategoryName", Details.subcategoryName);
            objParameter[3] = new SqlParameter("@status", Details.status);
            if (!string.IsNullOrEmpty(Details.price.ToString()))
            { objParameter[4] = new SqlParameter("@price", Details.price); }
            else { objParameter[4] = new SqlParameter("@price", DBNull.Value); }
            if (!string.IsNullOrEmpty(Details.image.ToString()))
            { objParameter[5] = new SqlParameter("@image", Details.image); }
            else { objParameter[5] = new SqlParameter("@image", DBNull.Value); }
            objParameter[6] = new SqlParameter("@Action", "INSERT");

            int count = SqlHelper.ExecuteNonQuery("[usp_SubCategories]", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // SubCategory Already Exist
        public bool SubCategoryExist(string subcategoryName, int id)
        {
            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@subcategoryName", subcategoryName);
            objParameter[1] = new SqlParameter("@SubCategoriesId", id);
            SqlHelper.Fill(dtDetail, "[usp_SubCategoryExist]", objParameter);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // SubCategory  Checkbox
        public bool SubCategoryCheckbox(AdminUpdateStatus adminUpdateStatus)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[4];
            objParameter[0] = new SqlParameter("@Action", "CheckBox3");
            objParameter[1] = new SqlParameter("@Type", "SubCategories");
            objParameter[2] = new SqlParameter("@SubCategoriesId", adminUpdateStatus.SubCategoriesId);
            objParameter[3] = new SqlParameter("@Status", adminUpdateStatus.Status);
            int count = SqlHelper.ExecuteNonQuery("usp_Checkbox", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //for Sub Category pagination
        public List<SubCategoryDataContext> SubCategoryPagination(int pageSize, int pageIndex)
        {
            List<SubCategoryDataContext> categoryList = new List<SubCategoryDataContext>();
            //CategoryDetails objCategory = new CategoryDetails();
            DataTable dtCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[3];
            objParameter[0] = new SqlParameter("@Action", "SubCategories");
            objParameter[1] = new SqlParameter("@pageIndex", pageIndex);
            objParameter[2] = new SqlParameter("@pageSize", pageSize);
            SqlHelper.Fill(dtCategory, "[usp_Pagination]", objParameter);
            if (dtCategory != null && dtCategory.Rows.Count > 0)
            {
                foreach (DataRow row in dtCategory.Rows)
                {
                    SubCategoryDataContext objCategory = new SubCategoryDataContext();
                    {

                        objCategory.categoryId = Convert.ToInt32(row["categoryId"]);
                        objCategory.SubCategoriesId = Convert.ToInt32(row["SubCategoriesId"]);
                        objCategory.status = Convert.ToBoolean(row["status"]);
                        objCategory.price = Convert.ToInt32(row["price"]);
                        objCategory.image = Convert.ToString(row["image"]);
                        objCategory.subcategoryName = Convert.ToString(row["subcategoryName"]);
                        objCategory.Category = Convert.ToString(row["Category"]);

                    };

                    categoryList.Add(objCategory);
                }

            }
            return categoryList;
        }
        public List<UserListContext> getuserlist()
        {
            List<UserListContext> userList = new List<UserListContext>();
            //CategoryDetails objCategory = new CategoryDetails();
            DataTable dtuser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];

            objParameter[0] = new SqlParameter("@Action", "SELECT");
            SqlHelper.Fill(dtuser, "usp_UserList", objParameter);
            //if (dtuser != null && dtuser.Rows.Count > 0)
            //{
            foreach (DataRow row in dtuser.Rows)
            {
                UserListContext objuser = new UserListContext();
                {

                    objuser.userId = Convert.ToInt32(row["userId"]);
                    objuser.Name = Convert.ToString(row["Name"]);
                    objuser.userName = Convert.ToString(row["userName"]);
                    objuser.password = Convert.ToString(row["password"]);
                    objuser.contactNumber = Convert.ToString(row["contactNumber"]);
                    objuser.email = Convert.ToString(row["email"]);
                    objuser.address = Convert.ToString(row["address"]);
                    objuser.occupation = Convert.ToString(row["occupation"]);
                    objuser.image = Convert.ToString(row["image"]);
                    objuser.roleId = Convert.ToInt32(row["roleId"]);
                    objuser.RoleName = Convert.ToString(row["RoleName"]);
                    objuser.token = Convert.ToString(row["token"]);
                    objuser.IsActive = Convert.ToBoolean(row["IsActive"]);


                };

                userList.Add(objuser);
            }

            //}
            return userList;
        }
        public List<UserListContext> getsingleuserlist(int userId)
        {
            List<UserListContext> userList = new List<UserListContext>();
            //CategoryDetails objCategory = new CategoryDetails();
            DataTable dtuser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];

            objParameter[0] = new SqlParameter("@Action", "SELECTSINGLE");
            objParameter[1] = new SqlParameter("userId", userId);
            SqlHelper.Fill(dtuser, "usp_UserList", objParameter);
            if (dtuser != null && dtuser.Rows.Count > 0)
            {
                foreach (DataRow row in dtuser.Rows)
                {
                    UserListContext objuser = new UserListContext();
                    {

                        objuser.userId = Convert.ToInt32(row["userId"]);
                        objuser.Name = Convert.ToString(row["Name"]);
                        objuser.userName = Convert.ToString(row["userName"]);
                        objuser.password = Convert.ToString(row["password"]);
                        objuser.contactNumber = Convert.ToString(row["contactNumber"]);
                        objuser.email = Convert.ToString(row["email"]);
                        objuser.address = Convert.ToString(row["address"]);
                        objuser.occupation = Convert.ToString(row["occupation"]);
                        objuser.image = Convert.ToString(row["image"]);
                        objuser.roleId = Convert.ToInt32(row["roleId"]);
                        objuser.RoleName = Convert.ToString(row["RoleName"]);
                        objuser.token = Convert.ToString(row["token"]);
                        objuser.IsActive = Convert.ToBoolean(row["IsActive"]);


                    };

                    userList.Add(objuser);
                }

            }
            return userList;
        }

        public List<SettingContext> getsetting(string type)
        {


            List<SettingContext> getsetting = new List<SettingContext>();

            DataTable settingdt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@Action", "SELECTSETTING");
            objParameter[1] = new SqlParameter("@type", type);

            SqlHelper.Fill(settingdt, "usp_setting", objParameter);
            if (settingdt != null && settingdt.Rows.Count > 0)
            {
                foreach (DataRow row in settingdt.Rows)
                {
                    SettingContext objsetting = new SettingContext();
                    {

                        objsetting.SettingId = Convert.ToInt32(row["SettingId"]);
                        objsetting.parameter = Convert.ToString(row["parameter"]);
                        objsetting.parameterValue = Convert.ToString(row["parameterValue"]);
                        objsetting.type = type;


                    };

                    getsetting.Add(objsetting);
                }

            }
            return getsetting;
        }


        public List<SettingContext> gettemplate(string type)
        {


            List<SettingContext> getsetting = new List<SettingContext>();

            DataTable settingdt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@Action", "SELECTTEMPLATE");
            objParameter[1] = new SqlParameter("@type", type);

            SqlHelper.Fill(settingdt, "usp_setting", objParameter);
            if (settingdt != null && settingdt.Rows.Count > 0)
            {
                foreach (DataRow row in settingdt.Rows)
                {
                    SettingContext objsetting = new SettingContext();
                    {

                        objsetting.SettingId = Convert.ToInt32(row["SettingId"]);
                        objsetting.parameter = Convert.ToString(row["parameter"]);
                        objsetting.parameterValue = Convert.ToString(row["parameterValue"]);
                        objsetting.templatesubject = Convert.ToString(row["templatesubject"]);
                        objsetting.type = type;


                    };

                    getsetting.Add(objsetting);
                }

            }
            return getsetting;
        }

        public List<SettingContext> getedittemplate(int SettingId)
        {


            List<SettingContext> getsetting = new List<SettingContext>();

            DataTable settingdt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@Action", "SELECTEDITTEMPLATE");
            //objParameter[1] = new SqlParameter("@type", type);
            objParameter[1] = new SqlParameter("@SettingId", SettingId);

            SqlHelper.Fill(settingdt, "usp_setting", objParameter);
            if (settingdt != null && settingdt.Rows.Count > 0)
            {
                foreach (DataRow row in settingdt.Rows)
                {
                    SettingContext objsetting = new SettingContext();
                    {

                        objsetting.SettingId = Convert.ToInt32(row["SettingId"]);
                        objsetting.parameter = Convert.ToString(row["parameter"]);
                        objsetting.parameterValue = Convert.ToString(row["parameterValue"]);
                        objsetting.templatesubject = Convert.ToString(row["templatesubject"]);
                        // objsetting.type = type;


                    };

                    getsetting.Add(objsetting);
                }

            }
            return getsetting;
        }
        public bool Updatesetting(SettingContext settingmodel)
        {

            DataTable settingdt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[5];
            objParameter[0] = new SqlParameter("@Action", "UPDATESETTING");
            objParameter[1] = new SqlParameter("@SettingId", settingmodel.SettingId);
            objParameter[2] = new SqlParameter("@parameter", settingmodel.parameter);
            objParameter[3] = new SqlParameter("@parameterValue", settingmodel.parameterValue);
            objParameter[4] = new SqlParameter("@type", settingmodel.type);

            int count = SqlHelper.ExecuteNonQuery("usp_setting", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool updatetemplate(SettingContext settingmodel)
        {

            DataTable settingdt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[6];
            objParameter[0] = new SqlParameter("@Action", "UPDATETEMPLATE");
            objParameter[1] = new SqlParameter("@SettingId", settingmodel.SettingId);
            objParameter[2] = new SqlParameter("@parameter", settingmodel.parameter);
            objParameter[3] = new SqlParameter("@parameterValue", settingmodel.parameterValue);
            objParameter[4] = new SqlParameter("@templatesubject", settingmodel.templatesubject);
            objParameter[5] = new SqlParameter("@type", settingmodel.type);

            int count = SqlHelper.ExecuteNonQuery("usp_setting", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        //Master Category AdminControl
        public bool ActiveUserCheckbox(UserListContext userListContext)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[3];
            objParameter[0] = new SqlParameter("@Action", "UserCheckBoxActive");
            objParameter[1] = new SqlParameter("@userId", userListContext.userId);
            objParameter[2] = new SqlParameter("@IsActive", userListContext.IsActive);

            int count = SqlHelper.ExecuteNonQuery("usp_Checkbox", objParameter);

            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
