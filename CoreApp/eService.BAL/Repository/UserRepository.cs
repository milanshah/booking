﻿using eService.Common.Helper;
using eService.DataContext.Models;
using HiQPdf;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace eService.BAL.Repository
{
    public class UserRepository
    {
        public readonly string connectionString = ConfigurationManager.ConnectionStrings["myclean"].ToString();
        public readonly bool _disposed;

        public RegistrationDataContext AuthenticateUser(string email, string password)
        {

            RegistrationDataContext objUsers = new RegistrationDataContext();
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@email", email);
            string x = Convert.ToBase64String(Encoding.UTF8.GetBytes(password));
            objParameter[1] = new SqlParameter("@password", x);
            SqlHelper.Fill(dtUser, "[userLogin]", objParameter);

            if (dtUser != null && dtUser.Rows.Count > 0)
            {
                foreach (DataRow row in dtUser.Rows)
                {
                    objUsers.userName = Convert.ToString(row["userName"]);
                    objUsers.email = Convert.ToString(row["email"]);
                    objUsers.roleId = Convert.ToInt32(row["roleId"]);
                    objUsers.userId = Convert.ToInt64(row["userId"]);
                }
            }
            return objUsers;
        }
        public bool RegisterUser(RegistrationDataContext users)
        {

            RegistrationDataContext objUsers = new RegistrationDataContext();

            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[10];
            objParameter[0] = new SqlParameter("@name", users.name);
            objParameter[1] = new SqlParameter("@contactNumber", users.contactNumber);
            objParameter[2] = new SqlParameter("@address", users.address);
            objParameter[3] = new SqlParameter("@email", users.email);
            objParameter[4] = new SqlParameter("@userName", users.userName);
            objParameter[5] = new SqlParameter("@password", Convert.ToBase64String(Encoding.UTF8.GetBytes(users.password)));
            objParameter[6] = new SqlParameter("@image", users.image);
            objParameter[7] = new SqlParameter("@occupation", users.occupation);
            objParameter[8] = new SqlParameter("@roleId", users.roleId);
            objParameter[9] = new SqlParameter("@IsActive", users.IsActive);

            //IsuserExist(isUserExist.email);

            int insertCount = SqlHelper.ExecuteNonQuery("UserRegister", objParameter);

            if (insertCount > 0)
            {
                if (insertCount == 1)
                {



                    List<SettingContext> getsetting = new List<SettingContext>();

                    DataTable settingdt = new DataTable();
                    SqlParameter[] objtemp = new SqlParameter[2];
                    objtemp[0] = new SqlParameter("@Action", "SELECTSENDINGTEMPLATE");
                    //objParameter[1] = new SqlParameter("@type", type);
                    objtemp[1] = new SqlParameter("@parameter", "WelcomeMail");

                    SqlHelper.Fill(settingdt, "usp_setting", objtemp);

                    SettingContext objsetting = new SettingContext();
                    {


                        objsetting.parameter = Convert.ToString(settingdt.Rows[0]["parameter"]);
                        objsetting.parameterValue = Convert.ToString(settingdt.Rows[0]["parameterValue"]);


                    };
                    string MailTemplate = objsetting.parameterValue;




                    //string MailTemplate = System.IO.File.ReadAllText(@"D:\Hardik\Booking Website All Data\Booking API\eService\eService.Common\EmailTemplate\SendMail.html");

                    MailTemplate = MailTemplate.Replace("[#Name#]", users.name);

                    Email.SendMail(MailTemplate, "Registration", users.email);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
       
      
        /// <returns>chcek login status</returns>
        public List<RegistrationDataContext> userData(RegistrationDataContext data)
        {
            SqlParameter[] param = {
                new SqlParameter ("@email", data.email)


            };

            return SqlHelper.ExtecuteProcedureReturnData<List<RegistrationDataContext>>(
                "userLogin", r => r.TranslateAsUsersList(), param);

        }

        //
        public List<TimeSchedule> TimeSchedules(int dayNumber)
        {
            List<TimeSchedule> list = new List<TimeSchedule>();
            List<string> myCollection = new List<string>();

            string dayName = string.Empty;
            DateTime startTime = new DateTime();
            DateTime endTime = new DateTime();
            double interval = 1;

            DataTable dtTimeDetails = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@dayNumber", dayNumber);
            SqlHelper.Fill(dtTimeDetails, "spTimeSchedule", objParameter);

            if (dtTimeDetails != null && dtTimeDetails.Rows.Count > 0)
            {
                for (int index = 0; index < dtTimeDetails.Rows.Count; index++)
                {
                    dayName = Convert.ToString(dtTimeDetails.Rows[index]["dayName"]);
                    startTime = Convert.ToDateTime(dtTimeDetails.Rows[index]["startTime"]);
                    endTime = Convert.ToDateTime(dtTimeDetails.Rows[index]["endTime"]);
                    interval = Convert.ToDouble(dtTimeDetails.Rows[index]["interval"]);

                }

                TimeSpan diff = (endTime - startTime);
                double totalHrs = diff.TotalHours;


                DateTime indexLoop = new DateTime();

                myCollection.Add(startTime.ToString("hh:mm tt"));
                indexLoop = startTime;
                while (endTime >= indexLoop)
                {

                    TimeSchedule timeSchedule = new TimeSchedule
                    {
                        dayName = dayName,
                        dayNumber = dayNumber,
                        interval = interval,
                        time = indexLoop.ToString("hh:mm tt")
                    };
                    indexLoop = indexLoop.AddHours(interval);
                    myCollection.Add(indexLoop.ToString("hh:mm tt"));
                    list.Add(timeSchedule);
                }

            }
            return list;
        }


        //get user profile_data
        public UserProfile profile(int userId)
        {
            //  List<UserProfile> List = new List<UserProfile>();
            UserProfile objUser = new UserProfile();
            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@userId", userId);
            SqlHelper.Fill(dtDetail, "usp_email", objParameter);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                foreach (DataRow row in dtDetail.Rows)
                {
                    objUser.userId = Convert.ToInt32(row["userId"]);
                    objUser.userName = Convert.ToString(row["userName"]);
                    objUser.email = Convert.ToString(row["email"]);
                    objUser.address = Convert.ToString(row["address"]);
                    objUser.contactNumber = Convert.ToString(row["contactNumber"]);
                }
            }
            return objUser;
        }
        
        //get user cancel profile_data
        public List<Userorder> Usercancelorder(int userId)
        {
            List<Userorder> objlist = new List<Userorder>();
            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@userId", userId);
            objParameter[1] = new SqlParameter("@IsCancel", true);
            SqlHelper.Fill(dtDetail, "usp_order", objParameter);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                for (int index = 0; index < dtDetail.Rows.Count; index++)
                {
                    Userorder objUser = new Userorder();
                    objUser.userId = Convert.ToInt32(dtDetail.Rows[index]["userId"]);
                    objUser.category = Convert.ToString(dtDetail.Rows[index]["category"]);
                    objUser.subcategoryName = Convert.ToString(dtDetail.Rows[index]["subCategory"]);
                    objUser.Typeserivce = Convert.ToString(dtDetail.Rows[index]["Typeserivce"]);
                    objUser.date = Convert.ToString(dtDetail.Rows[index]["date"]);
                    objUser.time = Convert.ToString(dtDetail.Rows[index]["time"]);
                    objUser.Address = Convert.ToString(dtDetail.Rows[index]["Address"]);
                    objUser.price = Convert.ToString(dtDetail.Rows[index]["price"]);
                    objUser.GrandTotal = Convert.ToString(dtDetail.Rows[index]["GrandTotal"]);
                    objUser.OrderId = Convert.ToString(dtDetail.Rows[index]["OrderID"]);
                    objlist.Add(objUser);

                }
            }
            return objlist;
        }
        //get user profile_data
        public List<Userorder> userorder(int userId)
        {
            List<Userorder> objlist = new List<Userorder>();
            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@userId", userId);
            objParameter[1] = new SqlParameter("@IsCancel", false);
            SqlHelper.Fill(dtDetail, "usp_order", objParameter);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                for (int index = 0; index < dtDetail.Rows.Count; index++)
                {
                    Userorder objUser = new Userorder();
                    objUser.userId = Convert.ToInt32(dtDetail.Rows[index]["userId"]);
                    objUser.category = Convert.ToString(dtDetail.Rows[index]["category"]);
                    objUser.subcategoryName = Convert.ToString(dtDetail.Rows[index]["subCategory"]);
                    objUser.Typeserivce = Convert.ToString(dtDetail.Rows[index]["Typeserivce"]);
                    objUser.date = Convert.ToString(dtDetail.Rows[index]["date"]);
                    objUser.time = Convert.ToString(dtDetail.Rows[index]["time"]);
                    objUser.Address = Convert.ToString(dtDetail.Rows[index]["Address"]);
                    objUser.price = Convert.ToString(dtDetail.Rows[index]["price"]);
                    objUser.GrandTotal = Convert.ToString(dtDetail.Rows[index]["GrandTotal"]);
                    objUser.OrderId = Convert.ToString(dtDetail.Rows[index]["OrderID"]);
                    objlist.Add(objUser);

                }
            }
            return objlist;
        }
        // edit profile
        public bool EditProfile(UserProfile Profile)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[5];
            objParameter[0] = new SqlParameter("@userId", Profile.userId);
            objParameter[1] = new SqlParameter("@userName", Profile.userName);
            objParameter[2] = new SqlParameter("@email", Profile.email);
            objParameter[3] = new SqlParameter("@address", Profile.address);
            objParameter[4] = new SqlParameter("@contactNumber", Profile.contactNumber);
            int count = SqlHelper.ExecuteNonQuery("[usp_EditProfile]", objParameter);

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // ChangePassword
        public bool ChngPassword(ChangePassword Password)
        {
            DataTable dtUser = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@userId", Password.userId);
            objParameter[1] = new SqlParameter("@password", Convert.ToBase64String(Encoding.UTF8.GetBytes(Password.NewPassword)));
            int count = SqlHelper.ExecuteNonQuery("[usp_changePassword]", objParameter);

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Add Employee
        public bool AddUser(UserDetails UserDetails)
        {
            try
            {
                SqlParameter[] objParameter = new SqlParameter[8];

                objParameter[0] = new SqlParameter("@userId", UserDetails.userId);
                objParameter[1] = new SqlParameter("@address", UserDetails.Address);
                objParameter[2] = new SqlParameter("@zipCode", UserDetails.ZipCode);
                objParameter[3] = new SqlParameter("@Apartment", UserDetails.Apartment);
                objParameter[4] = new SqlParameter("@serviceTime", UserDetails.serviceTime);
                objParameter[5] = new SqlParameter("@keyInfo", UserDetails.keyInfo);
                if (!string.IsNullOrEmpty(UserDetails.about_Key))
                { objParameter[6] = new SqlParameter("@about_Key", UserDetails.about_Key); }
                else { objParameter[6] = new SqlParameter("@about_Key", DBNull.Value); }
                if (!string.IsNullOrEmpty(UserDetails.about_Key))
                { objParameter[7] = new SqlParameter("@special_Notes", UserDetails.special_Notes); }
                else { objParameter[7] = new SqlParameter("@special_Notes", DBNull.Value); }

                int insertCount = SqlHelper.ExecuteNonQuery("usp_addUser", objParameter);
                if (insertCount > 0)
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception)
            {
                return false;
            }
        }
        //PAyment Detail
        public bool Payment(PaymentDataContext paymentData)
        {
            try
            {
                SqlParameter[] objParameter = new SqlParameter[6];

                objParameter[0] = new SqlParameter("@userId", paymentData.userId);
                //objParameter[1] = new SqlParameter("@PaymentId", paymentData.PaymentId);
                objParameter[1] = new SqlParameter("@Credit_Card_No", paymentData.Credit_Card_No);
                //  objParameter[2] = new SqlParameter("@Expire_Date", paymentData.Expire_Date);
                objParameter[2] = new SqlParameter("@CVV", paymentData.CVV);
                objParameter[3] = new SqlParameter("@CardHolder_Name", paymentData.CardHolder_Name);
                objParameter[4] = new SqlParameter("@Email", paymentData.Email);
                objParameter[5] = new SqlParameter("@ContactNo", paymentData.ContactNo);
                int insertCount = SqlHelper.ExecuteNonQuery("usp_Payment", objParameter);
                if (insertCount > 0)
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception)
            {
                return false;
            }

        }
        //category
        public List<CategoryDetails> CategoryDetail(string CategoryValues)
        {
            List<CategoryDetails> categoryList = new List<CategoryDetails>();
            //CategoryDetails objCategory = new CategoryDetails();
            DataTable dtCategory = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@SubcategoriesId", CategoryValues);
            SqlHelper.Fill(dtCategory, "usp_getCardName", objParameter);
            if (dtCategory != null && dtCategory.Rows.Count > 0)
            {
                foreach (DataRow row in dtCategory.Rows)
                {
                    CategoryDetails objCategory = new CategoryDetails
                    {
                        SubCategoriesId = Convert.ToInt32(row["SubCategoriesId"]),
                        categoryId = Convert.ToInt32(row["categoryId"]),
                        subCategoryName = Convert.ToString(row["subCategoryName"]),
                        price = Convert.ToInt32(row["price"]),
                        image = Convert.ToString(row["image"])
                    };

                    categoryList.Add(objCategory);
                }

            }
            return categoryList;
        }

        
        public Userorder Downloadorderinpdf(string orderId)
        {
           
            Userorder objUser = new Userorder();
            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@orderID", orderId);
            SqlHelper.Fill(dtDetail, "usp_Downloadpdf", objParameter);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                for (int index = 0; index < dtDetail.Rows.Count; index++)
                {

                    objUser.userId = Convert.ToInt32(dtDetail.Rows[index]["userId"]);
                    objUser.category = Convert.ToString(dtDetail.Rows[index]["category"]);
                    objUser.subcategoryName = Convert.ToString(dtDetail.Rows[index]["subCategory"]);
                    objUser.Typeserivce = Convert.ToString(dtDetail.Rows[index]["Typeserivce"]);
                    objUser.date = Convert.ToString(dtDetail.Rows[index]["date"]);
                    objUser.time = Convert.ToString(dtDetail.Rows[index]["time"]);
                    objUser.Address = Convert.ToString(dtDetail.Rows[index]["Address"]);
                    objUser.price = Convert.ToString(dtDetail.Rows[index]["price"]);
                    objUser.GrandTotal = Convert.ToString(dtDetail.Rows[index]["GrandTotal"]);
                    objUser.OrderId = Convert.ToString(dtDetail.Rows[index]["OrderID"]);


                }
                //string mailTemplteFile = System.IO.File.ReadAllText(@"D:\Naitik\bookinglatest\svnbooking\CoreApp\eService.Common\EmailTemplate\Evoice.html");

                //mailTemplteFile = mailTemplteFile.Replace("[#category#]", objUser.category).
                //     Replace("[#subCategory#]", objUser.subcategoryName
                //  .Replace("[", "").Replace('"', ' ').Replace("]", ""))
                //    .Replace("[#Typeserivce#]", objUser.Typeserivce)
                //    .Replace("[#date#]", objUser.date)
                //    .Replace("[#time#]", objUser.time)
                //    .Replace("[#Address#]", objUser.Address)
                //    .Replace("[#price#]", objUser.price)
                //    .Replace("[#GrandTotal#]", objUser.GrandTotal)
                //    .Replace("[#orderId#]", objUser.OrderId);
                    

                //string pdfFilePath = @"D:\Naitik\bookinglatest\svnbooking\CoreApp\eService.BAL\PDF";
                ////Convert HTML to PDF
                //HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
                //htmlToPdfConverter.Document.PageSize = PdfPageSize.A4;
                //htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Portrait;
                ////htmlToPdfConverter.BrowserWidth = 1200;
                //htmlToPdfConverter.Document.FitPageWidth = true;
                //htmlToPdfConverter.Document.FitPageHeight = true;
                ////htmlToPdfConverter.Document.Margins = new PdfMargins(
                ////                0, 0, 10, 10);

                //if (!System.IO.Directory.Exists(pdfFilePath))
                //{
                //    System.IO.Directory.CreateDirectory(pdfFilePath);
                //}

                //string filepath = pdfFilePath + @"\Evoice.pdf";

                //if (!System.IO.File.Exists(filepath))
                //{
                //    System.IO.File.Create(filepath);
                //}

                //htmlToPdfConverter.ConvertHtmlToFile(mailTemplteFile, null, filepath);


            }
            return objUser;
        }

        public Userorder UserOrderDelete(string orderId)
        {

            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@orderId", orderId);
            objParameter[1] = new SqlParameter("@IsCancel", true);
            SqlHelper.Fill(dtDetail, "usp_cancelorder", objParameter);
           
           return null;
            
        }
        public string Download(string orderId,string filepath)
        {
            //string filepath = string.Empty;
            Userorder objUser = new Userorder();
            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@orderID", orderId);
            SqlHelper.Fill(dtDetail, "usp_Downloadpdf", objParameter);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                for (int index = 0; index < dtDetail.Rows.Count; index++)
                {

                    objUser.userId = Convert.ToInt32(dtDetail.Rows[index]["userId"]);
                    objUser.category = Convert.ToString(dtDetail.Rows[index]["category"]);
                    objUser.subcategoryName = Convert.ToString(dtDetail.Rows[index]["subCategory"]);
                    objUser.Typeserivce = Convert.ToString(dtDetail.Rows[index]["Typeserivce"]);
                    objUser.date = Convert.ToString(dtDetail.Rows[index]["date"]);
                    objUser.time = Convert.ToString(dtDetail.Rows[index]["time"]);
                    objUser.Address = Convert.ToString(dtDetail.Rows[index]["Address"]);
                    objUser.price = Convert.ToString(dtDetail.Rows[index]["price"]);
                    objUser.GrandTotal = Convert.ToString(dtDetail.Rows[index]["GrandTotal"]);
                    objUser.OrderId = Convert.ToString(dtDetail.Rows[index]["OrderID"]);


                }
                string mailTemplteFile = System.IO.File.ReadAllText(@"D:\Naitik\bookinglatest\svnbooking\CoreApp\eService.Common\EmailTemplate\Evoice.html");

                mailTemplteFile = mailTemplteFile.Replace("[#category#]", objUser.category).
                     Replace("[#subCategory#]", objUser.subcategoryName
                  .Replace("[", "").Replace('"', ' ').Replace("]", ""))
                    .Replace("[#Typeserivce#]", objUser.Typeserivce)
                    .Replace("[#date#]", objUser.date)
                    .Replace("[#time#]", objUser.time)
                    .Replace("[#Address#]", objUser.Address)
                    .Replace("[#price#]", objUser.price)
                    .Replace("[#GrandTotal#]", objUser.GrandTotal)
                    .Replace("[#orderId#]", objUser.OrderId);


                //string pdfFilePath = @"D:\Naitik\bookinglatest\svnbooking\CoreApp\eService.BAL\PDF";
                //Convert HTML to PDF
                HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
                htmlToPdfConverter.Document.PageSize = PdfPageSize.A4;
                htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Portrait;
                //htmlToPdfConverter.BrowserWidth = 1200;
                htmlToPdfConverter.Document.FitPageWidth = true;
                htmlToPdfConverter.Document.FitPageHeight = true;
                //htmlToPdfConverter.Document.Margins = new PdfMargins(
                //                0, 0, 10, 10);

                //if (!System.IO.Directory.Exists(pdfFilePath))
                //{
                //    System.IO.Directory.CreateDirectory(pdfFilePath);
                //}

                //filepath = pdfFilePath + @"\Evoice.pdf";

                if (!System.IO.File.Exists(filepath))
                {
                    System.IO.File.Create(filepath);
                }

                htmlToPdfConverter.ConvertHtmlToFile(mailTemplteFile, null, filepath);

               
            }
            return filepath;
            //var stream = File.OpenRead(filepath);
            //return new FileStreamResult(stream, "application/octet-stream");
        }

        //Payment Receipt
        public bool Receipt(PaymentReceipt paymentData)
        {
            try
            {
                SqlParameter[] objParameter = new SqlParameter[14];

                objParameter[0] = new SqlParameter("@userId", paymentData.userId);
                objParameter[1] = new SqlParameter("@subCategory", paymentData.subCategory);
                objParameter[2] = new SqlParameter("@Typeserivce", paymentData.Typeserivce);
                objParameter[3] = new SqlParameter("@date", paymentData.date);
                objParameter[4] = new SqlParameter("@keyInfo", paymentData.keyInfo);
                if (!string.IsNullOrEmpty(paymentData.Apartment))
                { objParameter[5] = new SqlParameter("@Apartment", paymentData.Apartment); }
                else { objParameter[5] = new SqlParameter("@Apartment", DBNull.Value); }
                objParameter[6] = new SqlParameter("@time", paymentData.time);
                objParameter[7] = new SqlParameter("@Address", paymentData.Address);
                objParameter[8] = new SqlParameter("@category", paymentData.category);
                objParameter[9] = new SqlParameter("@price", paymentData.price);
                objParameter[10] = new SqlParameter("@GrandTotal", paymentData.GrandTotal);
                objParameter[11] = new SqlParameter("@orderID", paymentData.orderID);
                objParameter[12] = new SqlParameter("@TransactionID", paymentData.TransactionID);
                objParameter[13] = new SqlParameter("@IsCancel", false);

                int insertCount = SqlHelper.ExecuteNonQuery("usp_Receipt", objParameter);
                List<SettingContext> getsetting = new List<SettingContext>();

                DataTable settingdt = new DataTable();
                SqlParameter[] objtemp = new SqlParameter[2];
                objtemp[0] = new SqlParameter("@Action", "SELECTRECEIPTTEMPLATE");
                //objParameter[1] = new SqlParameter("@type", type);
                objtemp[1] = new SqlParameter("@parameter", "order");

                SqlHelper.Fill(settingdt, "usp_setting", objtemp);

                SettingContext objsetting = new SettingContext();
                {


                    objsetting.parameter = Convert.ToString(settingdt.Rows[0]["parameter"]);
                    objsetting.parameterValue = Convert.ToString(settingdt.Rows[0]["parameterValue"]);


                };
                string MailTemplate = objsetting.parameterValue;
                //string MailTemplate = System.IO.File.ReadAllText(@"D:\Hardik\Booking Website All Data\Booking API\eService\eService.Common\EmailTemplate\Receipt.html");
                MailTemplate = MailTemplate.Replace("[#category#]", paymentData.category).
               Replace("[#subCategory#]", paymentData.subCategory
                  .Replace("[", "").Replace('"', ' ').Replace("]", ""))
                    .Replace("[#Typeserivce#]", paymentData.Typeserivce)
                    .Replace("[#date#]", paymentData.date.ToString("ddd, dd MMM yyyy"))
                    .Replace("[#keyInfo#]", paymentData.keyInfo)
                    .Replace("[#Apartment#]", paymentData.Apartment)
                    .Replace("[#time#]", paymentData.time)
                    .Replace("[#Address#]", paymentData.Address)
                    .Replace("[#price#]", paymentData.price)
                    .Replace("[#GrandTotal#]", paymentData.GrandTotal)
                    .Replace("[#orderId#]", paymentData.orderID).
                    Replace("[#TransactionId#]", paymentData.TransactionID)
                    .Replace("[#Status#]", paymentData.Status);

                string EmailAddress = string.Empty;

                DataTable dtEmailAddress = new DataTable();
                SqlParameter[] obj = new SqlParameter[1];
                obj[0] = new SqlParameter("@userId", paymentData.userId);
                SqlHelper.Fill(dtEmailAddress, "usp_email ", obj);
                if (dtEmailAddress != null && dtEmailAddress.Rows.Count > 0)
                {
                    foreach (DataRow row in dtEmailAddress.Rows)
                    {
                        EmailAddress = Convert.ToString(row["email"]);
                    }
                }
                Email.Receipt(MailTemplate, "Receipt", EmailAddress);
                if (insertCount > 0)
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception)
            {
                return false;
            }
        }

        ////for exist email
        ///
        public bool IsuserExist(string email)
        {
            DataTable dtDetail = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@email", email);
            SqlHelper.Fill(dtDetail, "usp_userExist", objParameter);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        //forgot Password
        public bool ForgotPassword(ForgotPassword forgot)
        {
            if (IsuserExist(forgot.email))
            {
                string token = CrptographyEngine.Encrypt(forgot.email);
                try
                {
                    DataTable dtUser = new DataTable();
                    SqlParameter[] objParameter = new SqlParameter[2];
                    objParameter[0] = new SqlParameter("@email", forgot.email);
                    objParameter[1] = new SqlParameter("@token", token);
                    int updateCount = SqlHelper.ExecuteNonQuery("usp_ResetPassword", objParameter);
                    if (updateCount > 0)
                    {
                        List<SettingContext> getsetting = new List<SettingContext>();

                        DataTable settingdt = new DataTable();
                        SqlParameter[] objtemp = new SqlParameter[2];
                        objtemp[0] = new SqlParameter("@Action", "SELECTFORGOTPASSWORDTEMPLATE");
                        //objParameter[1] = new SqlParameter("@type", type);
                        objtemp[1] = new SqlParameter("@parameter", "forgotpassword");

                        SqlHelper.Fill(settingdt, "usp_setting", objtemp);

                        SettingContext objsetting = new SettingContext();
                        {


                            objsetting.parameter = Convert.ToString(settingdt.Rows[0]["parameter"]);
                            objsetting.parameterValue = Convert.ToString(settingdt.Rows[0]["parameterValue"]);


                        };
                        string MailTemplate = objsetting.parameterValue;
                        //string MailTemplate = System.IO.File.ReadAllText(@"D:\Hardik\Booking Website All Data\Booking API\eService\eService.Common\EmailTemplate\ForgotPassword.html");
                        MailTemplate = MailTemplate.Replace("[#tokenLink#]", "http://192.168.0.16:7071/#/create-password?token=" + token);

                        Email.ForgotPassword(MailTemplate, "Booking Website - Forget Password", forgot.email);
                        return true;
                    }

                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {

                }
            }

            return false;

        }
        //create Password
        public bool CreatePassword(Create_Password create)
        {
            string email = CrptographyEngine.Decrypt(create.token);
            try
            {
                DataTable dtUser = new DataTable();
                SqlParameter[] objParameter = new SqlParameter[2];
                objParameter[0] = new SqlParameter("@email", email);
                objParameter[1] = new SqlParameter("@password", Convert.ToBase64String(Encoding.UTF8.GetBytes(create.password)));
                int count = SqlHelper.ExecuteNonQuery("[usp_CreatePassword]", objParameter);

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }
    }
}
